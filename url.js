var subscription = require('./lib/controller/subscription');
var subscribers = require('./lib/controller/subscribers');
var stat = require('./lib/controller/backoffice/stat');
var index = require('./routes/index');
var dashboard = require('./lib/controller/backoffice/dashboard');

var usersRoute = require('./lib/controller/backoffice/users');
var productRoute = require('./lib/controller/backoffice/products');
var manufacturerRoute = require('./lib/controller/backoffice/manufacturer');

var backofficeCategory = require('./lib/controller/backoffice/category');

var loginAdmin = require('./lib/controller/backoffice/login');

var galleryRoute = require('./lib/controller/backoffice/gallery');

var termsRoute = require('./lib/controller/backoffice/terms');

var storesRoute = require('./lib/controller/backoffice/stores');

var articlesRoute = require('./lib/controller/backoffice/articles');

var howtoRoute = require('./lib/controller/backoffice/howto');

var offersRoute = require('./lib/controller/backoffice/offers');

var routeHelpers = require('./lib/helpers');

var initRoute = require('./lib/controller/backoffice/init');

var linkRoute = require('./lib/controller/backoffice/links');

var backendSitemapRoute = require('./lib/controller/backoffice/sitemap');

var ajaxRoute = require('./lib/controller/app/ajax');

var todoRoute = require('./lib/controller/backoffice/todo');

//route for update_offers_api file
var priceCheckerRoute = require('./lib/controller/backoffice/update_offers_api');

var setupSettingsRoute = require('./lib/controller/backoffice/setup');
var backofficeSubscriberRoute = require('./lib/controller/backoffice/subscribers');

var indexAllRoute = require('./lib/controller/backoffice/index_all_products');

var landingPageRoute = require('./lib/controller/backoffice/landing_pages');

var rankingRoute = require('./lib/controller/app/ranking');


var appIndex = require('./lib/controller/app/index');
var appLandingPage = require('./lib/controller/app/landing_page');
var _appLandingPage = require('./lib/controller/app/landingpage');
var termsAppRoute = require('./lib/controller/app/terms');
var appProduct = require('./lib/controller/app/product');
var appShop = require('./lib/controller/app/shop');
var appBrand = require('./lib/controller/app/manufacturer');
var appReviews = require('./lib/controller/app/reviews');
var appHowTo = require('./lib/controller/app/howto');
var appTopTen = require('./lib/controller/app/top10');
var appNews = require('./lib/controller/app/news');
var appBlog = require('./lib/controller/app/blog');
var appCategory = require('./lib/controller/app/categories');
var sitemapRoute = require('./lib/controller/app/sitemap');
var tagRoute = require('./lib/controller/app/tags');
var rssRoute = require('./lib/controller/app/rss');
var staticSitemapRoute = require('./lib/controller/app/static-sitemap');
var fbCatalogFeedRoute = require('./lib/controller/app/facebook_catalog_feed');

var manualRoute = require('./lib/controller/app/manual');





var homeRoute = require('./lib/controller/app/home');



module.exports = function(app){
//    backoffice homepage
    app.get('/backoffice', function(req, res){
        res.render('backoffice.html', {});
    })

//    robots.txt file
    app.get('/robots.txt', function(req, res){
        var txt = "User-agent: * <br>" +
            "Disallow: /backoffice/ <br>"+
            "Disallow: /backend/<br>"+
            "Disallow: /ajax/";
        return res.send(txt)
    })

    app.get('/test/template', function(req, res){
        return res.render('v2/404', {});
    });

//    app.get('/home', homeRoute);
    app.get("/backend/backoffice/api/manual/create_admin", manualRoute.create_super_admin_user);
    app.get("/backend/backoffice/api/manual/category/add_postion", manualRoute.add_positon_to_all_categories);

    var Product = require('./lib/models/product');
    app.get('/fix/publish_all', function(req, res){
        Product.update({'offers.0': {$exists: true}}, {$set: {status: 'published'}}, {multi:true}, function(err){
            if(err){
                console.log('error occurred in publish update');
                res.send('done')
            }else{
                console.log('no error')
                res.send('done')
            }
        })
    });


    app.get('/backend/backoffice/api/pricechecker/products', routeHelpers.is_admin, priceCheckerRoute.list);
    app.get('/backend/backoffice/api/pricechecker/add_offers_last_date', routeHelpers.is_admin, priceCheckerRoute.add_offers_last_date);
    app.post('/backend/backoffice/api/pricechecker/update/offers', routeHelpers.is_admin, priceCheckerRoute.update_product_offers);


    app.get('/backend/backoffice/api/products/solr/index_all', routeHelpers.is_admin, indexAllRoute.index_all);



    app.get('/backend/app/task/rerank', rankingRoute.rank);

    app.get('/backend/backoffice/api/fb/catalog/:slug', fbCatalogFeedRoute.feed);
    //setup settings routes

    app.get("/backend/backoffice/api/settings", setupSettingsRoute.settings);

    //subscribers backoffice route
    app.get("/backend/backoffice/api/subscribers", backofficeSubscriberRoute.list);
    app.get("/backend/backoffice/api/subscriber/:id", backofficeSubscriberRoute.get);

//    sitemap routes
    app.get('/sitemaps/:sitemap', sitemapRoute.sitemap);
    app.get('/sitemaps', sitemapRoute.sitemap_index);

    app.get('/rss/products', rssRoute.product_feed);
    app.get('/rss/articles', rssRoute.article_feed);
    app.get('/rss/howtos', rssRoute.howto_feed);

//    articles route
    app.get('/backend/backoffice/api/articles', routeHelpers.is_admin, articlesRoute.list);
    app.post('/backend/backoffice/api/article/new', routeHelpers.is_admin, articlesRoute.create);
    app.get('/backend/backoffice/api/article/:id', routeHelpers.is_admin, articlesRoute.get);
    app.post('/backend/backoffice/api/article/edit/:id', routeHelpers.is_admin, articlesRoute.edit);
    app.get('/backend/backoffice/api/article/search/:query', routeHelpers.is_admin, articlesRoute.search);
    app.post('/backend/backoffice/api/article/bulk_delete', routeHelpers.is_admin, articlesRoute.bulk_delete);

    app.get('/backend/backoffice/api/article/option/types', routeHelpers.is_admin, articlesRoute.article_types);


    //landing page routes
    app.get('/backend/backoffice/api/landing_pages', routeHelpers.is_admin, landingPageRoute.list);
    app.post('/backend/backoffice/api/landing_page/new', routeHelpers.is_admin, landingPageRoute.create);
    app.get('/backend/backoffice/api/landing_page/:id', routeHelpers.is_admin, landingPageRoute.get);
    app.post('/backend/backoffice/api/landing_page/edit/:id', routeHelpers.is_admin, landingPageRoute.edit);



    app.get('/backend/backoffice/api/terms', routeHelpers.is_admin, termsRoute.list);
    app.post('/backend/backoffice/api/term/new', routeHelpers.is_admin, termsRoute.create);
    app.post('/backend/backoffice/api/term/edit/:id', routeHelpers.is_admin, termsRoute.edit);
    app.get('/backend/backoffice/api/term/:id', routeHelpers.is_admin, termsRoute.get);
//	app.post('/backend/backoffice/api/todo/new', routeHelpers.is_admin, todoRoute.create);
//	app.get('/backend/backoffice/api/todo/delete/:id', routeHelpers.is_admin, todoRoute.delete);
//	app.get('/backend/backoffice/api/todos', routeHelpers.is_admin, todoRoute.list);


// upload to gallery
    app.post('/backend/backoffice/api/upload', routeHelpers.is_admin, galleryRoute.upload);
    app.get('/backend/backoffice/api/images', routeHelpers.is_admin, galleryRoute.list);
    app.post('/backend/backoffice/api/image/bulk_delete', routeHelpers.is_admin, galleryRoute.bulk_delete);
    app.get('/backend/backoffice/api/image/search/:query', routeHelpers.is_admin, galleryRoute.search);


//    api urls
    app.get('/backend/api/email/subscription', subscription);
    app.get('/backend/api/email/subscribers', routeHelpers.is_admin, subscribers.list);

//dashboard url
    app.get('/backend/backoffice/api/dashboard', routeHelpers.is_admin, dashboard);



    app.get('/backend/backoffice/api/init',  initRoute);




//    login and logout
    app.post('/backend/backoffice/api/login', loginAdmin.login);
    app.get('/backend/backoffice/api/logout', routeHelpers.is_admin, loginAdmin.logout);

//    backend backoffice api
    app.get('/backend/backoffice/api/categories', routeHelpers.is_admin, backofficeCategory.list);
    app.get('/backend/backoffice/api/category/delete/:id', routeHelpers.is_admin, backofficeCategory.delete)
    app.get('/backend/backoffice/api/category/:id', routeHelpers.is_admin, backofficeCategory.get)
    app.post('/backend/backoffice/api/category/new', routeHelpers.is_admin, backofficeCategory.create);
    app.post('/backend/backoffice/api/category/edit/:id', routeHelpers.is_admin, backofficeCategory.edit)


//    backend backoffice for manufacturer
    app.get('/backend/backoffice/api/manufacturers', routeHelpers.is_admin, manufacturerRoute.list);
    app.get('/backend/backoffice/api/manufacturer/:id', routeHelpers.is_admin, manufacturerRoute.get);
    app.post('/backend/backoffice/api/manufacturer/new', routeHelpers.is_admin, manufacturerRoute.create);
    app.post('/backend/backoffice/api/manufacturer/edit/:id', routeHelpers.is_admin, manufacturerRoute.edit)

//    backend backoffice for stores
    app.get('/backend/backoffice/api/stores', routeHelpers.is_admin, storesRoute.list);
    app.post('/backend/backoffice/api/store/new', routeHelpers.is_admin, storesRoute.create);
    app.get('/backend/backoffice/api/store/:id', routeHelpers.is_admin, storesRoute.get);
    app.post('/backend/backoffice/api/store/edit/:id', routeHelpers.is_admin, storesRoute.edit);


// users
    app.get('/backend/backoffice/api/users/roles', routeHelpers.is_admin, usersRoute.user_roles);
    app.get('/backend/backoffice/api/users', routeHelpers.is_admin, usersRoute.list);
    app.post('/backend/backoffice/api/user/edit/:id', routeHelpers.is_admin, usersRoute.edit);
    app.post('/backend/backoffice/api/user/new', routeHelpers.is_admin, usersRoute.create);
    app.get('/backend/backoffice/api/user/:id', routeHelpers.is_admin, usersRoute.get);
    app.post('/backend/backoffice/api/user/bulk_delete', routeHelpers.is_admin, usersRoute.bulk_delete);



    app.post('/backend/backoffice/api/product/new', routeHelpers.is_admin, productRoute.create);
    app.post('/backend/backoffice/api/product/edit/:id', routeHelpers.is_admin, productRoute.edit)
    app.get('/backend/backoffice/api/product/:id', routeHelpers.is_admin, productRoute.get);
    app.get('/backend/backoffice/api/products', routeHelpers.is_admin, productRoute.list);
    app.get('/backend/backoffice/api/product/delete/:id', routeHelpers.is_admin, productRoute.delete);
    app.post('/backend/backoffice/api/product/bulk_delete', routeHelpers.is_admin, productRoute.bulk_delete);
    app.get('/backend/backoffice/api/product/search/:query', routeHelpers.is_admin, productRoute.search)
    app.get('/backend/backoffice/api/products/todo', routeHelpers.is_admin, productRoute.get_products_todo);
    app.get('/backend/backoffice/api/products/remove/state/delete', routeHelpers.is_admin, productRoute.remove_all_with_state_delete);


    app.get('/backend/backoffice/api/offer/new', routeHelpers.is_admin, offersRoute.create);
    app.get('/backend/backoffice/api/offer/delete/:product_id/:offer_id', routeHelpers.is_admin, offersRoute.delete);
    app.get('/backend/backoffice/api/offer/bulk_delete/:product_id', routeHelpers.is_admin, offersRoute.delete);


    app.get('/backend/backoffice/api/howtos', routeHelpers.is_admin, howtoRoute.list);
    app.post('/backend/backoffice/api/howto/new', routeHelpers.is_admin, howtoRoute.create);
    app.get('/backend/backoffice/api/howto/:id', routeHelpers.is_admin, howtoRoute.get);
    app.post('/backend/backoffice/api/howto/edit/:id', routeHelpers.is_admin, howtoRoute.edit);
    app.get('/backend/backoffice/api/howto/search/:query', routeHelpers.is_admin, howtoRoute.search);
    app.post('/backend/backoffice/api/howto/bulk_delete', routeHelpers.is_admin, howtoRoute.bulk_delete);


    // backendSitemapRoute
    // static sitemaps
    app.get('/backend/backoffice/api/sitemaps', routeHelpers.is_admin, backendSitemapRoute.list);
    app.get('/backend/backoffice/api/sitemap/delete/:id', routeHelpers.is_admin, backendSitemapRoute.delete)
    app.post('/backend/backoffice/api/sitemap/new', routeHelpers.is_admin, backendSitemapRoute.create);

    app.get('/backend/backoffice/api/links', routeHelpers.is_admin, linkRoute.list);
    app.get('/backend/backoffice/api/link/delete/:id', routeHelpers.is_admin, linkRoute.delete)
    app.get('/backend/backoffice/api/link/:id', routeHelpers.is_admin, linkRoute.get)
    app.post('/backend/backoffice/api/link/new', routeHelpers.is_admin, linkRoute.create);
    app.post('/backend/backoffice/api/link/edit/:id', routeHelpers.is_admin, linkRoute.edit);



//    statistics api
    app.get('/backend/api/backoffice/stat', routeHelpers.is_admin, stat)



//    app.get('/backend/api/correction', require('./lib/controller/app/correction'));




//    app urls
//    app.get('/', index.index);



//    frontend url

    app.get('/subdomain/reviews/', routeHelpers.set_global_template_variables, appReviews.list);
    app.get('/subdomain/reviews/:slug', routeHelpers.set_global_template_variables, appReviews.review);

    app.get('/subdomain/howto/', routeHelpers.set_global_template_variables, appHowTo.list);
    app.get('/subdomain/howto/:slug', routeHelpers.set_global_template_variables, appHowTo.howto);

    app.get('/subdomain/blog/', routeHelpers.set_global_template_variables, appBlog.list);
    app.get('/subdomain/blog/:slug', routeHelpers.set_global_template_variables, appBlog.blog);

    app.get('/subdomain/news/', routeHelpers.set_global_template_variables, appNews.list);
    app.get('/subdomain/news/:slug', routeHelpers.set_global_template_variables, appNews.news);

    app.get('/subdomain/top10/', routeHelpers.set_global_template_variables, appTopTen.list);
    app.get('/subdomain/top10/:slug', routeHelpers.set_global_template_variables, appTopTen.topten);

    app.get('/', routeHelpers.set_global_template_variables, homeRoute);
    app.get('/terms', routeHelpers.set_global_template_variables, termsAppRoute.list);
    app.get('/search', routeHelpers.set_global_template_variables, appIndex);
    app.get('/page/:slug', routeHelpers.set_global_template_variables, appLandingPage);
    app.get('/pg/:slug', routeHelpers.set_global_template_variables, _appLandingPage);
    app.get('/tags', function(req, res){
        res.redirect('/');
    });
    app.get('/tags/:tag', routeHelpers.set_global_template_variables, tagRoute)
    app.get('/stores', routeHelpers.set_global_template_variables, appShop.list);
    app.get('/stores/:slug', routeHelpers.set_global_template_variables, appShop.shop);
    app.get('/brands', routeHelpers.set_global_template_variables, appBrand.list);
    app.get('/brands/:slug', routeHelpers.set_global_template_variables, appBrand.manufacturer);


    app.get('/static_sitemap', staticSitemapRoute.display_sitemaps);

    app.get('/categories/:slug', routeHelpers.set_global_template_variables, appCategory.category)



//    routes that work with jquery components using ajax
    app.get('/ajax/pageviews', ajaxRoute.page_views_counter);
    app.get('/ajax/product/:id', ajaxRoute.product);
    app.get('/ajax/news/latest', ajaxRoute.recent_news);
    app.get('/ajax/posts/popular', ajaxRoute.popular_posts);
    app.get('/ajax/products/similar/:term', ajaxRoute.similar_products);
    app.get('/ajax/products/hot', ajaxRoute.hot_products);


//    app.get('/frontend/tags')
//    app.get('/frontend/tags/:tag')



    app.get('/:slug', routeHelpers.set_global_template_variables, appProduct);


    app.get('/testing/download', function(req, res){

//        console.log('query= '+JSON.stringify(req.query));
        routeHelpers.download_price(req.query.url, req.query.css).then(function(result){

            res.send(result);
        })

//        return res.send('done');
    })






}
