var _ = require('underscore');
var numeral = require('numeral');

module.exports = function(app){

//  return the price of the offer with the minimum price
    app.locals.minimum_price = function(offers){
        var cheapest_offer = _.min(offers, 'price');

        var number = numeral(cheapest_offer.price)
        var c = number.format('0,0');
        console.log('locale money '+c);
        return c;
    }

    app.locals.minimum_price_from_store = function(product, store_id){

        var prices_from_store = [];

        var offers = product.offers;

        console.log('type of: '+typeof offers);
//        var offers = String(offers).split(/\{[^]\}/);

        console.log('inserted product: '+offers)

//        console.log('inserted store id: '+store_id);
//        console.log('inserted offers: '+ offers);

        for(var i = 0; i < offers.length; i++){
            var offer = offers[i];
            console.log('single offer:'+offer);
            if(String(offer['store']) === String(store_id)){
                console.log('entered once');
                prices_from_store.push(offer.price);
            }

        }

        console.log('store offers: '+prices_from_store);
        var number = numeral(_.min(prices_from_store));
        var p = number.format('0,0');

        console.log('locale money '+p);
        return p;


    }


//    template tag to format number into currency
    app.locals.format_money = function(decPlaces, thouSeparator, decSeparator) {
            var n = this,
            decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
            decSeparator = decSeparator == undefined ? "." : decSeparator,
            thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
            sign = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
    };

    app.locals.insert_offers_count_in_meta_description = function(text, count){
        console.log('template offer count : '+count);

        console.log('initial text'+text);
        console.log('initial text type'+ typeof text);

        var new_text = text.replace('[store-count]', String(count));

        console.log('new seo text'+new_text);
        return new_text;
    }


    app.locals.addition = function(one, two){
        console.log("one is: " +one+" and two is: "+two );
        var result = parseInt(one) + parseInt(two)
        console.log("result of addition is: "+result);
        return result
    }

    app.locals.toMoney = function(amount){
        if(amount == null || amount == undefined){
            return "";
        }

        var number = numeral(amount);
        var money = number.format('0,0');
        console.log('locale money'+money);
        return money;

    }


}
