/*
* Hooks is a function that receives the socketio io object
* to listen to events to hook to
*
* write a function in the plugin directory then use the io object
* here to attach the function to the event it is listening too
*
* List of Events
* 1.
*
*
* */

var helpers = require('./helpers')

var send_email = helpers.send_email;
var registration_email = helpers.compose_registration_email;

var Product = require("./models/product");

module.exports = function(events){

    events.on('user-created', function(user){
//      send  login details
        var email_subject = 'Bestprices.ng login details';
        send_email(user.email, null, email_subject, registration_email(user));

    });

    //event trigged to publish a product in solr index
    events.on('index-product', function(product_id){
        Product.findOne({_id: product_id}).populate("offers")
                  .populate("offers.store").populate("manufacturer")
                  .populate("category").exec(function(err, product){
                      if(err){
                        console.log("error occured");
                      }else{
                        if(product.status == "published")
                          helpers.index_product(product);
                      }
                  });
    })




}
