var _ = require('underscore');
var Category = require('../lib/models/category');
var Link = require('../lib/models/link');
var settings = require('../settings.js');

var sendgrid_username = 'darilldrems';
var sendgrid_password = 'Jack2211989_';

var sendgrid = require('sendgrid')(sendgrid_username, sendgrid_password);

//var Browser = require('zombie');
//var assert = require('assert');
var YQL = require('yql');

var short_code_snippets = require('../lib/short_code_snippets');

var request = require('request');
var cheerio = require('cheerio');

var Q = require('q');

var mustache = require('mustache');

var solr = require('solr-client');

exports.is_loggedin = function(req, res, next){
    if(req.user){
        return next()
    }
    return res.status(401).json({});
}


exports.index_product = function(product){

    // do not index products without offers.
    if (product.offers.length == 0 || product.status === "draft"){
        return;
    }

    var minimum_price = product.offers[0].price;
    var lowest_price_store = "";

    for(var i =0; i < product.offers.length;i++){
        var offer = product.offers[i];

        if(offer.price < minimum_price){
            minimum_price = offer.price;
            lowest_price_store = offer.store.name;
        }
    }

    var client = solr.createClient({
        host: settings.solr.ip,
        port: settings.solr.port,
        path: settings.solr.path,
        core: settings.solr.core
    });

    var productData = {
        "id": product._id,
        "brand": product.manufacturer.name,
        "category": product.category.name,
        "description": product.description,
        "lowest_price": minimum_price,
        "title": product.title,
        "offers": JSON.stringify(product.offers),
        "image_src": product.photos[0],
        "number_of_offers": product.offers.length,
        "slug": product.slug,
        "lowest_price_store": lowest_price_store

    }

    // Add a new document
    client.add(productData,function(err,obj){
       if(err){
          console.log("error:", err);
       }else{
          console.log('Solr response:', obj);
       }
    });

    // var solr_server = settings.solr;

    // var solr_update_path = solr_server + "update";
    // var solr_commit_path = solr_server + "update?commit=true";

    // console.log(solr_update_path)

    // var minimum_price = product.offers[0].price;



    // var productData = {
    //     "id": product._id,
    //     "brand": product.manufacturer.name,
    //     "category": product.category.name,
    //     "description": product.description,
    //     "lowest_price": minimum_price,
    //     "title": product.title,
    //     "offers": JSON.stringify(product.offers),
    //     "image_src": product.photos[0]
    // }

    // console.log(JSON.stringify(productData))
    // request({
    //     url: solr_update_path, //URL to hit
    //     qs: productData, //Query string data
    //     method: 'POST',
    //     headers: {
    //         'Content-Type': 'application/json'
    //     },
    //     body: JSON.stringify(productData) //Set the body as a string
    // }, function(error, response, body){
    //     if(error) {
    //         console.log("in error");
    //     } else {
    //         console.log("works with response: ", body);
    //     }
    // });


  //   request({
  //   method: 'POST',
  //   preambleCRLF: true,
  //   postambleCRLF: true,
  //   uri: solr_update_path,
  //   multipart: [
  //     {
  //       'content-type': 'application/json',
  //       body: JSON.stringify(productData)
  //     }
  //   ]
  // },
  // function (error, response, body) {
  //   if (error) {
  //     return console.log('upload failed:');
  //   }
  //   console.log('Upload successful!  Server responded with:', body);
  // })

    // console.log(JSON.stringify(productData))

    // request.post({url:solr_update_path, form: productData, }, function(err,httpResponse,body){

    //     if(err){
    //         console.log("error");
    //     }else{
    //         console.log("doing fine");
    //     }

    // })

    // request.post({url:solr_update_path, formData: productData}, function optionalCallback(err, httpResponse, body) {
    //   if (err) {
    //     console.log("in error here")
    //     return console.error('upload failed:', err);
    //   }
    //   console.log('Upload successful!  Server responded with:', body);
    // });

}

//exports.is_admin = function(req, req, next){
//    if(req.user && req.user.role == 'admin')
//        next()
//    res.status(401).json({});
//}

exports.is_admin = function(req, res, next){
    if(req.session.user){
        if(req.session.user.role === "qc manager" || req.session.user.role === 'admin' || req.session.user.role === 'author' || req.session.user.role === 'product manager'){
            next();
        }else{
            return res.status(401).json({})
        }
    }else if(req.body.pass || req.query.pass){
        if(req.body.pass === "golang-crawler" || req.query.pass === "golang-crawler"){
            next();
        }
    }
    else{
//        return status not logged in
        return res.status(401).json({})
    }
}

exports.create_navigation = create_navigation = function(categories){

    var nav = [];

    var parents = {};

    for(var i = 0; i < categories.length; i++){
        var category = categories[i];

        if(!category.parent){
            var parent_cat = {
                name: category.name,
                slug: category.slug,
                meta_title: category.meta_title,
                meta_description: category.meta_description,
                position: category.position,
                children: []
            }

//            console.log('parent cat'+parent_cat)
            parents[category.name] = parent_cat;
        }


    }

    //sort parents by their assigned position

    var sorted = _.sortBy(parents, 'position');
    
    //after sorting, loop is done to properly arrange for discovery
    var tmp_parents = {}
    for(var i in sorted){
        var c = sorted[i];
        tmp_parents[c.name] = c;
    }
    parents = tmp_parents;
//    console.log('parents only'+parents);

//    add children to parents

    for(var i = 0; i < categories.length; i++){
        var category = categories[i];

        if(category.parent){
            var child = {
                name: category.name,
                slug: category.slug,
                meta_title: category.slug,
                meta_description: category.meta_description
            }

//            console.log('cat parent'+category.parent);
            if(parents[category.parent.name]){
                parents[category.parent.name].children.push(child);
            }else{
                console.log("did not find parent");
            }
                
        }

    }

    for(var key in parents){
        // var n = {key: parents[key]}
        nav.push(parents[key]);
    }

    return nav;




}

var getPath = function(req, k_switch){
    var path = String(req.path);
    var env = String(req.app.get('env'));

    var _switch = k_switch || null;
    console.log("in getPath");
    console.log("query before delete:"+JSON.stringify(req.query))
    // if(_switch){
    //   delete req.query["page"];
    //   console.log("req query after delete: "+JSON.stringify(req.query));
    // }

    var protocol = 'http://'

    var host = (env === 'development') ? 'localhost:3000' : settings.host;

    console.log('registered path: -- '+path);

    var subdomain = '';

    var hasSubdomain = false;

    if(path.indexOf('subdomain') > -1){
        hasSubdomain = true;
    }

    var splitted_domain = path.split('/')

    var queries = '';
//    concatenate req queries.
    for(var propName in req.query){
        if (queries != ''){
            queries += '&';
        }
        if(_switch && propName == "page"){
          continue;
        }
        queries += propName + '=' + req.query[propName]
    }

    if(queries != ''){
        queries = '?' + queries;
    }

    if(hasSubdomain){
        var concantenated_path = '';
        subdomain = splitted_domain[2];
        for(var i = 3; i < splitted_domain.length; i++){
            if(splitted_domain[i]){
                concantenated_path += '/'+splitted_domain[i];
            }

        }



        return protocol+subdomain+'.'+host + concantenated_path + queries
    }

    return protocol + host + path + queries;
}


exports.set_global_template_variables = function(req, res, next){
    Category.find({state: 'active'}).populate('parent').exec(function(error, categories){
        if(!error){
//            req.app.set('nav_menu', create_navigation(categories));



            var nav = create_navigation(categories);

            console.log("navigation"+JSON.stringify(nav))

            Link.getLinksByGroup().then(function(result){
                if(result.error){
//                    print error occurred page
                }else{
//                    console.log(nav);
//                    req.app.set('links', result.links);
                    var links = result.links;

                    req.app.locals({
                        nav_menu: nav,
                        links: links,
                        path: getPath(req),
                        path_without_page: getPath(req, "yes"),
                        settings: settings
                    })

                    console.log(settings.country)

                    //set settings values


                    next();
                }
            })


        }else{
//            return status code error occurred or server down
        }
    })
}


/*
* takes the link to the product on store site and also takes the store price xpath
* in order to download, parse and return price as Number type
* */
exports.download_price = function(link, css_selector){
    console.log('url: '+link);
    console.log('css_selector: '+css_selector)
    link = String(link);
    css_selector = String(css_selector);


//    Browser.visit(link, function(e, browser){
//        if(!e){
//            if(browser.statusCode === 200){
////                console.log('body: '+browser.html(String(css_selector)));
//            var html = browser.html(css_selector);
//            if(html){
//
////                var price_element = browser.html(css_selector);
//
//                var price_element_in_string = String(html);
////                price_element_in_string = price_element_in_string.replace(',', '');
//                console.log(typeof price_element_in_string);
//                console.log('price element: '+price_element_in_string);
//
////                console.log('index '+price_element_in_string.indexOf('20,000'));
////                var second_regex = /^(\d*?)(\.\d{1,2})?/;
////                var first_regex = /^\$?[0-9][0-9,]*[0-9]\.?[0-9]{0,2}/;
//
//                var money_pattern = /\$?[0-9][0-9,]*[0-9]\.?[0-9]{0,2}/;
//
//
//
//                var matched_position = price_element_in_string.search(money_pattern);
//
//                var isValid = matched_position > -1;
//
//                console.log('isValid: '+isValid);
//                if(isValid){
//                    var matched_values = price_element_in_string.match(money_pattern)
//                    console.log('price: '+matched_values)
//
//
//                }else{
//                    console.log('no match found');
//                }
//
//
////                console.log('price element: '+price_element_in_string)
//
////                console.log(browser.html());
//            }else{
////                  console.log(Browser.html());
//                  console.log('selector not found');
//                  return null;
//
//            }
//
//
//            }else{
//                console.log('status code not 200');
//                return null;
//            }
//        }else{
//            console.log('error: '+e);
//        }
//    })

//    var query = 'select * from html where url="'+link+'"';
//    console.log(query);
//    new YQL.exec(query, function(response){
//        var content = JSON.stringify(response);
////        console.log('content: '+content);
////        console.log('price: '+content.match(/\$?[0-9][0-9,]*[0-9]\.?[0-9]{0,2}/));
//        res.json({
//            content: content
////            price: content.match(/\$?[0-9][0-9,]*[0-9]\.?[0-9]{0,2}/)
//        })
////        console.log('response here:'+JSON.stringify(response));
////        console.log('content: '+response.query.results.results.content);
//    })

//    stable version of download price.
    var deferred = Q.defer();

    request(link, function(error, response, body){



        if(!error && response.statusCode == 200){




            var $ = cheerio.load(body);



            console.log(css_selector);

//            TODO check if multiple expath values then find the contained on in body and use it.

            var html = $(css_selector).html()

//            console.log(html: )
            var html_string = String(html);

//            this will find html tags using regex /(<([^>]+)>)/ig and replace it with empty string ''
            var stripped_html = html_string.replace(/(<([^>]+)>)/ig, '')

//            strip the html of html entities
            var stripped_html = stripped_html.replace(/&#([^\s]*);/g, '');

//            var splitted_html = stripped_html.split(' ');

            var price = stripped_html.match(/\$?[0-9][0-9,]*[0-9]\.?[0-9]{0,2}/);

            console.log('stripped html: '+stripped_html);

            console.log('price: '+String(price));
            return deferred.resolve(String(price));

//            res.json({
//                stripped_html: stripped_html,
//                price: String(price)
//            });
        }else{
            return deferred.reject(null);
        }


    })

    return deferred.promise;



}




exports.send_email = function(to, from, subject, email, type){

    var deferred = Q.defer();

    var _from = from || "ridwan@bestprices.ng";
    var _type = type || 'html';

    var email_literal = {
        from: _from,
        to: to,
        subject: subject
    }

    if(_type === 'html'){
        email_literal.html = email;
    }else{
        email_literal.text = email;
    }


    var email = new sendgrid.Email(email_literal)

    sendgrid.send(email, function(error){

        if(error){
            deferred.reject(false);
        }

        deferred.resolve(true)
    });

    deferred.promise;

}


exports.compose_registration_email = function(user){
    var compose_email_template ="<p>Welcome {{first_name}} {{last_name}}</p> \
     <p>Your {{role}} account has been successfully created</p>\
     <p><bold>Credentials</bold></p>\
     <p><bold>Website: </bold> <a href=\"http://bestprices.ng/backoffice/#/login\">http://bestprices.ng/backoffice/#/login</a></p>\
     <p><bold>Username: </bold> {{email}}</p>\
     <p><bold>Password: </bold> **********</p>\
    "

    return mustache.render(compose_email_template, user);
}


exports.replace_short_code = function(content){

    var article_content = content;

    var short_code_pattern = /\[([a-z]+) type=['|"]([a-z\-]+)['|"]\]/gi;

    var short_code_pattern_with_data = /\[([a-z]+) type=['|"]([a-z\-]+)['|"] ?(data\-.+='[a-z0-9\-]+')*\]/gi;

    var match = article_content.match(short_code_pattern_with_data);

    if(match && match.length > 0){
        for(var i = 0; i < match.length; i++){
            //reset pattern in local loop
            var short_code_pattern_with_data = /\[([a-z]+) type=['|"]([a-z\-]+)['|"] ?(data\-.+='[a-z0-9\-]+')*\]/gi;
            console.log('on match number: '+i);
            var short_code = match[i];
            console.log('shortcode testin against:'+short_code);
            console.log(typeof short_code);

            var params = short_code_pattern_with_data.exec(short_code);

            console.log(params);

            var short_code_name = params[1];
            var short_code_type = params[2];

//            holds the data as params for the function
            var data_params = [];

            var size = params.length;

            if(size > 3){
                var data_obj = params.slice(3, size);
                var data_params = String(data_obj).split(" ");
            }

            console.log('type and name: '+short_code_name+' : '+short_code_type)

//          test if the object is a function in order to pass data as params to it
            if(typeof short_code_snippets[short_code_name][short_code_type] == 'function'){
                article_content = article_content.replace(short_code, short_code_snippets[short_code_name][short_code_type](data_params));
            }else{
                article_content = article_content.replace(short_code, short_code_snippets[short_code_name][short_code_type]);
            }


        }

        return article_content;
    }

    return article_content;




}


exports.escape_html_entities = escapeHtmlEntities = function (text) {
    return text.replace(/[\u00A0-\u2666<>\&]/g, function(c) {
        return '&' +
            (escapeHtmlEntities.entityTable[c.charCodeAt(0)] || '#'+c.charCodeAt(0)) + ';';
    });
};

// all HTML4 entities as defined here: http://www.w3.org/TR/html4/sgml/entities.html
// added: amp, lt, gt, quot and apos
escapeHtmlEntities.entityTable = {
    34 : 'quot',
    38 : 'amp',
    39 : 'apos',
    60 : 'lt',
    62 : 'gt',
    160 : 'nbsp',
    161 : 'iexcl',
    162 : 'cent',
    163 : 'pound',
    164 : 'curren',
    165 : 'yen',
    166 : 'brvbar',
    167 : 'sect',
    168 : 'uml',
    169 : 'copy',
    170 : 'ordf',
    171 : 'laquo',
    172 : 'not',
    173 : 'shy',
    174 : 'reg',
    175 : 'macr',
    176 : 'deg',
    177 : 'plusmn',
    178 : 'sup2',
    179 : 'sup3',
    180 : 'acute',
    181 : 'micro',
    182 : 'para',
    183 : 'middot',
    184 : 'cedil',
    185 : 'sup1',
    186 : 'ordm',
    187 : 'raquo',
    188 : 'frac14',
    189 : 'frac12',
    190 : 'frac34',
    191 : 'iquest',
    192 : 'Agrave',
    193 : 'Aacute',
    194 : 'Acirc',
    195 : 'Atilde',
    196 : 'Auml',
    197 : 'Aring',
    198 : 'AElig',
    199 : 'Ccedil',
    200 : 'Egrave',
    201 : 'Eacute',
    202 : 'Ecirc',
    203 : 'Euml',
    204 : 'Igrave',
    205 : 'Iacute',
    206 : 'Icirc',
    207 : 'Iuml',
    208 : 'ETH',
    209 : 'Ntilde',
    210 : 'Ograve',
    211 : 'Oacute',
    212 : 'Ocirc',
    213 : 'Otilde',
    214 : 'Ouml',
    215 : 'times',
    216 : 'Oslash',
    217 : 'Ugrave',
    218 : 'Uacute',
    219 : 'Ucirc',
    220 : 'Uuml',
    221 : 'Yacute',
    222 : 'THORN',
    223 : 'szlig',
    224 : 'agrave',
    225 : 'aacute',
    226 : 'acirc',
    227 : 'atilde',
    228 : 'auml',
    229 : 'aring',
    230 : 'aelig',
    231 : 'ccedil',
    232 : 'egrave',
    233 : 'eacute',
    234 : 'ecirc',
    235 : 'euml',
    236 : 'igrave',
    237 : 'iacute',
    238 : 'icirc',
    239 : 'iuml',
    240 : 'eth',
    241 : 'ntilde',
    242 : 'ograve',
    243 : 'oacute',
    244 : 'ocirc',
    245 : 'otilde',
    246 : 'ouml',
    247 : 'divide',
    248 : 'oslash',
    249 : 'ugrave',
    250 : 'uacute',
    251 : 'ucirc',
    252 : 'uuml',
    253 : 'yacute',
    254 : 'thorn',
    255 : 'yuml',
    402 : 'fnof',
    913 : 'Alpha',
    914 : 'Beta',
    915 : 'Gamma',
    916 : 'Delta',
    917 : 'Epsilon',
    918 : 'Zeta',
    919 : 'Eta',
    920 : 'Theta',
    921 : 'Iota',
    922 : 'Kappa',
    923 : 'Lambda',
    924 : 'Mu',
    925 : 'Nu',
    926 : 'Xi',
    927 : 'Omicron',
    928 : 'Pi',
    929 : 'Rho',
    931 : 'Sigma',
    932 : 'Tau',
    933 : 'Upsilon',
    934 : 'Phi',
    935 : 'Chi',
    936 : 'Psi',
    937 : 'Omega',
    945 : 'alpha',
    946 : 'beta',
    947 : 'gamma',
    948 : 'delta',
    949 : 'epsilon',
    950 : 'zeta',
    951 : 'eta',
    952 : 'theta',
    953 : 'iota',
    954 : 'kappa',
    955 : 'lambda',
    956 : 'mu',
    957 : 'nu',
    958 : 'xi',
    959 : 'omicron',
    960 : 'pi',
    961 : 'rho',
    962 : 'sigmaf',
    963 : 'sigma',
    964 : 'tau',
    965 : 'upsilon',
    966 : 'phi',
    967 : 'chi',
    968 : 'psi',
    969 : 'omega',
    977 : 'thetasym',
    978 : 'upsih',
    982 : 'piv',
    8226 : 'bull',
    8230 : 'hellip',
    8242 : 'prime',
    8243 : 'Prime',
    8254 : 'oline',
    8260 : 'frasl',
    8472 : 'weierp',
    8465 : 'image',
    8476 : 'real',
    8482 : 'trade',
    8501 : 'alefsym',
    8592 : 'larr',
    8593 : 'uarr',
    8594 : 'rarr',
    8595 : 'darr',
    8596 : 'harr',
    8629 : 'crarr',
    8656 : 'lArr',
    8657 : 'uArr',
    8658 : 'rArr',
    8659 : 'dArr',
    8660 : 'hArr',
    8704 : 'forall',
    8706 : 'part',
    8707 : 'exist',
    8709 : 'empty',
    8711 : 'nabla',
    8712 : 'isin',
    8713 : 'notin',
    8715 : 'ni',
    8719 : 'prod',
    8721 : 'sum',
    8722 : 'minus',
    8727 : 'lowast',
    8730 : 'radic',
    8733 : 'prop',
    8734 : 'infin',
    8736 : 'ang',
    8743 : 'and',
    8744 : 'or',
    8745 : 'cap',
    8746 : 'cup',
    8747 : 'int',
    8756 : 'there4',
    8764 : 'sim',
    8773 : 'cong',
    8776 : 'asymp',
    8800 : 'ne',
    8801 : 'equiv',
    8804 : 'le',
    8805 : 'ge',
    8834 : 'sub',
    8835 : 'sup',
    8836 : 'nsub',
    8838 : 'sube',
    8839 : 'supe',
    8853 : 'oplus',
    8855 : 'otimes',
    8869 : 'perp',
    8901 : 'sdot',
    8968 : 'lceil',
    8969 : 'rceil',
    8970 : 'lfloor',
    8971 : 'rfloor',
    9001 : 'lang',
    9002 : 'rang',
    9674 : 'loz',
    9824 : 'spades',
    9827 : 'clubs',
    9829 : 'hearts',
    9830 : 'diams',
    338 : 'OElig',
    339 : 'oelig',
    352 : 'Scaron',
    353 : 'scaron',
    376 : 'Yuml',
    710 : 'circ',
    732 : 'tilde',
    8194 : 'ensp',
    8195 : 'emsp',
    8201 : 'thinsp',
    8204 : 'zwnj',
    8205 : 'zwj',
    8206 : 'lrm',
    8207 : 'rlm',
    8211 : 'ndash',
    8212 : 'mdash',
    8216 : 'lsquo',
    8217 : 'rsquo',
    8218 : 'sbquo',
    8220 : 'ldquo',
    8221 : 'rdquo',
    8222 : 'bdquo',
    8224 : 'dagger',
    8225 : 'Dagger',
    8240 : 'permil',
    8249 : 'lsaquo',
    8250 : 'rsaquo',
    8364 : 'euro'
};
