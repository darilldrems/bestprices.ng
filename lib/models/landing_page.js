var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');



var landingPageSchema = new mongoose.Schema({
    meta_title:{type: String, unique:true, required:true},
    meta_description: {type: String, unique:true, required:true},
    title: {type: String, unique: true, required:true},
    slug: {type:String, unique: true, required:true},
    products: {type: [{type: mongoose.Schema.Types.ObjectId, ref: 'Product'}] },
    banner: String,
    mobile_banner: String,
    description: {type: String, required: true, unique: true},
    created_on: {type: Date, default: Date.now()},
    published_on: {type: Date},
    last_updated_on: Date,
    created_by: {type: mongoose.Schema.Types.ObjectId, ref:'User', required: true},
    page_views: Number,
    status: {type: String, enum: ['published', 'draft']}
});

landingPageSchema.plugin(mongoosePaginate);

var model = mongoose.model('LandingPage', landingPageSchema);

module.exports = model;
