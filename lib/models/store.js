var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var storeSchema = new mongoose.Schema({
    name: {type: String, required: true, unique: true},
    meta_title: String,
    meta_description: {type: String, required: true},
    slug: {type: String, required: true, unique: true, lowercase: true},
    about: {type: String},
    website: {type: String},
    logo: String,
    address: String,
    email: String,
    tel: String,
    is_dynamic: String,
    trusted_shop: {type: Boolean, default: false},
    accepted_payments: String,
    rating: String,
    price_xpath: {type: String, required: true},

    uploaded_on: {type: Date, default: Date.now()},

    affiliate_query: String,

    delivery_note: String,
	search_path: String
    	
});

storeSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Store', storeSchema);
