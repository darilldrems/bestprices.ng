var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var manufacturerSchema = new mongoose.Schema({
    logo: String,
    name: {type: String, required: true, unique: true},
    slug: {type: String, required: true, unique: true, lowercase: true},
    meta_title: {type: String, unique: true, required: true},
    state: {type: String, enum:['active', 'deleted'], default:'active'},
    meta_description: {type: String, required: true},
    about: String,
    created_on: {type: Date, default: Date.now()},
    created_by: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    website: String,
    wikipedia: String,
//    official_website: String,
    founded: String
});


manufacturerSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Manufacturer', manufacturerSchema);