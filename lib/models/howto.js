var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var q = require('q');

var stepsSchema = new mongoose.Schema({
    title: {type: String, required: true},
    content: {type: String, required: true}
})

var howtoSchema = new mongoose.Schema({
    title: {type: String, unique: true, required: true},
    meta_title: String,
    meta_description: {type: String, required: true},
    slug: {type: String, unique: true, required: true, lowercase: true},
    steps: {type: [stepsSchema], required: true},
    category: {type: mongoose.Schema.Types.ObjectId, ref: 'Category'},
    tags: [String],
    summary: {type: String, required: true},
    thumbnail:{type: String, required: true},
    published_on: {type: Date, default: Date.now()},
    created_by: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    state:{type: String, enum:['active', 'deleted'], default:'active'},
    status:{type: String, enum:['published', 'draft'], default:'draft'},
    page_views: Number
});

howtoSchema.plugin(mongoosePaginate);


howtoSchema.statics.getLatest = function(exclude){
    var deferred = q.defer();

    console.log('excluded: '+exclude._id)

    this.model('HowTo').find({status:'published'}).exec(function(error, results){
        var resolved_result = [];
        if(error){
            return deferred.reject({});
        }else{
            for(var i = 0; i < results.length; i++){
                var item = results[i];
                console.log('item: '+item._id);
                if(String(item._id) != String(exclude._id)){
                    resolved_result.push(item);
                }
            }
            return deferred.resolve(resolved_result);
        }


    })

    return deferred.promise;

}

module.exports = mongoose.model('HowTo', howtoSchema);

