var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var sitemapSchema = new mongoose.Schema({
	url: {type: String, unique:true, required:true},
	frequency: String,
	priority: String,
	created_on: {type: Date, default: Date.now()}
});

sitemapSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Sitemap', sitemapSchema);