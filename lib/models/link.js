var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var Q = require('q');


var linkSchema = new mongoose.Schema({
    title: {type: String, required: true, unique: true},
    url: {type: String, required: true},
    target: {type: String, default: ""},
    group: {type: String, default:'default'},
    meta_title: String,
    meta_description: String
})

linkSchema.plugin(mongoosePaginate);

linkSchema.statics.getLinksByGroup = function(){
    var deferred = Q.defer();

    var links_by_group = {}

    this.model('Link').find({}, function(err, links){
        if(err){
            deferred.reject({error: err});
        }else{

//            arrange links by group
            for(var i =0; i < links.length; i++){
                var link = links[i];

                if(!links_by_group[link.group]){
                    links_by_group[link.group] = {};
                }

                links_by_group[link.group][link.title] = link;


            }

            return deferred.resolve({links: links_by_group});


        }


    })


    return deferred.promise;
}

module.exports = mongoose.model('Link', linkSchema);