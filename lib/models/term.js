var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var termSchema = new mongoose.Schema({
	title: {type: String, required: true, unique: true},
	slug: {type: String, required: true, unique: true},
	created_on: {type: Date, default: Date.now()},
	medium: {type: String},
	status: {type: String, enum:['draft', 'published'], default:'draft'}
})

termSchema.plugin(mongoosePaginate);

var model = mongoose.model('Term', termSchema);

module.exports = model;