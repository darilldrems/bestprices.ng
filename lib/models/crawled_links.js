var mongoose = require('mongoose');

var craledLinksSchema = mongoose.Schema({
	url: {type: String, required:true, unique:true},
	state: {type: String, enum:['crawled', 'indexed'], default:'crawled'},
	date_crawled: {type: Date, dafault: Date.now()},
	date_indexed: {type: Date}
});

module.exports = mongoose.model('CrawledLinks', crawledLinksSchema);
