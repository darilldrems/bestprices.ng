var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var q = require('q');

var categorySchema = new mongoose.Schema({
    name: {type: String, required: true,  unique: true},
    meta_title: {type: String, required: true, unique: true},
    meta_description: {type: String, required: true},
    description: {type: String},
    slug: {type: String, required: true, unique: true, lowercase: true},
    parent: {type: mongoose.Schema.Types.ObjectId, ref:'Category'},
    state:{type: String, enum:['active', 'deleted'], default:'active'},
    created_on:{type: Date, default: Date.now()},
    bag_of_words: {type: String},
    position: {type: Number, default:100} //this is used to indicate the position of a category on the menu
});

categorySchema.plugin(mongoosePaginate);

categorySchema.statics.getChildren = function(categoryId){
    var defer = q.defer();
    this.find({parent: categoryId}, function(err, results){
        if(err){
            return defer.reject([]);
        }else{
            return defer.resolve(results);
        }
    })

    return defer.promise;
}

module.exports = mongoose.model('Category', categorySchema);
