var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var q = require('q');

var Manufacturer = require('./manufacturer');

var offerSchema = new mongoose.Schema({
    price: Number,
    store: {type: mongoose.Schema.Types.ObjectId, ref:'Store', required: true},
    link: {type: String, required:true, sparse:true},
    product_state: String,
    affiliate_link: String,
    last_update: Date
})

var productSchema = new mongoose.Schema({
    title: {type: String, required: true},
    meta_title: {type: String, required: true},
    meta_description: {type: String, required: true},
    slug: {type: String, required: true, unique:true, lowercase: true },
    description: {type: String },

    manufacturer: {type: mongoose.Schema.Types.ObjectId, ref:'Manufacturer', required:true},

    uploaded_on: {type: Date, default: Date.now()},
    uploaded_by: {type: mongoose.Schema.Types.ObjectId, ref:'User', required: true},

    wikipedia: {type: String, lowercase: true},
    official_website: {type: String, lowercase: true},

    photos: {type: [String], lowercase: true},
    videos: {type: [String], lowercase: true},

    features_summary:{type: String},
    summary:{type: String},

    tags: {type: [String]},
    category: {type: mongoose.Schema.Types.ObjectId, ref: 'Category', required: true},

    page_views: {type:Number, default:0},
    last_updated: Date,

    status: {type: String, enum:['published', 'draft'], default: 'draft'},
    state:{type: String, enum:['active', 'deleted'], default:'active'},

    offers: {type: [offerSchema], sparse: true, required: false, unique: false},

    is_featured: {type: Boolean, default: false},

    is_hot: {type: Boolean, default: false},

    offers_last_updated: Date,

    ranking: {type:Number, default:0}

});

productSchema.index({description: 'text', title: 'text', tags: 'text', summary:'text', manufacturer: 'text'}, {weights: {title: 10, description: 5}})

productSchema.plugin(mongoosePaginate);

productSchema.statics.findFeaturedProductsFromCategory = function(id){
    // console.log("slug is "+slug);
    var deferred = q.defer();


    this.find({category: id, status:'published'}).sort('-ranking').populate("offers.store category manufacturer store").limit(5).exec(function(err, products){
        if(err){
            console.log('error'+err);
            return deferred.reject([]);
        }else{
            console.log('no error'+JSON.stringify(products));
            return deferred.resolve(products);
        }
    })

    return deferred.promise;
}


productSchema.statics.findHotProducts = function(){
    var deferred = q.defer();

    this.find({status: 'published'}).sort('-ranking').populate("offers.store category manufacturer store").limit(6).exec(function(err, products){
        if(err){
            console.log('error'+err);
            return deferred.reject([]);
        }else{
            console.log('no error'+JSON.stringify(products));
            return deferred.resolve(products);
        }
    });

    return deferred.promise;
}


productSchema.statics.findManufacturers = function(cond){
    var deferred = q.defer();

    var condition = cond || {};

    var query = this.find(condition).distinct('manufacturer');

    query.exec(function(err, data){
        if(err){
            return deferred.reject([]);
        }else{
            console.log(JSON.stringify(data));
            Manufacturer.find({_id: {$in: data}}, function(err, result){
                if(err){
                    return deferred.reject([]);
                }else{
                    return deferred.resolve(result);
                }
            })


        }
    })

    return deferred.promise;
}


var model = mongoose.model('Product', productSchema);

//model.collection.ensureIndex({title: 'text', description: 'text', tags:'text', slug: 'text'});



module.exports = model;
