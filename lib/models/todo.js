var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var todoSchema = new mongoose.Schema({
	title: {type: String, required: true, unique:true, lowercase: true},
	category: {type: String, required: true},
	features: {type: String},
	wikipedia: {type: String},
	official_url: String,
	manufacturer: {type: String, required: true},
	ranking: String, 
	status: {type: String, enum: ['pending', 'done'], default:'pending'},
	uploaded_on: {type: Date, default:Date.now()}
});

todoSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Todo', todoSchema);
