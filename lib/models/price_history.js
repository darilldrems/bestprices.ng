var mongoose = require('mongoose');

var mongoosePaginate = require('mongoose-paginate');

var priceHistorySchema = new mongoose.Schema({
  product_title: {type: String, required:true},
  product_id: {type: String},
  price: Number,
  store_name: String,
  category: String,
  created_on: {type: Date, default: Date.now()}
});

module.exports = mongoose.model('PriceHistory', priceHistorySchema);
