var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

subscribers_types = ['pre-launch', 'updates']

var subscriberSchema = new mongoose.Schema({
    email: {type: String, required: true, unique: true, lowercase:true, match:[/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address']},
    status: {type: String, enum:['subscribe', 'un-subscribed'], default: 'subscribed'},
    subscribe_for: {type: String, enum:subscribers_types, required:true},
    subscribed_on: {type: Date, default: Date.now()},
    un_subscribed_on: Date
});

subscriberSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Subscriber', subscriberSchema);