var mongoose = require('mongoose');

var queryStringSchema = new mongoose.Schema({
    query: {type: String, required: true, lowercase: true},
    count: Number
});

module.exports = mongoose.model('QueryString', queryStringSchema);