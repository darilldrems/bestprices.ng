var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var imageSchema = new mongoose.Schema({
    path: {type: String, required: true, unique: true},
    alt: String,
    title: String,
    uploaded_on: {type: Date, default: Date.now()},
    state: {type: String, enum:['active', 'deleted'], default:'active'},
    uploaded_by: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
});

imageSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Image', imageSchema);