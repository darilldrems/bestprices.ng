var mongoose = require('mongoose');

var commentSchema = new mongoose.Schema({
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    product: {type: mongoose.Schema.Types.ObjectId, ref: 'Product'},
    comment: String,
    published: {type: Date, default: Date.now()}
});

module.exports = mongoose.model('Comment', commentSchema);