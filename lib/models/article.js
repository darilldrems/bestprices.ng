var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var q = require('q');

var articleSchema = new mongoose.Schema({
    title: {type: String, required: true, unique: true},
    content: {type: String, required: true},

    slug: {type: String, required: true, unique: true, lowercase: true},
    meta_title: {type: String, required: true},
    meta_description: {type: String, required: true},

    summary: {type: String, required: true},

    thumbnail:{type: String, required: true},

    article_type: {type: String, enum:['Reviews', 'News', 'Top10', 'Blog'], required: true},

    status: {type: String, enum:['published', 'draft'], required: true, default: 'draft'},

    state: {type: String, enum: ['active', 'deleted'], default:'active'},

    tags: [String],
    category: {type: mongoose.Schema.Types.ObjectId, ref:'Category'},

    published_on: {type: Date, default: Date.now()},
    published_by: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},

    page_views: Number

});

articleSchema.plugin(mongoosePaginate);

articleSchema.statics.getLatest = function(type, exclude){
    var deferred = q.defer();

//    console.log('excluded: '+exclude._id)

    var excluded = exclude;

    console.log('exclude: '+JSON.stringify(exclude));


    this.model('Article').find({status:'published', article_type:type}).exec(function(error, results){

        var resolved_result = [];
        if(error){
            return deferred.reject({});
        }else{
            for(var i = 0; i < results.length; i++){
                var item = results[i];
                console.log('item: '+item._id);
                if(String(item._id) != String(excluded._id)){
                    resolved_result.push(item);
                }
            }
            return deferred.resolve(resolved_result);
        }


    })

    return deferred.promise;

}

module.exports = mongoose.model('Article', articleSchema);