var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

user_roles = ['admin', 'visitor', 'store owner', 'author', 'product manager', 'qc manager']

var userSchema = new mongoose.Schema({
    email: {type: String, required: true, unique: true, lowercase:true, match:[/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address']},
    role:{type: String, enum: user_roles, required: true},
    password: {type: String, required: true},
    state:{type: String, required: true, enum:['active', 'deleted'], default: 'active'},
    store: {type: mongoose.Schema.Types.ObjectId, ref: 'Store'},
    first_name: String,
    last_name: String,
    photo: String,
    fb_url: String,
    twitter_handle: String,
    google_plus: String,
    created_on: {type: Date, default: Date.now()}
});

userSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('User', userSchema);