var google_ads_snippets = require('./googleads');


function convertDataToArray(rData){
    var rawData = rData || [];

    var data = {};

    for(var i =0; i < rawData.length; i++){
        var iter = rawData[i];

        var splited_iter = iter.split('=');
//        alert(splited_iter[0]);
//        alert(splited_iter[1]);
        data[splited_iter[0]] = splited_iter[1].split("'")[1];


    }
//    alert(JSON.stringify(data));

    return data;

}


var email_subscribe_string = '<div id="email-subscribe-box" style="margin-left:auto; margin-right:auto;margin-top:10px;background-color:#f8f8f8;" class="standard-padding pure-u-1 pure-u-md-12-24"> \
    <div style="margin-left:auto; margin-right:auto;"> \
    <p class="no-margin">Get Email Updates (It\'s Free)</p> \
    <div id="email-subscribe-message" style="display:none;" class="pure-u-1 text-color-red"> \
                <p>Thank You</p> \
            </div> \
    <form class="pure-form" id="email-subscribe-form"> \
    <fieldset> \
        <div id="subscribe-input-container" class="pure-g"> \
            <div class="pure-u-md-18-24 pure-u-1"> \
                <input type="email" id="subscribe-email-textfield" placeholder="Enter your Email Address" class="pure-u-1"/> \
            </div> \
            <div class="pure-u-md-2-24 pure-u-1 margin-left-5"> \
                <input type="submit" id="subscribe-email-button" name="subscribe-email-button" class="pure-button button-red"/> \
            </div> \
        </div> \
    </fieldset> \
    </form> \
</div> \
</div>'

var twitter_string = '<div id="social-embed-component" class="pure-g" style="background-color: inherit">\
        <div class="pure-u-1">\
        <p>Follow us on twitter for more</p>\
        <p><a href="https://twitter.com/bestprices_ng" class="twitter-follow-button">follow us @twitter</a></p>\
</div>\
</div>';


var facebook_string = '<div id="social-embed-component" class="pure-g" style="background-color: inherit">' +
    '<div class="pure-u-1" style="text-align: center;"><p>' +
    '<div class="fb-page" data-href="https://www.facebook.com/bestpricesng" data-hide-cover="false" data-show-facepile="true" data-show-posts="false">' +
    '<div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/bestpricesng"><a href="https://www.facebook.com/bestpricesng">Bestprices</a>' +
    '</blockquote></div></div></p></div></div>';

var social_media_types = {
    'twitter': twitter_string,
    'facebook': facebook_string
}


var components_types = {
    offers: function(d){
        var obj = convertDataToArray(d);

        return '<div class="embed-component" id="'+obj['data-id']+'"></div>'

    }
}

var subscribe_type = {
    email: email_subscribe_string
}

//snippet receives object of objects for passing both short code name and type and can also receive function in order to pass data-id
snippets = {
    'googleads': google_ads_snippets,
    'socialmedia': social_media_types,
    'subscribebox': subscribe_type,
    'components': components_types
}




module.exports = snippets;