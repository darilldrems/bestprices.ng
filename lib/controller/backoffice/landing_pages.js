var LandingPage = require('../../models/landing_page');
var moment = require('moment');

exports.create = function(req, res){
  var new_landing_page = new LandingPage(req.body);

  new_landing_page.created_by = req.session.user.id;
  new_landing_page.created_on = moment().toDate();
  new_landing_page.last_updated_on = moment().toDate();

  new_landing_page.save(function(err, obj){
    if(err){
        return res.json({
            error: err
        });
    }else{
        return res.json({
            landing_page: obj
        });
    }
  });

}


exports.list = function(req, res){
  var page = req.query.page || 1;

  LandingPage.paginate({}, page, req.app.get('total_items_per_page'),
          function(error, pageCount, paginatedResults, itemCount){
            if(error){
                return res.json({
                    error: error
                });
            }else{
                return res.json({
                    landing_pages: paginatedResults,
                    item_count: itemCount,
                    page_count: pageCount
                })
            }
          }, {populate: 'products', sortBy: {created_on: -1}});
}

exports.get = function(req, res){
  var id = req.params.id;

  LandingPage.findById(id).populate('products').exec(function(err, obj){
    if(err){
        return res.json({
            error: err
        });
    }else{
        return res.json({landing_page: obj});
    }
  });

}

exports.edit = function(req, res){
  var id = req.params.id;
  delete req.body._id;

  var ids = [];

  for(var i in req.body.products){
    ids.push(req.body.products[i]._id);
  }

  delete req.body.products;
  req.body.products = ids;

  console.log("body content here:"+JSON.stringify(req.body));

  LandingPage.findByIdAndUpdate(id, {$set: req.body}).exec(function(err, obj){
    if(err){
        return res.json({error: err});
    }else{
        return res.json({
            landing_page: obj
        });
    }
  })
}
