var User = require('../../models/user');
var md5 = require('MD5');

exports.login = function(req, res){
    User.findOne({
        email: req.body.email,
        password: md5(req.body.password),
        state: 'active'
    }).exec(function(err, u){
            if(err){
                res.json({error: 'error occurred'});
            }else{
                if(u){
                    console.log("role is: "+u.role);
                    if(u.role === 'qc manager' || u.role === 'admin' || u.role === 'author' || u.role === 'product manager'){
                        console.log("user passed");
                        var logged_in_user = {
                            id: u._id,
                            role: u.role,
                            email: u.email,
                            first_name: u.first_name,
                            last_name: u.last_name,
                            photo: u.photo
                        }
                        req.session.user = logged_in_user;
                        return res.json({
                            user: logged_in_user
                        })
                    }else{
                        return res.json({error: "user not found"})
                    }
                }else{
                    return res.json({error: "user not found"})
                }
            }
        })
}

exports.logout = function(req, res){
    req.session.user = '';
    return res.json({success: 'logged out'})
}

