var Sitemap = require('../../models/sitemap');

exports.create = function(req, res){
	
	var url = req.body.url;
	var frequency = req.body.frequency;
	var priority = req.body.priority;
	


	if(!url)
		return res.json({error: {message: "Url is required"}});

	if(!frequency)
		return res.json({error: {message: "frequency is required"}});

	if(!priority)
		return res.json({error: {message: "priority is required"}});

	var sitemap = new Sitemap();
	sitemap.url = url;
	sitemap.frequency = frequency;
	sitemap.priority = priority;
	
	
	sitemap.save(function(err, saved){
		if(err)
			return res.json({error: err});
		
		return res.json({sitemap: saved});

	});

}


exports.delete = function(req, res){
	var id = req.params.id;
	
	Sitemap.remove({_id: id}, function(err){
		if(err)
			return res.json({error: err});
		return res.json({sitemap:"deleted"});

	});		


}

exports.list = function(req, res){
	var page_no = req.query.page || 1;

	var condition = req.query.condition || {};
	
	var total_items = 20;
	Sitemap.paginate(condition, page_no, total_items, function(err, pageCount, results, itemCount){
		if(err)
			return res.json({error: err});
		return res.json({ sitemaps: results, item_count: itemCount, page_count: pageCount });
	});
}
