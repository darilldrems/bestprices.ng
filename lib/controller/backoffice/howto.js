var HowTo = require('../../models/howto');


exports.create = function(req, res){

    var howto = new HowTo(req.body);

    howto.slug = howto.slug.trim()
    howto.slug = howto.slug.split(' ').join('-')

    howto.created_by = req.session.user.id;

    howto.tags = String(howto.tags).split(',')

    console.log(howto);

    howto.save(function(err, obj){
        if(err){
            return res.json({
                error: err
            });
        }else{
            req.app.get('events').emit('howto-created', obj);
            return res.json({
                howto: obj
            });
        }
    });

}


exports.list = function(req, res){
    var page = 1;
    var condition = {
        state: 'active'
    }

    if(req.query.page){
        page = req.query.page;
    }

    if(req.session.user.role === 'author'){

        condition.created_by = req.session.user.id;
    }


    HowTo.count({status: 'published'}, function(err, count){
        var total_published_howto = count;

        HowTo
        .paginate(condition, page, req.app.get('total_items_per_page'), function(err, pageCount, results, itemCount){
            if(err){
                return res.json({error: err});
            }else{
                return res.json({
                    howtos: results,
                    item_count: itemCount,
                    page_count: pageCount,
                    published_howtos: total_published_howto
                });
            }
        }, {populate: 'category created_by', sortBy: { published_on: -1}})
    })

    
}

exports.get = function(req, res){
    var id = req.params.id;


    HowTo.findById(id, function(err, obj){
        if(err){
            return res.json({
                error: err
            })
        }else{
            return res.json({
                howto: obj
            })
        }
    })
}

exports.delete = function(req, res){

    var id = req.params.id;

    if(id){
        HowTo.findById(id, function(error, obj){
            if(error){
                return res.json({
                    error: error
                })
            }else{
                obj.state = 'deleted';
                obj.save(function(err, obj){
                    if(error){
                        return res.json({error: err})
                    }else{
                        return res.json({howto: obj})
                    }
                })
            }
        })
    }

}

exports.edit = function(req, res){
    var id = req.params.id;

    delete req.body._id;

    req.body.tags = String(req.body.tags).split(',');
    req.body.published_on = Date.now();

    HowTo.findByIdAndUpdate(id, {$set: req.body}, function(err, obj){
        if(err){
            return res.json({error: err});
        }else{
            req.app.get('events').emit('howto-edited', obj);
            return res.json({howto: obj});
        }
    })
}


exports.search = function(req, res){
    var query = req.params.query;

    HowTo.find({title: RegExp(query, "i")}, function(error, result){
        if(error){
            return res.json({error: error});
        }else{
            return res.json({result: result});
        }
    })
}

exports.bulk_delete = function(req, res){
    var ids = req.body.ids;
    console.log('hello boys')
//    console.log(ids);
//    return res.json();
    HowTo.find({_id: {$in: ids}}, function(error, objs){
        if(error){
            console.log('in error');
            return res.json({success: 0});

        }else{
            console.log('found items: '+objs);
            for(var i = 0; i < objs.length; i++){
                var p = objs[i];

                p.remove().exec()
            }

            return res.json({success: 1});
        }

    });

}
