var Term = require('../../models/term');

exports.create = function(req, res){
	var new_term = new Term(req.body);

	new_term.save(function(err, term){
		if(err){
			return res.json({error: err});
		}else{
			return res.json({term: term});
		}
	})

}

exports.get = function(req, res){
	var id = req.params.id;

	Term.findById(id, function(error, obj){
		if(error){
            return res.json({
                error: error
            })
        }else{
            return res.json({
                term: obj
            })
        }
	})
}

exports.edit = function(req, res){
	var id = req.params.id;

	delete req.body._id;

	Term.findByIdAndUpdate(id, {$set: req.body}, function(err, obj){
		if(err){
			return res.json({
				error: err
			})
		}else{
			return res.json({
				term: obj
			})
		}
	})
}

exports.list = function(req, res){
	var page = req.query.page || 1;

	Term.paginate({}, page, 30, function(error, pageCount, paginatedResult, itemCount){
		if(error){
                return res.json({
                    error: error
                });
            }else{
                return res.json({
                    terms: paginatedResult,
                    item_count: itemCount,
                    page_count: pageCount
                })
            }
        }, {sortBy: {created_on:-1}})
}