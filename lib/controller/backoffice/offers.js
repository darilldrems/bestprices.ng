var Product = require('../../models/product');
var download_price = require('../../helpers').download_price;
var Store = require('../../models/store');


exports.create = function(req, res){
    var product_id = req.query.product;
    var store_id = req.query.store;
    var link = req.query.link;

    var new_offer = {
        store: store_id,
        link: link,
        last_update: Date.now()
    };

    if(req.query.price){
        new_offer.price = req.query.price;

        Product.findById(product_id, function(error, product){
            if(!error && product){
                //this is used to prevent duplicate offers
                var offer_exist = false;

                for(var i in product.offers){
                    var existing_offer = product.offers[i];
                    if(existing_offer.link == new_offer.link)
                        offer_exist = true;
                }

                if(!offer_exist)
                    product.offers.push(new_offer);

                product.save(function(error, saved){
                    if(error){
                        return res.json({error: error});
                    }else{
                        return res.json({product: saved})
                    }
                })
            }else{
                return res.json({error: 'product not found'});
            }
        })

    }else{
        Store.findById(store_id, function(error, store){
            if(!error && store){

                download_price(link, store.price_xpath).then(function(result){

                    if(result != null){
                        console.log('in save offer')
                        console.log('price result'+result);

                        new_offer.price = Number(result.split(',').join(''))
//                    new_offer.price = Number(result);

                        Product.findById(product_id, function(error, product){
                            if(!error){
                                product.offers.push(new_offer);
                                product.save(function(error, saved){
                                    if(error){
                                        return res.json({error: error});
                                    }else{
                                        return res.json({
                                            product: saved
                                        });
                                    }
                                })
                            }else{
                                return res.json({error: error});
                            }
                        })

                    }else{
//                  TODO:  raise alert that price was not found for this store
                        return res.json({error: 'price was null'})
                    }


                });


            }else{
                console.log(error);
                return res.json({error: error})
            }

        })
    }





}

exports.delete = function(req, res){
    var product_id = req.params.product_id;
    var offer_id = req.params.offer_id;
    var bulk_offers_ids = req.query.offers_ids || [];

    console.log("queries: "+JSON.stringify(req.query))

    console.log("offers ids"+JSON.stringify(bulk_offers_ids));

    Product.findById(product_id).populate('offers.store category').exec(function(error, product){
        if(!error){
            if(bulk_offers_ids.length > 0){
                for(var i =0; i < bulk_offers_ids.length; i++){
                    product.offers.pull({_id: bulk_offers_ids[i]});
                }
            }else{
                product.offers.pull({_id: offer_id})
            }

            
            product.save(function(error, saved){
                if(!error){
                    return res.json({
                        status: 'success',
                        product: saved
                    })
                }else{
                    return res.json({error: error});
                }
            })
        }else{
            return res.json({error: error});
        }
    })


}

