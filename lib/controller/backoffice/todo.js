var Todo = require('../../models/todo');

exports.create = function(req, res){
	
	var title = req.body.title;
	var category = req.body.category;
	var features = req.body.features;
	var wikipedia = req.body.wikipedia;
	var official_url = req.body.official_url;
	var manufacturer = req.body.manufacturer;
	var ranking = req.body.ranking;	


	if(!title)
		return res.json({error: {message: "Title is required"}});

	if(!category)
		return res.json({error: {message: "Category is required"}});

	if(!manufacturer)
		return res.json({error: {message: "Manufacturer is required"}});

	var todo = new Todo();
	todo.title = title;
	todo.category = category;
	todo.features = features;
	todo.wikipedia = wikipedia;
	todo.official_url = official_url;
	todo.manufacturer = manufacturer;
	todo.ranking = ranking;
	
	todo.save(function(err, saved){
		if(err)
			return res.json({error: err});
		
		return res.json({todo: saved});

	});

}


exports.delete = function(req, res){
	var id = req.params.id;
	
	Todo.remove({_id: id}, function(err){
		if(err)
			return res.json({error: err});
		return res.json({todo:"deleted"});

	});		


}

exports.list = function(req, res){
	var page_no = req.query.page || 1;

	var condition = req.query.condition || {};
	
	var total_items = 20;
	Todo.paginate(condition, page_no, total_items, function(err, pageCount, results, itemCount){
		if(err)
			return res.json({error: err});
		return res.json({ todos: results, item_count: itemCount, page_count: pageCount });
	});
}
