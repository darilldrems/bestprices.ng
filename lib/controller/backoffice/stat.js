var mongoose = require('mongoose');
var Subscriber = require('../../models/subscriber');
require('datejs');

module.exports = function(req, res){

    if(req.query.doc == 'doc'){
        return res.json({
            'title': 'this will display the stat elements',
            'query': 'days_ago(for selecting how many days ago to use)',
            'elements': {
                'subscribers_by_status': '',
                'total_subscribers_per_day': '',
                'total_un_subscribers_per_day': ''
            }
        })
    }

    var days_ago = 7;

    if(req.query.days_ago)
        days_ago = parseInt(req.query.days_ago)

    var total_subscribers_per_day = null;
    var total_unsubscribed_emails_per_day = null;
    var subscribers_by_status = null;


    Subscriber.aggregate({
        $group: {
            _id: '$status',
            total: {$sum:1}
        }
    }).exec(function(err, result){
            subscribers_by_status = result;

            Subscriber.aggregate([
                {$match: {status: 'subscribe', subscribed_on: {$gt: (days_ago).days().ago()} } },
                {
                    $group:{
                        _id: {day: {$dayOfMonth: '$subscribed_on'}},
                        total: {$sum:1}
                    }
                }
            ]).exec(function(err, result){
                total_subscribers_per_day = result

                    Subscriber.aggregate([
                            {$match: {status: 'un-subscribed', un_subscribed_on: {$gt: (days_ago).days().ago()} } },
                            {
                                $group:{
                                    _id: {day: {$dayOfMonth: '$un_subscribed_on'}},
                                    total: {$sum:1}
                                }
                            }
                        ]).exec(function(err, result){
                            if(err)
                                console.log(err);
                            total_unsubscribed_emails_per_day = result

                            return res.json({
                                subscribers_by_status: subscribers_by_status,
                                total_subscribers_per_day: total_subscribers_per_day,
                                total_unsubscribed_emails_per_day: total_unsubscribed_emails_per_day
                            })
                        });


                });





        })



}