var Product = require('../../models/product');
//var Offer = require('../../models/offer');
var moment = require('moment');


exports.create = function(req, res){

    var new_product = new Product(req.body);

    console.log('body'+JSON.stringify(req.body))

    console.log('uploaded by'+req.session.user.id)
    new_product.uploaded_by = req.session.user.id;

    if(new_product.videos != ''){
        new_product.videos = String(new_product.videos).split(",");
    }

    if(new_product.tags != ''){
        new_product.tags = String(new_product.tags).split(",");
    }

    new_product.slug = new_product.slug.trim();
    new_product.slug = new_product.slug.split(' ').join('-');
    new_product.offers_last_updated = moment().toDate();

    console.log(new_product);

    new_product.save(function(err, obj){
        if(err){
            return res.json({
                error: err
            });
        }else{
            req.app.get('events').emit('product-created', obj);
            return res.json({
                product: obj
            });
        }
    });


}

exports.get_products_todo = function(req, res){
    Product.paginate({'offers.0': {$exists: false}}, 1, 50, function(error, pageCount, paginatedResults, itemCount){
                if(error){
                    return res.json({
                        error: error
                    });
                }else{
                    return res.json({
                        products: paginatedResults,
                        item_count: itemCount,
                        page_count: pageCount
                    })
                }
    }, {sortBy:{uploaded_on: -1}});

}


exports.get = function(req, res){

    var id = req.params.id;


    Product.findById(id).populate('offers.store').exec(function(err, obj){
        if(err){
            return res.json({
                error: err
            })
        }else{
            return res.json({product: obj});


        }
    })


}


exports.edit = function(req, res){
    var id = req.params.id;

    delete req.body._id;
    delete req.body.offers;

//    req.body.category = req.body.category._id;

    Product.findByIdAndUpdate(id, {$set: req.body}).exec(function(err, obj){
        if(err){
            return res.json({error: err});
        }else{
            req.app.get('events').emit('index-product', id);
            return res.json({
                product: obj
            });
        }

    })

}





exports.delete = function(req, res){
    var id = req.params.id;

    Product.find({ _id:id }).remove().exec();

    return res.json({
        product: {

        }
    });

    // Product.findById(id, function(error, obj){
    //     if(error){
    //         return res.json({
    //             error: error
    //         });
    //     }else{

    //         Pfind({ id:333 }).remove().exec();
    //         obj.state = 'deleted'
    //         obj.save(function(error, obj){
    //             if(error){
    //                 return res.json({
    //                     error: error
    //                 })
    //             }else{
    //                 return res.json({
    //                     product: obj
    //                 })
    //             }
    //         });
    //     }
    // });
}

exports.remove_all_with_state_delete = function(req, res){

    Product.find({state: "deleted"}, function(err, products){

        if(err)
            return res.json({status: "done with error"});

        for(var i in products){
            var product_id = products[i]._id;

            Product.find({ _id:product_id }).remove().exec();
        }

        return res.json({status: "done successfully"});
    })

}


exports.bulk_delete = function(req, res){
    var ids = req.body.ids;
    console.log('hello boys')
//    console.log(ids);
//    return res.json();

    // for(var i = 0; i < ids.length; i++){
    //     var id = ids[i];

    //     Product.find({ _id:id }).remove().exec();
    // }
    Product.find({_id: {$in: ids}}, function(error, objs){
        if(error){
            console.log('in error');
            return res.json({success: 0});

        }else{
            console.log('found items: '+objs);
            for(var i = 0; i < objs.length; i++){
                var p = objs[i];

                p.remove().exec()
            }

            return res.json({success: 1});
        }

    });

}


exports.list = function(req, res){

    var condition = {
        state: 'active'
    }
    var sort_by = {uploaded_on: -1}
////    socket.emit('in products list', {});
   if(req.query.condition){
        console.log("seen condition")
       // for(var key in req.query.condition){
       //      console.log(key+"::::"+req.query.condition[key]);
       // }
       // console.log(JSON.stringify(req.query.condition))
       condition = JSON.parse(req.query.condition);

       if(condition["offers.0"]){
            condition["offers.0"] = {$exists: false};
       }
       // console.log(JSON.stringify(condition))
       if(condition["uploaded_on"]){
            var today = moment().startOf('day'),
                tomorrow = moment(today).add(1, 'days');
            condition["uploaded_on"] = {
                  $gte: today.toDate(),
                  $lt: tomorrow.toDate()
                }
       }

       if(condition["offers_last_updated"]){
          delete condition.offers_last_updated;
          sort_by = {offers_last_updated: -1};
       }

       console.log(JSON.stringify(condition));
   }
    var page = 1;

    if(req.query.page){
        page = req.query.page
    }

    if(req.session.user.role === 'product manager'){
        condition.uploaded_by = req.session.user.id;
    }


    // if (req.query.filter){
    //     for(var key in req.query.filter){

    //         condition[key] = req.query.filter[key];
    //     }
    // }

    console.log(JSON.stringify(condition))

    Product
        .paginate(condition, page, req.app.get('total_items_per_page'),
            function(error, pageCount, paginatedResults, itemCount){
                if(error){
                    return res.json({
                        error: error
                    });
                }else{
                    return res.json({
                        products: paginatedResults,
                        item_count: itemCount,
                        page_count: pageCount
                    })
                }
    }, {populate: 'category manufacturer uploaded_by', sortBy: sort_by});




}

exports.search = function(req, res){
    var query = req.params.query;

    Product
        .find({title: new RegExp(query, "i")})
        .populate('category manufacturer uploaded_by')
        .exec(function(err, obj){
            if(err){
                return res.json({error: err});
            }else{
                return res.json({result: obj});
            }
        })
}
