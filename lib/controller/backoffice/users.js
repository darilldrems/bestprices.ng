var User = require('../../models/user');
var md5 = require('MD5')

var events = require('events');
var eventsEmitter = new events.EventEmitter();

//using paginate mondule list the users detail for backoffice to edit
exports.list = function(req, res){
    var page = 1;
    var total_per_page = 10;

    var condition = {
        state: 'active'
    }

    if(req.query.page)
        page = req.query.page

    if(req.query.total){
        total_per_page = null;
    }

    User
        .paginate(condition, page, total_per_page, function(err, pageCount, users, itemCount){
            if(err){
                return res.json({error: err})
            }else{
                return res.json({
                    users: users,
                    page_count: pageCount,
                    item_count: itemCount
                });
            }
        }, {sortBy: {created_on: -1}});
}


exports.user_roles = function(req, res){
    var roles = User.schema.path('role').enumValues;

    return res.json(roles);
}

//edit user details
exports.edit = function(req, res){

    delete req.body._id;

    User.findByIdAndUpdate(req.params.id, {$set:req.body}, function(err, u){
        if(err){
            return res.json({error: err});
        }else{
            return res.json({user: u});
        }
    })
}


//create new user
exports.create = function(req, res){
    req.body.password = md5(req.body.password)

    var user = new User(req.body)

    user.save(function(err, u){
        if(err){
            return res.json({error: err})
        }else{
            req.app.get('events').emit('user-created', u);
            return res.json({user: u});
        }
    })
}


exports.get = function(req, res){
    var id = req.params.id;

    User.findById(id, function(err, u){
        if(err){
            return res.json({
                error: err
            })
        }else{
            return res.json({
                user: u
            })
        }
    })
}


exports.bulk_delete = function(req, res){
    var ids = req.body.ids;

    User.find({_id: {$in: ids}}, function(error, objs){
        if(error){
            console.log('in error');
            return res.json({success: 0});

        }else{
            console.log('found items: '+objs);
            for(var i = 0; i < objs.length; i++){
                var p = objs[i];

                p.remove().exec()
            }

            return res.json({success: 1});
        }

    });

}

//var Article = require('../../models/article');
//exports.ngo = function(req, res){
//    var id = "5502fd326c3f80383f154078";
//
//    Article.update({published_by: id}, {$set: {published_by: "550172d7808aeacc17353a90"}}).exec(function(err){
//        if(err){
//            console.log(err);
//            return res.send('not done');
//        }else{
//            Article.count({published_by: id}, function(er, c){
//                return res.send(c+" counted");
//            })
////            return res.send('done');
//        }
//
//    })
//
//
//}