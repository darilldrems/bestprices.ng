var Product = require('../../models/product');
var User = require('../../models/user');
var Store = require('../../models/store');
var Subscriber = require('../../models/subscriber');
var Article = require('../../models/article');
var HowTo = require('../../models/howto');
var Manufacturer = require('../../models/manufacturer');

var settings = require('../../../settings');
var request = require('request');

module.exports = function(req, res){

    var products_count = 0;
    var stores_count = 0
    var users_count = 0;
    var manufacturers = 0;
//    var subscribers_count = 0;
    var howto_count = 0;
    var articles_count_by_type = {};
    var subscribed_unsubscribed = {};

    var condition = {state: 'active'}

    Product.count(condition, function(err, count){
        if(count) products_count = count;

        Store.count(condition, function(err, count){
            if(count) stores_count = count;

            HowTo.count(condition, function(err, c){
                if(c) howto_count = c;

                Subscriber.aggregate({
                    $group: {_id: '$status', count:{ $sum: 1 } }
                }, function(err, result){
                    if(result){
                        for(var i = 0; i < result.length; i++){
                            subscribed_unsubscribed[result[i]._id] = result[i].count;
                        }
                    }

                    User.count(condition, function(err, c){
                        if(c) users_count = c;

                        Article.aggregate({
                            $group: {_id: '$article_type', count:{ $sum: 1}}
                        }, function(err, result){
                            if(result) {
                                for(var i = 0; i < result.length; i++){
                                    articles_count_by_type[result[i]._id] = result[i].count;
                                }

                            }

                            Manufacturer.count(condition, function(err, c){
                                manufacturers = c;
                                

                                var solr_url = "http://"+settings.solr.ip+":"+settings.solr.port+settings.solr.path+"/"+settings.solr.core+"/"+"select?q=*%3A*&wt=json&indent=true"
                                request({url:solr_url}, function(err, response, body){

                                            body = JSON.parse(body);
                                            var total_indexed = body["response"]["numFound"];


                                            Product.count({status: 'published'}, function(err, c){
                                                var published_products = c;

                                                Product.count({"offers.0": {$exists: false}}, function(err, oc){
                                                    var no_offers = oc;
                                                    console.log("oc is "+JSON.stringify(err));

                                                    return res.json({
                                                        manufacturers: manufacturers,
                                                        products: products_count,
                                                        users: users_count,
                                                        stores: stores_count,
                                                        howto: howto_count,
                                                        subscribers: subscribed_unsubscribed,
                                                        articles: articles_count_by_type,
                                                        products_no_offer: no_offers,
                                                        products_indexed: total_indexed,
                                                        published_products: published_products
                                                    })

                                                })


                                            })

                                            




                                        })
                                

                                

                            })
                        })

                    })

                })

            })
        })


    })


}