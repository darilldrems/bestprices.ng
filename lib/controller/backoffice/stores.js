var Store = require('../../models/store');


exports.create = function(req, res){

    var new_store = new Store(req.body);

    new_store.slug = new_store.slug.trim()
    new_store.slug = new_store.slug.split(' ').join('-')

    new_store.save(function(error, obj){
        if(error){
            return res.json({
                error: error
            });
        }else{
            req.app.get('events').emit('store-created', obj);
            return res.json({
                store: obj
            })
        }

    })
}



exports.get = function(req, res){
    var id = req.params.id;


    Store.findById(id, function(err, obj){
        if(err){
            return res.json({
                error: err
            })
        }else{
            return res.json({
                store: obj
            })
        }
    })
}



exports.edit = function(req, res){
    var id = req.params.id;

    delete req.body._id;

    Store.findByIdAndUpdate(id, {$set: req.body}, function(err, obj){
        if(err){
            return res.json({error: err});
        }else{
            return res.json({
                store: obj
            });
        }

    });

}



exports.delete = function(req, res){

    var id = req.params.id;

    if(id){
        Store.findById(id, function(error, obj){
            if(error){
                return res.json({
                    error: error
                })
            }else{
                obj.state = 'deleted';
                obj.save(function(err, obj){
                    if(error){
                        return res.json({error: err})
                    }else{
                        return res.json({store: obj})
                    }
                })
            }
        })
    }

}


exports.list = function(req, res){
    var page = req.query.page || 1;

    var condition = {
        state: 'active'
    }

    if(condition){
        condition = req.query.condition;
    }

    var total_items = req.app.get('total_items_per_page');

    if(req.query.total){
        total_items = null;
    }



    Store
        .paginate(condition, page, total_items, function(error, pageCount, paginatedResult, itemCount){
         if(error){
             return res.json({
                 error: error
             });
         }else{
             return res.json({
                 stores: paginatedResult,
                 item_count: itemCount,
                 page_count: pageCount
             })
         }
        }, {sortBy: {uploaded_on: 1}});

}