var Manufacturer = require('../../models/manufacturer');


exports.create = function(req, res){

    var new_man = new Manufacturer(req.body);

    new_man.slug = new_man.slug.trim()
    new_man.slug = new_man.slug.split(' ').join('-')

    new_man.save(function(error, obj){
        if(error){
            return res.json({
                error: error
            });
        }else{

            req.app.get('events').emit('manufacturer-created', obj);
            return res.json({
                manufacturer: obj
            })
        }

    })
}



exports.get = function(req, res){
   var id = req.params.id;




    Manufacturer.findById(id, function(err, obj){
        if(err){
            return res.json({
                error: err
            })
        }else{
            return res.json({
                manufacturer: obj
            })
        }
    })
}



exports.delete = function(req, res){

    var id = req.params.id;

    if(id){
        Manufacturer.findById(id, function(error, obj){
            if(error){
                return res.json({
                    error: error
                })
            }else{
                obj.state = 'deleted';
                obj.save(function(err, obj){
                    if(error){
                        return res.json({error: err})
                    }else{
                        return res.json({manufacturer: obj})
                    }
                })
            }
        })
    }

}


exports.list = function(req, res){
    var page = 1;

    var condition = {
        state: 'active'
    }

    if(req.query.page){
        console.log('page number: '+req.query.page)
        page = req.query.page;
    }

    if(condition){
        condition = req.query.condition;
    }

    var total_items = req.app.get('total_items_per_page');

    if(req.query.total){
        total_items = null;
    }

    Manufacturer
        .paginate(condition, page, total_items, function(error, pageCount, paginatedResult, itemCount){
            if(error){
                return res.json({
                    error: error
                });
            }else{
                return res.json({
                    manufacturers: paginatedResult,
                    item_count: itemCount,
                    page_count: pageCount
                })
            }
        }, {populate: 'created_by', sortBy: {created_on: -1}});

}

exports.edit = function(req, res){
    var id = req.params.id;

    delete req.body._id;

    Manufacturer.findByIdAndUpdate(id, {$set: req.body}, function(err, obj){
        if(err){
            return res.json({error: err});
        }else{
            req.app.get('events').emit('manufacturer-edited', obj);
            return res.json({manufacturer: obj});
        }
    })
}