var Article = require('../../models/article');


exports.create = function(req, res){

    var new_article = new Article(req.body);

    new_article.slug = new_article.slug.trim();
    new_article.slug = new_article.slug.split(' ').join('-');

    new_article.tags = String(new_article.tags).split(',');

    new_article.published_by = req.session.user.id;

    new_article.save(function(error, obj){
        if(error){
            return res.json({
                error: error
            });
        }else{
            req.app.get('events').emit('article-created', obj);
            return res.json({
                article: obj
            });
        }

    })
}

exports.edit = function(req, res){
    var id = req.params.id;

    delete req.body._id;
//    delete req.body.published_on;

    console.log('type of tags'+typeof req.body.tags);
    req.body.tags = String(req.body.tags).split(',');
    req.body.published_on = Date.now();

    Article.findByIdAndUpdate(id, {$set: req.body}, function(err, obj){
        if(err){
            return res.json({error: err});
        }else{
            req.app.get('events').emit('article-edited', obj);
            return res.json({article: obj});
        }
    })
}



exports.get = function(req, res){
    var id = req.params.id;

    Article.findById(id, function(err, obj){
        if(err){
            return res.json({
                error: err
            })
        }else{
            return res.json({
                article: obj
            })
        }
    })
}



exports.delete = function(req, res){

    var id = req.params.id;

    if(id){
        Article.findById(id, function(error, obj){
            if(error){
                return res.json({
                    error: error
                })
            }else{
                obj.state = 'deleted';
                obj.save(function(err, obj){
                    if(error){
                        return res.json({error: err})
                    }else{
                        return res.json({article: obj})
                    }
                })
            }
        })
    }

}


exports.list = function(req, res){
    var page = 1;

    var condition = {
        state: 'active'
    }

    if(req.query.page){
        page = req.query.page;
    }

    if(req.session.user.role === 'author'){
        condition.published_by = req.session.user.id;
    }

    Article.count({status: 'published'}, function(err, count){
        var total_articles_published = count;

        console.log("total published articles: "+total_articles_published);
        Article
        .paginate(condition, page, req.app.get('total_items_per_page'), function(error, pageCount, paginatedResult, itemCount){
            if(error){
                return res.json({
                    error: error
                });
            }else{
                return res.json({
                    articles: paginatedResult,
                    item_count: itemCount,
                    page_count: pageCount,
                    published_articles: total_articles_published
                })
            }
        }, {populate: 'published_by category', sortBy: {published_on: -1}});

    })

    

}


exports.search = function(req, res){
    var query = req.params.query;

    Article.find({title: new RegExp(query, "i")}, function(err, objs){
        if(err){
            return res.json({error: err});
        }else{
            return res.json({result: objs})
        }
    })
//    return res.json();
}

exports.bulk_delete = function(req, res){
    var ids = req.body.ids;
    Article.find({_id: {$in: ids}}, function(error, objs){
        if(error){
            console.log('in error');
            return res.json({success: 0});

        }else{
            console.log('found items: '+objs);
            for(var i = 0; i < objs.length; i++){
                var p = objs[i];


                p.remove().exec();
            }

            return res.json({success: 1});
        }

    });

}


exports.article_types = function(req, res){
//    console.log('yes in types');
    var types = Article.schema.path('article_type').enumValues;
    return res.json(types);
}
