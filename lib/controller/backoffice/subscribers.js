var Subscriber = require("../../models/subscriber");


exports.list = function(req, res){
	var page = req.query.page || 1;

    var total_items = req.app.get('total_items_per_page');

    Subscriber.paginate({}, page, total_items, function(error, pageCount, paginatedResults, itemCount){

        if(error){
            return res.json({
                error: error
            });
        }else{
            // console.log(paginatedResults);
            return res.json({
                subscribers: paginatedResults,
                page_count: pageCount,
                item_count: itemCount
            })
        }


    }, {sortBy:{subscribed_on: -1}})
}

exports.get = function(req, res){
	var id = req.params.id;


    Subscriber.findById(id, function(err, obj){
        if(err){
            return res.json({
                error: err
            })
        }else{
            return res.json(obj)
        }
    })
}

// exports.delete = function(req, res){

// }

