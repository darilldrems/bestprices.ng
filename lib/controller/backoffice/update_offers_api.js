/*
*this file contains api endpoints for the crawler that update product offeer prices and checks if offer still ex
*exists on the websites
*/
var Product = require('../../models/product.js');
var PriceHistory = require('../../models/price_history.js');
var moment = require('moment');
//this function is used to update offers_last_updated date for all products

exports.add_offers_last_date = function(req, res){
  Product.update({}, {$set: {offers_last_updated: moment().subtract(30, 'days').toDate()}}, {multi:true}, function(err){
    if(err){
      console.log("error occured", err);

    }

    return res.send("done");
  })
}


//list products that have been published and last updated 15 days ago
exports.list = function(req, res){
  var total = req.query.count || 50;

  Product.find({status: "published", offers_last_updated: {$lt: moment().subtract(15, 'days').toDate()}}).populate("manufacturer category offers offers.store").limit(total).exec(function(err, products){
    if(err)
      return res.json({status: "failed", message:"error occured"});

    console.log(moment().subtract(15, 'days').toDate());
    return res.json({status: "success", products: products});
  })
}

//bulk delete offers not available and update prices if price changed and add new offers found
exports.update_product_offers = function(req, res){
  var product_id = req.body.id;
  var offers = req.body.offers;

  console.log(JSON.stringify(offers));

  Product.findOne({_id: product_id}).populate("offers offers.store manufacturer category").exec(function(err, product){
    if(err)
      return res.json({status: "failed", message: "product with id not found"});

    if(offers.length == 0){
      product.offers = [];
      product.save();
      return res.json({status: "success", message: "done"});
    }
    // console.log("product: "+JSON.stringify(product))
    //loop through the returned product offers, if offers found in form submission update price if not delete it
    for(var i =0; i < product.offers.length; i++){
      var offer = product.offers[i];
      var offer_exist = -1;

      console.log("offer: "+JSON.stringify(offer));

      for(var j = 0; j < offers.length; j++){
        var form_offer = offers[j];
        console.log("form offer:"+JSON.stringify(form_offer));
        if(form_offer["_id"] === offer["_id"]){
          //set the index to offer_exist
          console.log("in here");
          offer_exist = j;
          break;
        }
      }
      //check if offer_exist is not -1 then update and add to price history. if not delete it and also add to price history
      if(offer_exist != -1){
        product.offers[i]["price"] = offers[offer_exist]["price"];

        var prev_hist = new PriceHistory();
        prev_hist.product_title = product.title;
        prev_hist.product_id = product._id;
        prev_hist.price = offer.price;
        prev_hist.category = product.category.name;
        prev_hist.store_name = offer.store.name;
        prev_hist.save();

        var hist = new PriceHistory();
        hist.product_title = product.title;
        hist.product_id = product._id;
        hist.price = offers[offer_exist]["price"];
        hist.category = product.category.name;
        hist.store_name = offer.store.name;
        hist.save();
        //add to price history here
      }else{
        var prev_hist = new PriceHistory();
        prev_hist.product_title = product.title;
        prev_hist.product_id = product._id;
        prev_hist.price = offer.price;
        prev_hist.category = product.category.name;
        prev_hist.store_name = offer.store.name;
        prev_hist.save();
        product.offers.splice(i, 1);
        console.log("not exist");
      }

    }

    //save changes to product here
    product.save();
    return res.json({status: "success", message: "done"});
  })
}
