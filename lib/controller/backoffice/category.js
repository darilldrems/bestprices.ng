var Category = require('../../models/category');


exports.list = function(req, res){

    var page = 1;

    var condition = {
        state: 'active'
    }

    if(req.query.page)
        page = parseInt(req.query.page)

    var total_items = req.app.get('total_items_per_page');

    if(req.query.total){
        total_items = null;
    }

//    check the req query for the conditions available
    if(condition){
        condition = req.query.condition
    }

    Category.paginate(condition, page, total_items, function(error, pageCount, paginatedResults, itemCount){

        if(error){
            return res.json({
                error: error
            });
        }else{
            console.log(paginatedResults);
            return res.json({
                categories: paginatedResults,
                page_count: pageCount,
                item_count: itemCount
            })
        }


    }, {populate: 'parent', sortBy:{created_on: -1}})



}


exports.create = function(req, res){

    if(req.body.parent == ''){
        delete req.body.parent;
    }

    var new_category = new Category(req.body)
    console.log(req.body);

    new_category.slug = new_category.slug.trim()
    new_category.slug = new_category.slug.split(' ').join('-')


    new_category.save(function(error, obj){
        if(error){
            return res.json({
                error: error
            })
        }else{
            return res.json({
                category: obj
            })
        }
    })
}

exports.delete = function(req, res){
    var id = req.params.id

    Category.findById(id, function(err, obj){
        if(err){
            return res.json({error: err});
        }else{
            obj.state = 'deleted';
            obj.save(function(error, obj){
                if(error){
                    return res.json({error: error});
                }else{
                    return res.json({category: obj});
                }
            })
        }
    })

}


exports.edit = function(req, res){
    var id = req.params.id;

    delete req.body._id;

    Category.findByIdAndUpdate(id, {$set: req.body}, function(err, obj){
        if(err){
            return res.json({error: err});
        }else{
            return res.json({category: obj});
        }
    })
}

exports.get = function(req, res){
    var id = req.params.id;


    Category.findById(id, function(err, obj){
        if(err){
            return res.json({
                error: err
            })
        }else{
            return res.json({
                category: obj
            })
        }
    })
}