var Link = require('../../models/link');


exports.list = function(req, res){

    var page = 1;


    if(req.query.page)
        page = parseInt(req.query.page)

    var total_items = req.app.get('total_items_per_page');

    if(req.query.total){
        total_items = null;
    }

//    check the req query for the conditions available


    Link.paginate({}, page, total_items, function(error, pageCount, paginatedResults, itemCount){

        if(error){
            return res.json({
                error: error
            });
        }else{
            console.log(paginatedResults);
            return res.json({
                links: paginatedResults,
                page_count: pageCount,
                item_count: itemCount
            })
        }


    })



}


exports.create = function(req, res){

    if(req.body.external){
        req.body.target = '_blank'
    }

    var link = new Link(req.body)
//    console.log(req.body);




    link.save(function(error, obj){
        if(error){
            return res.json({
                error: error
            })
        }else{
            return res.json({
                link: obj
            })
        }
    })
}

exports.delete = function(req, res){
    var id = req.params.id

    Link.findById(id, function(err, obj){
        if(err){
            return res.json({error: err});
        }else{

            obj.remove().exec();
            return res.json({link:''})
        }
    })

}


exports.edit = function(req, res){
    var id = req.params.id;

    delete req.body._id;

    if(req.body.external){
        req.body.target = '_blank'
    }

    Link.findByIdAndUpdate(id, {$set: req.body}, function(err, obj){
        if(err){
            return res.json({error: err});
        }else{
            return res.json({link: obj});
        }
    })
}

exports.get = function(req, res){
    var id = req.params.id;


    Link.findById(id, function(err, obj){
        if(err){
            return res.json({
                error: err
            })
        }else{
            return res.json({
                link: obj
            })
        }
    })
}