var Image = require('../../models/image');
var fs = require('fs');

exports.upload = function(req, res){

    console.log(req.app.get('root_folder'));
    console.log(req.query)
    console.log(req.files.file);

    fs.readFile(req.files.file.path, function(err, data){


        if(err){

            return res.json({error: err});

        }else{
            var formatted_alt = req.query.alt.split(' ').join('-'); //make sure spaces are removed from alt

            var splited_old_name = req.files.file.name.split('.'); //split the old name to get image format

            //create a new name using the formatted alt with the image format
            var new_file_name = formatted_alt+'-'+Date.now()+'.'+splited_old_name[1];
            var new_path = req.app.get('root_folder')+'/public/media/'+new_file_name;
//            console.log('new path: '+new_path);

            fs.writeFile(new_path, data, function(err){
                if(err){
                    return res.json({error: err});
                }else{
                    var image = new Image(req.query);
                    image.path = new_file_name;
                    image.uploaded_by = req.session.user.id;

                    image.save(function(err, im){
                        if(err){
                            return res.json({error: err});
                        }else{
                            req.app.get('events').emit('image-uploaded', im);
                            return res.json({image: im});
                        }
                    })

                }
            })

        }
    });

}

exports.list = function(req, res){

    var page = 1;

    if(req.query.page) page = req.query.page;

    Image
        .paginate({state: 'active'}, page, req.app.get('total_items_per_page'), function(error, pageCount, results, itemCount){
            if(error){
                return res.json({
                    error: error
                });
            }else{
                return res.json({
                    page_count: pageCount,
                    images: results,
                    item_count: itemCount
                })
            }
        }, {populate: 'uploaded_by', sortBy: { uploaded_on: -1} })

}

exports.search = function(req, res){
    var query = req.params.query;

    Image
        .find({alt: new RegExp(query, "i")})
        .exec(function(err, obj){
            if(err){
                return res.json({error: err});
            }else{
                return res.json({result: obj});
            }
        })
}


exports.bulk_delete = function(req, res){
    var ids = req.body.ids;

    Image.find({_id: {$in : ids}}, function(error, images){

        if(error){
            return res.json({success: 0});
        }else{

            for(var i = 0; i < images.length; i++){
                var image = images[i];

//                image.state = "deleted";
                image.remove().exec();

            }

            return res.json({success: 1});

        }

    })


}