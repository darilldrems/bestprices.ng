var Sitemap = require('../../models/sitemap');


exports.display_sitemaps = function(req, res){

	Sitemap.find({}).sort({created_on: -1}).exec(function(err, results){

		if(err){
			return res.send("error");
		}
		var root = '<?xml version="1.0" encoding="UTF-8"?>';
    	var header = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
    	var footer = '</urlset>';

    	var body = "";
    	for(var i in results){
    		var sm = results[i];

    		body +='<url><loc>'+sm.url+'</loc><priority>'+sm.priority+'</priority><changefreq>'+sm.frequency+'</changefreq></url>';
    	}

		res.set('Content-Type', 'text/xml');

		var sitemap_xml = root + header + body + footer;
		res.send(sitemap_xml);

	})

}

