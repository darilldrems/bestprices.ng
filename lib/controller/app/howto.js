var HowTo = require('../../../lib/models/howto');
var settings = require("../../../settings");

//var replace_content_ads = require('../../../lib/helpers').replace_content_ads;

var replace_content_short_code = require('../../../lib/helpers').replace_short_code;

exports.list = function(req, res){
    var page_no = 1;
    if(req.query.page) page_no = req.query.page;

    HowTo.paginate({status: 'published', state: 'active'},
        page_no, req.app.get('total_items_per_page'),
        function(error, pageCount, results, itemCount){

            var page = {
                name: 'HowTo',
                sub_name: ''

            }


            if(req.app.locals.links.top){
                //            console.log(req.app.locals.links)
                if(req.app.locals.links.top.HowTo.meta_title) page.meta_title = req.app.locals.links.top.HowTo.meta_title;

                if(req.app.locals.links.top.HowTo.meta_description) page.meta_description = req.app.locals.links.top.HowTo.meta_description;

            }

            if(!error){
                if(results.length > 0){
                    return res.render(req.app.get("theme")+'/howtos', {
                        page: page,
                        posts: results,
                        page_count: pageCount,
                        item_count: itemCount,
                        current_page: page_no,
                        og:{
                            type: settings.og.type,
                            image: settings.og.image
                        }
                    });
                }else{
                    return res.render('v2/404', {});
                }
            }else{
//                return error code and error page
                return res.render('v2/500', {});
            }

        }, {populate:'created_by', sortBy:{published_on: -1}})

}

exports.howto = function(req, res){
    var slug = req.params.slug;

    HowTo.findOne({slug: slug}).populate('created_by').exec(function(error, result){
        if(error){
            return res.status(404).render('v1/404', {})
        }else{

            if(result === null){
                return res.render('v2/404', {});
            }else{
                var fb_url = ''
                var twitter_handle = ''

                if(result.created_by.fb_url){
                    fb_url = result.created_by.fb_url;
                }

                if(result.created_by.twitter_handle){
                    twitter_handle = result.created_by.twitter_handler;
                }

                for(var j = 0; j < result.steps.length; j++){
                    var step = result.steps[j];
                    result.steps[j].content = replace_content_short_code(step.content);
                }


                return res.render(req.app.get("theme")+'/howto', {
                    page:{
                        name: 'HowTo Item',
                        sub_name: 'HowTo Item',
                        meta_title: result.meta_title,
                        meta_description: result.meta_description

                    },
                    post: result,
                    og:{
                        type: 'article',
                        image: settings.domain+result.thumbnail,
                        article_publisher: settings.fb_page+", "+ fb_url,
                        article_published_time: result.published_on,
                        article_fb_admins: req.app.get('fb_admins'),
                        article_tags: result.tags.join(","),
                        twitter_creator: twitter_handle,
                        google_plus: result.created_by.google_plus || null
                    }
//                latest_posts: latest
                })
            }


        }
    })
}