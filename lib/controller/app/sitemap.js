var Category = require('../../models/category');
var Store = require('../../models/store');
var Brand = require('../../models/manufacturer');
var Product = require('../../models/product');
var HowTo = require('../../models/howto');
var Article = require('../../models/article');

var settings = require('../../../settings.js');

var escape_html_entities = require('../../../lib/helpers').escape_html_entities;

var _ = require('underscore');

var sitemap_xml = function(list, changeFreq, linkPriority, type){

    var priority = linkPriority || '0.8';
    var freq = changeFreq || 'monthly';
    var root = '<?xml version="1.0" encoding="UTF-8"?>';
    var header = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
    var footer = '</urlset>';
    var body = '';

    var header_sitemap_index = '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
    var footer_sitemap_footer = '</sitemapindex>';

    if(type && type === 'index'){
        for(var i = 0; i < list.length; i++){
            var path = escape_html_entities(list[i]);
            body += '<sitemap><loc>'+path+'</loc><priority>'+priority+'</priority><changefreq>'+freq+'</changefreq></sitemap>'
        }

        return root + header_sitemap_index + body + footer_sitemap_footer;
    }else{
        for(var i = 0; i < list.length; i++){
            var path = escape_html_entities(list[i]);
            body += '<url><loc>'+path+'</loc><priority>'+priority+'</priority><changefreq>'+freq+'</changefreq></url>'
        }

        return root + header + body + footer;

    }




}

var sitemaps = {
    'categories': {
        condition: {state: 'active'},
        model: Category,
        path: settings.domain+'/categories/',
        sort_by: {created_on: -1}
    },
    'stores': {
        condition: null,
        model: Store,
        path: settings.domain+'/stores/',
        sort_by: {uploaded_on: -1}
    },
    'brands': {
        condition: {state: 'active'},
        model: Brand,
        path: settings.domain+'/brands/',
        sort_by: {created_on: -1}
    },
    'products': {
        condition: {status: 'published', state: 'active'},
        model: Product,
        path: settings.domain+'/',
        sort_by: {uploaded_on: -1}
    },
    'howtos': {
        condition: {status: 'published', state: 'active'},
        model: HowTo,
        path: 'http://howto.'+settings.host+"/",
        sort_by: {published_on: -1}
    },
    // 'reviews': {
    //     condition: {status: 'published', article_type: 'Reviews', state: 'active'},
    //     model: Article,
    //     path: 'http://reviews.bestprices.ng/',
    //     sort_by: {published_on: -1}
    // },
    'blog': {
        condition: {status: 'published', article_type: 'Blog', state: 'active'},
        model: Article,
        path: 'http://blog.'+settings.host+"/",
        sort_by: {published_on: -1}
    },
    // 'top10': {
    //     condition: {status: 'published', article_type: 'Top10', state: 'active'},
    //     model: Article,
    //     path: 'http://top10.bestprices.ng/',
    //     sort_by: {published_on: -1}
    // },
    // 'news': {
    //     condition: {status: 'published', article_type: 'News', state: 'active'},
    //     model: Article,
    //     path: 'http://news.bestprices.ng/',
    //     sort_by: {published_on: -1}
    // }

}

exports.sitemap_index = function(req, res){
    var links = []
    _.each(sitemaps, function(value, key){
        links.push(settings.domain+'/sitemaps/'+key);
    })

    var xml = sitemap_xml(links, 'weekly', '0.9', 'index');


    return res.header('Content-Type','text/xml').send(xml);
}


exports.sitemap = function(req, res){
    var page_no = 1;

    if(req.query.page) page_no = req.query.page;

    var base = settings.domain;



    console.log('param: '+req.params.sitemap);

    var sitemap_selected = sitemaps[req.params.sitemap];

    console.log('condition: '+JSON.stringify(sitemap_selected.condition));
    sitemap_selected.model
        .paginate(sitemap_selected.condition, page_no, 20, function(error, pageCount, results, itemCount){
            if(error){
                return res.status(404).send();
            }else{
                 console.log(results.length);
                var links = [];
                for(var i = 0; i < results.length; i++){
                    var slug = results[i].slug;
                    var url = sitemap_selected.path + slug
                    links.push(url)

                }

                if(pageCount > 1 && page_no < pageCount){
                    var next = page_no + 1;
                    links.push(base+req.path+'?page='+next);
                }

                var xml = sitemap_xml(links, '0.8', 'weekly');
                return res.header('Content-Type','text/xml').send(xml);
            }
        }, {sortBy: sitemap_selected.sort_by})



}