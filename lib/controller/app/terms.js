var Term = require('../../models/term')
var settings = require("../../../settings");

exports.list = function(req, res){
	console.log("in here");
	var page_no = req.query.page || 1;

	Term.paginate({status:'published'}, page_no, 30, function(error, pageCount, result, itemCount){

		var page = {
            meta_title: 'Bestprices - Terms',
            meta_description: 'Popular searches on bestprices. The leading price comparison website in '+settings.country,
            name: 'Shopping',
            sub_name: 'Categories'
        }

        var og = {
            type: settings.og.type,
            image: settings.og.image
        }

        if(!error){
        	return res.render(req.app.get("theme")+'/terms', {
                                        page: page,
                                        og:og,
                                        terms: result,
                                        item_count: itemCount,
                                        page_count: pageCount,
                                        current_page: page_no

                                    })
        }else{
//                       TODO error page
            return res.render('v2/500', {});
            // console.log(error);
//                       return res.json(error);
        }

	}, {sortby:{created_on:-1}});
}