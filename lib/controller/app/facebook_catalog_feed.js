var settings = require('../../../settings');
var mustache = require('mustache');
var Product = require('../../models/product');
var Category = require('../../models/category');

exports.feed = function(req, res){
	var category = req.params.slug;
	
	Category.find({slug: category}, function(error, cat){
		if(error){
			console.log(error);
			return res.send("error")
		}else{
			// console.log(JSON.stringify(cat));
			Product.find({category: cat[0]._id, status:'published', 'offers.0': {$exists: true}}).populate("category").populate("offers").populate("manufacturer").sort({uploaded_on:-1}).limit(10).exec(function(err, results){
				if(err){
					console.log(err);
					return res.send("error");
				}

				var products = [];

				for(var i in results){



					var item = results[i];

					console.log(JSON.stringify(item))

					var lowest_price = item.offers[0].price;

					for(var c in item.offers){
						var off = item.offers[c];
						if(off.price < lowest_price){
							lowest_price = off.price
						}
					}

					var product = {
						id: item._id,
						title: item.title,
						description: item.description,
						url: settings.domain+"/"+item.slug,
						image: settings.domain+item.photos[0],
						manufacturer: item.manufacturer.name,
						price:settings.currency+lowest_price,
						country: settings.country,
						category: cat[0].name
					}

					products.push(product);
				}

				console.log(products.length);

				var feed_obj = {
					products: products,
					domain: settings.domain,
					description: settings.meta.home.meta_title
				}
				var xml = mustache.render(feed_template, feed_obj);

				res.set('Content-Type', 'text/xml');
				return res.send(xml);
			})

		}
	})	
	
}

var feed_template = "<?xml version='1.0' ?> \
	<rss xmlns:g='http://base.google.com/ns/1.0' version='2.0'> \
	    <channel> \
	        <title>Bestprices</title> \
	        <link>{{domain}}</link> \
	        <description>{{description}}</description> \
	        {{#products}} \
				<item> \
		            <g:id>{{id}}</g:id> \
		            <g:title>{{title}}</g:title> \
		            <g:description>{{description}}</g:description> \
		            <g:link>{{url}}</g:link> \
		            <g:image_link>{{image}}</g:image_link> \
		            <g:brand>{{manufacturer}}</g:brand> \
		            <g:condition>new</g:condition> \
		            <g:availability>in stock</g:availability> \
		            <g:price>{{price}}</g:price> \
		            <g:shipping> \
		                <g:country>{{country}}</g:country> \
		                <g:service>Standard</g:service> \
		            </g:shipping> \
		            <g:google_product_category>{{category}}</g:google_product_category> \
		            <g:custom_label_0></g:custom_label_0> \
		        </item> \
	        {{/products}} \
	    </channel> \
	</rss>"