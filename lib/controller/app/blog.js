var Article = require('../../../lib/models/article');
var replace_content_short_code = require('../../../lib/helpers').replace_short_code;
var settings = require("../../../settings");

exports.list = function(req, res){

    var page_no = 1;
    if(req.query.page) page_no = req.query.page;

    Article.paginate({status: 'published', state: 'active'},
        page_no, 12,
        function(error, pageCount, results, itemCount){

            var page = {
                name: 'Blog',
                sub_name: '',
                meta_title:settings.meta.blog.meta_title,
                meta_description: settings.meta.blog.meta_description

            }


            if(req.app.locals.links.top){
                //            console.log(req.app.locals.links)
                if(req.app.locals.links.top.Blog.meta_title) page.meta_title = req.app.locals.links.top.Blog.meta_title;

                if(req.app.locals.links.top.Blog.meta_description) page.meta_description = req.app.locals.links.top.Blog.meta_description;

            }

            if(!error){

                if(results.length > 0){
                    return res.render(req.app.get("theme")+'/blogs', {
                        page: page,
                        posts: results,
                        page_count: pageCount,
                        item_count: itemCount,
                        current_page: page_no,
                        og:{
                            type:settings.og.type,
                            image: settings.og.image

                        }
                    });
                }else{
                    return res.render('v2/404', {});
                }

            }else{
//                return error code and error page
                return res.render('v2/500', {});
            }

        }, {populate:'published_by', sortBy:{published_on: -1}})



}

exports.blog = function(req, res){

    var slug = req.params.slug;

    Article.findOne({slug: slug}).populate('published_by').exec(function(error, result){
        if(error){
            return res.render('v2/500', {});
        }else{
            if(result === null){
                return res.render('v2/404', {});
            }else{


                var author_fb_url = '';
                var twitter_creator = ''

                if (result.published_by.fb_url){
                    author_fb_url = result.published_by.fb_url;
                }

                if(result.published_by.twitter_handle){
                    twitter_creator = result.published_by.twitter_handle
                }

                result.content = replace_content_short_code(result.content)

                return res.render(req.app.get("theme")+'/blog', {
                    page:{
                        name: 'Blog Item',
                        sub_name: 'Blog Item',
                        meta_title: result.meta_title,
                        meta_description: result.meta_description

                    },
                    post: result,
                    og:{
                        type: 'article',
                        image: settings.domain+result.thumbnail,
                        article_publisher: settings.fb_page+ ", "+ author_fb_url,
                        article_published_time: result.published_on,
                        article_fb_admins: req.app.get('fb_admins'),
                        article_tags: result.tags.join(","),
                        twitter_creator: twitter_creator,
                        google_plus: result.published_by.google_plus || null

                    }
    //                latest_posts: latest
                })
            }

        }
    })


}