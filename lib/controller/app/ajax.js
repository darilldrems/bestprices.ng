/*
* Ajax contains http functions for jquery ajax components requests
* i.e pageviews counter, popular posts, etc
* */

var Product = require('../../models/product');
var Article = require('../../models/article');
var HowTo = require('../../models/howto');

exports.page_views_counter = function(req, res){
    var obj = {
        article: {
            model: Article

        },
        product: {
            model: Product
        },
        howto: {
            model: HowTo
        }
    }

    var object = req.query.object;
    var object_id = req.query.id;

    obj[object].model.findOne({_id: object_id}, function(err, result){
        if(err){
            console.log(err);
            return res.status(500).json("failed")
        }else{
            var initial_val = result.page_views || 0
            result.page_views = initial_val + 1;

            result.save(function(err){
                if(err){
                    console.log(err);
                    return res.status(500).json("failed");
                }else{
                    return res.status(200).json("success");
                }
            })
        }
    })



}


exports.product = function(req, res){
    Product.findById(req.params.id).populate("offers.store").exec(function(err, pr){
        if(err){
            return res.status(500).jsonp();
        }else{
            return res.status(200).jsonp(pr);
        }
    })
}


exports.recent_news = function(req, res){
    var total = req.params.count || 5;
    Article.find({article_type: "News", status: "published"}).sort({published_on: -1}).limit(total).exec( function(err, results){
        if(err){
            return res.status(500).jsonp();
        }else{
            return res.status(200).jsonp(results);
        }
    })
}



exports.similar_products = function(req, res){

    var term = req.params.term;
    var match = { $text:{ $search: term }, status: 'published' };

    Product.aggregate([
        {$match: match},
        { $sort: { score: { $meta: "textScore" } } }

    ]).limit(5).exec(function(error, products){

            if(error){
//                    TODO error 500 or 404
//                console.log(error);
                return res.status(500).jsonp()
            }else{

                return res.status(200).jsonp(products);

            }
        });

}

exports.popular_posts = function(req, res){
    Article.find({status: "published", state: "active"}).sort({page_views: -1, published_on: -1}).limit(5).exec(function(err, results){
        if(err){
            return res.status(500).jsonp();
        }else{
            return res.status(200).jsonp(results);
        }
    })

}

exports.hot_products = function(req, res){
    Product.find({status: "published", state: "active"}).sort({page_views: -1, uploaded_on: -1}).limit(5).exec(function(err, results){
        if(err){
            return res.status(500).jsonp();
        }else{
            return res.status(200).jsonp(results);
        }
    })
}