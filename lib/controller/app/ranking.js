/*
* this file is responsible for adding ranking value for all products in the database
* it can be run once a day and it uses a function similar to reddits ranking algorithm
*/
var lupus = require('lupus');
var Product = require('../../models/product');
var moment = require('moment');

exports.rank = function(req, res){

	Product.find({}, function(error, products){
		if(error){
			return res.json("error");ojects
		}else{

			//lupus helps to run long running tasks in nodejs
			lupus(0, products.length, function(n){
				//calculate the ranking and add to product
				var product = products[n];

				var in_2001 = moment("2001-01-01");

				var sDay = product.uploaded_on.getDate();
				var sMonth = product.uploaded_on.getMonth() + 1;
				var sYear = product.uploaded_on.getFullYear();

				console.log(sDay+"-"+sMonth+"-"+sYear);

				var now = moment(sYear+"-"+sMonth+"-"+sDay);

				// console.log(now);

				//time difference in seconds between 2001 and when the product was uploaded
				var diff_in_seconds = now.diff(in_2001);

				// console.log("diff in seconds"+diff_in_seconds);

				//the average score which is used to get positive or negative score
				var avarage_score = 3000;

				//score
				var page_views = product.page_views || 0;
				var score = page_views - avarage_score

				//logrithm formular from reddit ranking algorithm
				var order = Math.log(Math.max(Math.abs(score), 1))/Math.log(10);

				var sign = 0;
				if(score > 0)
					sign = 1;

				if(score < 0)
					sign = -1;

				var ranking = sign * order + diff_in_seconds/45000;

				// console.log("ranking is: "+ranking);

				product.ranking = ranking;

				product.save();



			}, function(){
				return res.json("done");
			})

		}
	})
	
}