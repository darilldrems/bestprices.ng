/*
 * Ridwan Olalere
 * Index file responsible for the home page
 * takes filter parameters in req.query
 * pagination
 * */

var Product = require('../../models/product');
//var Offer = require('../../models/offer');
var Manufacturer = require('../../models/manufacturer');

var solr = require('solr-client');
var settings = require('../../../settings.js');

var q = require('q');

module.exports = function(req, res){

    var page_no = 1;

    var condition = {
        status: 'published'
    }



    if(req.query.page){
        page_no = req.query.page;
    }
    console.log('properties of app'+JSON.stringify(req.app.get('nav_menu')));

    var page = {
        name: 'Shopping',
        sub_name: '',
        title: 'Bestprices.ng - Nigeria\'s #1 price comparison website.',
        meta_title: 'Bestprices.ng - Nigeria\'s #1 price comparison website.',
        meta_description: 'Shop and compare prices of products from over 10 online stores in Nigeria at Bestprices. See prices from Konga, Jumia, Kaymu, DealDeay, Kara, AfriqBuy and more'
    }

    var og = {
        type: 'website',
        image: 'http://bestprices.ng/images/bestprices.png'
    }




    if(req.query.search){

        var client = solr.createClient({
            host: settings.solr.ip,
            port: settings.solr.port,
            path: settings.solr.path,
            core: settings.solr.core
        });

        
        console.log('search term: '+req.query.search);
        condition['$text'] = {'$search': req.query.search};
        condition['sort'] = {'score': {'$meta': 'textScore'}};

        page.meta_title = req.query.search + ' - compare prices of'+req.query.search +' on Bestprices.ng';
        page.meta_description = 'Shop and compare prices of '+req.query.search+' from over 10 online stores in Nigeria. Bestprices price comparison website in Nigeria.'

        var match = { $text:{ $search: req.query.search }, status: 'published' };

        if(req.query.price_from && req.query.price_to){
            console.log('in price filter');
            console.log('lower band: '+Number(req.query.price_from - 1));
            match["offers.price"] = {$gt: Number(req.query.price_from - 1), $lt: Number(req.query.price_to + 1)}
        }

        Product.aggregate([
            {$match: match},
            { $sort: { score: { $meta: "textScore" } } }

        ], function(error, products){

            if(error){
//                    TODO error 500 or 404
                console.log(error);
                return res.render('v2/500', {});
//                return res.send();
            }else{

                return res.render(req.app.get("theme")+'/search', {
                    page: page,
                    og: og,
                    products: products,
                    search: req.query.search,
                    price_from: req.query.price_from,
                    price_to: req.query.price_to
                })

            }
        })



    }else{

        if(req.query.price_from && req.query.price_to){
            console.log('in price filter');
            console.log('lower band: '+Number(req.query.price_from - 1));
            condition["offers.price"] = {$gt: Number(req.query.price_from - 1), $lt: Number(req.query.price_to + 1)}
        }

        Product
            .paginate(condition, page_no, req.app.get('total_items_per_page'),

            function(error, pageCount, results, itemCount){
                if(!error){
                    if(results.length > 0){
                        console.log(JSON.stringify(results));

//                    res.json(results);
                        return res.render('v2/index', {
                            page: page,
                            og: og,
                            products: results,
                            item_count: itemCount,
                            page_count: pageCount,
                            current_page: page_no,
                            price_from: req.query.price_from || null,
                            price_to: req.query.price_to || null
                        })
                    }else{
                        return res.render('v2/404', {});
                    }
                }else{
                    return res.render('v2/500', {})
                }


            }, {sortBy: {uploaded_on: -1}})
    }









}