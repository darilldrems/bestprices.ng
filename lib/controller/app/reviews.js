var Article = require('../../../lib/models/article');
var replace_content_short_code = require('../../../lib/helpers').replace_short_code;

exports.list = function(req, res){

    var page_no = 1;
    if(req.query.page) page_no = req.query.page;

    Article.paginate({status: 'published', article_type: 'Reviews', state: 'active'},
        page_no, req.app.get('total_items_per_page'),
        function(error, pageCount, results, itemCount){

            var page = {
                name: 'Reviews',
                sub_name: ''

            }


            if(req.app.locals.links.top){
                //            console.log(req.app.locals.links)
                if(req.app.locals.links.top.Reviews.meta_title) page.meta_title = req.app.locals.links.top.Reviews.meta_title;

                if(req.app.locals.links.top.Reviews.meta_description) page.meta_description = req.app.locals.links.top.Reviews.meta_description;

            }

            if(!error){
                if(results.length > 0){
                    return res.render('v2/reviews', {
                        page: page,
                        posts: results,
                        page_count: pageCount,
                        item_count: itemCount,
                        current_page: page_no,
                        og:{
                            type: 'article',
                            image: 'http://bestprices.ng/images/bestprices.png'
                        }
                    });
                }else{
                    return res.render('v2/404', {});
                }
            }else{
                return res.render('v2/500', {});
//                return error code and error page
            }

    }, {populate:'published_by', sortBy:{published_on: -1}})



}

exports.review = function(req, res){

    var slug = req.params.slug;

//    slug = slug.toLowerCase();

    console.log('slug here:'+slug);

    Article.findOne({slug: slug}).populate('published_by').exec(function(error, result){
        if(error || result === null){
            console.log('in 404');
            if(result === null){
                return res.render('v2/404', {})
            }else{
                return res.render('v2/500', {});
            }

        }else{
//            console.log(JSON.stringify(result));
//            console.log('id is: '+JSON.stringify(result));
//            Article.getLatest('Reviews', result).then(function(latest){
//
//
//            }
            var author_fb_url = '';
            var twitter_creator = ''

            if (result.published_by.fb_url){
                author_fb_url = result.published_by.fb_url;
            }

            if(result.published_by.twitter_handle){
                twitter_creator = result.published_by.twitter_handle
            }

            result.content = replace_content_short_code(result.content);


            return res.render('v2/review', {
                page:{
                    name: 'Review',
                    sub_name: 'Review Item',
                    meta_title: result.meta_title,
                    meta_description: result.meta_description

                },
                post: result,
                og:{
                    type: 'article',
                    image: 'http://bestprices.ng'+result.thumbnail,
                    article_publisher: 'https://www.facebook.com/bestpricesng, '+ author_fb_url,
                    article_published_time: result.published_on,
                    article_fb_admins: req.app.get('fb_admins'),
                    article_tags: result.tags.join(","),
                    twitter_creator: twitter_creator,
                    google_plus: result.published_by.google_plus || null
                }
//                latest_posts: latest
            })




        }
    })


}