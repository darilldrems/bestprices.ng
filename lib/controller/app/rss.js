var Product = require('../../models/product'),
    Article = require('../../models/article'),
    HowTo = require('../../models/howto'),
    rssBuilder = require('../../rssbuilder');

var settings = require('../../../settings.js');


exports.product_feed = function(req, res){
    var feed = new rssBuilder(settings.meta.home.meta_title, settings.domain, 'Recent products price comparison')

    var query = Product.find({status: "published", state:"active"}).sort("-uploaded_on").limit(50)

    query.exec(function(err, results){
        if(err){
            return res.render('v2/500', {})
        }else{
            for(var i =0; i < results.length; i++){
                var item = results[i];
                feed.addNewItem(item.title, settings.domain+"/"+item.slug, item.uploaded_on, item.description, {});

            }
            res.set('Content-Type', 'text/xml')
            return res.send(feed.getFeedXML())
        }
    })
}

exports.article_feed = function(req, res){
    var feed = new rssBuilder('Bestprices articles', settings.domain, 'Recent articles');

    var query = Article.find({status: "published", state:"active"}).sort("-published_on").limit(20)

    query.exec(function(err, results){
        if(err){
            return res.render('v2/500', {});
        }else{
            for(var i = 0; i < results.length; i++){
                var item = results[i];

                feed.addNewItem(item.title, 'http://'+item.article_type+"."+settings.host+"/"+item.slug, item.published_on, item.content, {})

            }

            res.set('Content-Type', 'text/xml')
            return res.send(feed.getFeedXML())
        }
    })

}

exports.howto_feed = function(req, res){
    var feed = new rssBuilder('Bestprices howtos', settings.domain, 'Recent how to');

    var query = HowTo.find({status: "published", state:"active"}).sort("-published_on").limit(20)

    query.exec(function(err, results){
        if(err){
            console.log(err);
            return res.render('v2/500', {});
        }else{
            for(var i = 0; i < results.length; i++){
                var item = results[i];

                feed.addNewItem(item.title, 'http://howto.'+settings.host+"/"+item.slug, item.published_on, item.steps[0], {})

            }

            res.set('Content-Type', 'text/xml')
            return res.send(feed.getFeedXML())
        }
    })
}