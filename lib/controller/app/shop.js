var Store = require('../../../lib/models/store');
//var Offer = require('../../../lib/models/offer');

var Product = require('../../../lib/models/product');
var settings = require('../../../settings.js');
exports.shop = function(req, res){
    var slug = req.params.slug;
    var page_no = 1;

    if(req.query.page){
        page_no = req.query.page;
    }

    Store.findOne({slug: slug}, function(error, store){
        if(!error && store != null){


            Product
                .paginate({'offers.store': store._id, status: "published", state:"active"}, page_no, req.app.get('total_items_per_page'),
                function(error, pageCount, results, itemCount){

                    if(!error){

                        console.log('in no error');
                        var page = {
                            name: 'Stores',
                            meta_title: store.meta_title,
                            meta_description: store.meta_description,
                            sub_name: store.name
                        }

//                        return res.json({result: results, store: store});

                        return res.render(req.app.get("theme")+'/store', {
                            page: page,
                            store: store,
                            store_offers:{
                                offers: results,
                                page_count: pageCount,
                                item_count: itemCount,
                                current_page: page_no
                            },
                            og: {
                                type: 'company',
                                image: settings.domain+store.logo
                            }
                        })

                    }else{
                        return res.render('v2/500', {});

//                        return res.json(error);
                    }

            }, {populate: ''});



        }else{
            if(store === null){
                return res.render('v2/404', {});
            }else{
                return res.render('v2/500', {});
            }
//            return res.render('v2/500', {});
//            return res.json(error);
        }


    })

}


exports.list = function(req, res){
    var page_no = 1;

    if(req.query.page){
        page_no = req.query.page
    }


    var condition = {}

    Store
        .paginate(condition, page_no, req.app.get('total_items_per_page'), function(error, pageCount, results, itemCount){

            if(!error){

                console.log("I am in here man");
                var page = {
                    name: 'Stores',
                    sub_name: '',
                    meta_title: settings.meta.shop.meta_title,
                    meta_description: settings.meta.shop.meta_description
                }

                // return res.json(results);
                return res.render(req.app.get("theme")+'/stores', {
                    stores: results,
                    item_count: itemCount,
                    page_count: pageCount,
                    page: page,
                    current_page: page_no,
                    og:{
                        type: 'website',
                        image: settings.domain+'/images/bestprices.png'
                    }
                });


            }else{
                console.log("error is"+error);
            }

        })


}