var Product = require('../../models/product');
var settings = require("../../../settings");
// var $q = require('q');

module.exports = function(req, res){
//    var home_categories = ['electronics', 'mobile-phones', 'computers-accessories', 'office', 'home-kitchen-appliances'];

    // var electronics_id = '550423b0d4612a7e621a1124';

  var electronics_id = '550423b0d4612a7e621a1124';

    var fake_electronics_id = settings.categories[0];
    var phones_id = settings.categories[1]; //live id
    // var phones_id = "55f37b77ede3ad420c02df73";
    var tablets = settings.categories[2]; //live id
    // var tablets = "55f37b9bede3ad420c02df74"; 
    var computers = settings.categories[3];
    var office = settings.categories[4];

    var data = {};


    var page = {
        name: 'Shopping',
        sub_name: '',
        title: settings.meta.home.meta_title,
        meta_title: settings.meta.home.meta_title,
        meta_description: settings.meta.home.meta_description
    }

    var og = {
        type: settings.og.type,
        image: settings.og.image
    }

    



    Product.findFeaturedProductsFromCategory(fake_electronics_id).then(function(r){
//        assign result
        data.electronics = r;
//        console.log('electronics'+r);
        return Product.findFeaturedProductsFromCategory(phones_id);

    }).then(function(r){
            data.phones = r;
//            console.log('fashion'+r);
            return Product.findFeaturedProductsFromCategory(tablets);

        }).then(function(r){
            data.tablets = r;
//            console.log('machines'+r);
            return Product.findFeaturedProductsFromCategory(computers);
        }).then(function(r){
            data.computers = r;
//            console.log('france'+r);

            return Product.findFeaturedProductsFromCategory(office);
        }).then(function(r){
            data.office = r;

            return Product.findHotProducts()
        }).
        then(function(r){
            
            data.hot_products = r;
            
            return res.render(req.app.get("theme")+'/home', {
                page: page,
                og: og,
                data: data

            })
        })


}