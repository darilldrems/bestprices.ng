var LandingPage = require('../../models/landing_page.js');
var settings = require('../../../settings.js');

module.exports = function(req, res){
	var slug = req.params.slug;

	LandingPage.findOne({slug: slug}).populate('products').exec(function(error, landing_page){
		if(error){
			return res.redirect('/');
		}else{
			if(landing_page.status == "draft"){
				if(!req.session || req.session.user.role != 'admin'){
					return res.redirect('/');
				}
			}

			if(!landing_page){
				return res.redirect('/');
			}

			var page = {
		        name: 'Shopping',
		        sub_name: '',
		        title: landing_page.title,
		        meta_title: landing_page.meta_title,
		        meta_description: landing_page.meta_description
		    }

		    var og = {
		        type: settings.og.type,
		        image: settings.og.image
		    }

		    if(req.query.resp_type=='json'){
		    	return res.json(landing_page);
		    }else{
		    	return res.render(req.app.get("theme")+'/landingpage', {
                            og: og,
                            search: req.query.search,
                            products: landing_page.products,
                            landing_page: landing_page,
                            page: page
                        })

		    }
		    

		}
	})
}