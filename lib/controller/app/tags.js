var Product = require('../../models/product');
var settings = require('../../../settings.js');


var Product = require('../../models/product');
//var Offer = require('../../models/offer');
var Manufacturer = require('../../models/manufacturer');

var solr = require('solr-client');
var settings = require('../../../settings.js');

var q = require('q');

module.exports = function(req, res){

    var page_no = 1;

    var condition = {
        status: 'published'
    }

    var tag = req.params.tag;
    tag = tag.split('-').join(" ");

    req.query.search = tag;

    if(req.query.page){
        page_no = req.query.page;
    }
    console.log('properties of app'+JSON.stringify(req.app.get('nav_menu')));

    var page = {
        name: 'Shopping',
        sub_name: '',
        title: tag+' - Compare prices of '+tag+settings.meta.index.meta_title,
        meta_title: tag+' - Compare prices of '+tag+settings.meta.index.meta_title,
        meta_description: 'Shop and compare prices of '+tag+settings.meta.index.meta_description
    }

    var og = {
        type: settings.og.type,
        image: settings.og.image
    }




    if(req.query.search){

        var client = solr.createClient({
            host: settings.solr.ip,
            port: settings.solr.port,
            path: settings.solr.path,
            core: settings.solr.core
        });


        

        // page.meta_title = req.query.search + ' - compare prices of'+req.query.search +settings.meta.index.meta_title;
        // page.meta_description = 'Shop and compare prices of '+req.query.search+settings.meta.index.meta_description

        var query = "q=*"+"&rows="+settings.solr.rows;
        if(req.query.search){
            query = "q="+encodeURIComponent(req.query.search)+"&rows="+settings.solr.rows;
        } 

        if(req.query.category){
            query += "&fq=category:"+encodeURIComponent(req.query.category)
        }

        if(req.query.price_range){
            query += "&fq="+encodeURIComponent(req.query.price_range)
        }

        if(req.query.brand){
            query += "&fq=brand:"+encodeURIComponent(req.query.brand)
        }

        var facets = "&facet=on&facet.field=category&facet.field=brand";

        var price_facets = ""

        for(var i in settings.price_facets){
            price_facets += "&facet.query="+encodeURIComponent(settings.price_facets[i]);
        }

        console.log(price_facets)
        // console.log("query is: "+JSON.stringify(query));
        client.search(query+facets+price_facets,function(err,results){
           if(err){
            console.log(err);
           }else{

            // console.log(results)
            var products = results.response.docs;

            if(req.query.resp_type == "json"){
                var limit = req.query.limit || 6
                return res.json(products.slice(0, limit))
            }

            
            console.log(JSON.stringify(results.facet_counts.facet_fields.category))
            return res.render(req.app.get("theme")+'/search', {
                            page: page,
                            og: og,
                            search: req.query.search,
                            products: products,
                            facets: results.facet_counts,
                            item_count: results.response.numFound,
                            page_count: 2,
                            current_page: page_no,
                            price_from: req.query.price_from || null,
                            price_to: req.query.price_to || null
                        })

            return res.json(results);
           }
        });
       

        



    }else{

        if(req.query.price_from && req.query.price_to){
            console.log('in price filter');
            console.log('lower band: '+Number(req.query.price_from - 1));
            condition["offers.price"] = {$gt: Number(req.query.price_from - 1), $lt: Number(req.query.price_to + 1)}
        }

        Product
            .paginate(condition, page_no, req.app.get('total_items_per_page'),

            function(error, pageCount, results, itemCount){
                if(!error){
                    if(results.length > 0){
                        console.log(JSON.stringify(results));

//                    res.json(results);
                        return res.render('v2/index', {
                            page: page,
                            og: og,
                            products: results,
                            item_count: itemCount,
                            page_count: pageCount,
                            current_page: page_no,
                            price_from: req.query.price_from || null,
                            price_to: req.query.price_to || null
                        })
                    }else{
                        return res.render('v2/404', {});
                    }
                }else{
                    return res.render('v2/500', {})
                }


            }, {sortBy: {uploaded_on: -1}})
    }









}




// module.exports = function(req, res){

//     var tag = req.params.tag;

//     var filter = {
// //        state: 'active',
// //        status: 'published'
//     }

//     tag = tag.split('-').join(" ");

//     if(tag){
//         filter.tags = new RegExp(tag, "i")
//     }

//     var page_no = req.query.page || 1

//     var page = {
//         name: 'Shopping',
//         sub_name: '',
//         title: tag+' - Compare prices of '+tag+settings.meta.index.meta_title,
//         meta_title: tag+' - Compare prices of '+tag+settings.meta.index.meta_title,
//         meta_description: 'Shop and compare prices of '+tag+settings.meta.index.meta_description
//     }

//     var og = {
//         type: 'website',
//         image: settings.domain+'/images/bestprices.png'
//     }

//     console.log('meta title: '+page.meta_title)

//     Product.paginate(filter, page_no, 20, function(err, pageCount, results, itemCount){
//         if(err){
//             return res.render('v2/500', {});

//         }else{
//             console.log('size of tags results'+results.length)
//             return res.render('v2/search', {
//                 page: page,
//                 og: og,
//                 products: results,
//                 current_page: page_no,
//                 page_count: pageCount,
//                 item_count:itemCount,
//                 search: tag
//             })
//         }
//     })


// }