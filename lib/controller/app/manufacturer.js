var Manufacturer = require('../../../lib/models/manufacturer');
var Product = require('../../../lib/models/product');
var settings = require("../../../settings");

exports.list = function(req, res){
    var page_no = 1;

    var condition = {};

    if(req.query.page){
        page_no = req.query.page;
    }

    Manufacturer
        .paginate(condition, page_no, req.app.get('total_items_per_page'),
        function(error, pageCount, result, itemCount){

            if(!error && result.length > 0){
                var page = {
                    name: 'Brands',
                    meta_title: settings.meta.manufacturers.meta_title,
                    meta_description: settings.meta.manufacturers.meta_description,
                    sub_name: ''
                }

                return res.render(req.app.get("theme")+'/brands', {
                    page: page,
                    og:{
                        type: settings.og.type,
                        image: settings.og.image
                    },
                    item_count: itemCount,
                    page_count: pageCount,
                    brands: result
                })

            }else{
                if(result.length === 0){
                    return res.render('v2/404', {});
                }else{
                    return res.render('v2/500', {});
                }

            }





        })



//    res.render('v1/index', {})
}

exports.manufacturer = function(req, res){

    var slug = req.params.slug;

    var page_no = 1;

    if(req.query.page){
        page_no = req.query.page;
    }


    Manufacturer.findOne({slug: slug}, function(error, brand){
        if(!error && brand != null){

            var page = {
                name: 'Brands',
                sub_name: brand.name,
                meta_title: brand.meta_title,
                meta_description: brand.meta_description
            }

            Product
                .paginate({manufacturer: brand._id, state:"active", status:"published"}, page_no, req.app.get('total_items_per_page'),

                function(error, pageCount, result, itemCount){

                    if(!error){
                        return res.render(req.app.get("theme")+'/brand', {
                            brand: brand,
                            products: result,
                            item_count: itemCount,
                            page_count: pageCount,
                            current_page: page_no,
                            page: page,
                            og:{
                                type: 'company',
                                image: settings.domain+brand.logo,
                            }
                        })
                    }else{
                        return res.render('v2/500', {});
                    }

                })



        }else{
            if(brand === null){
                return res.render('v2/404', {});
            }else{
                return res.render('v2/500', {});
            }
        }

    })




//    res.render('v1/manufacturer', {})
}