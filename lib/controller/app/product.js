var Product = require('../../models/product');
var Offer = require('../../models/offer');

//var minimum_price = require('../../../template_tags').
var _ = require('underscore');
var settings = require('../../../settings.js');

module.exports = function(req, res){

    var slug = req.params.slug;

    Product.findOne({slug: slug}).populate('manufacturer offers.store category').exec(function(error, product){
        console.log('product: '+JSON.stringify(product));
        if(!error && product != null){
            console.log("in no error");
            var page = {
                name: 'Shopping',
                sub_name: 'Product',
                meta_title: product.meta_title,
                meta_description: product.meta_description
            }

            var og = {
                type: 'product',
                image: settings.domain+product.photos[0],
                price_amount: _.min(product.offers, 'price').price,
                price_currency: settings.currency_in_string
            }

            var pageviews = product.page_views || 0
            product.page_views = pageviews + 1;
            product.save();

            return res.render(req.app.get("theme")+'/product', {
                page: page,
                product: product,
                og: og
            });

        }else{
            if(product === null){
                return res.render('v2/404', {});
            }else{
                return res.render('v2/500', {});
            }

        }
    })



}