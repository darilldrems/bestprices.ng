/*
* Categories result page
* handles filters with it
* */

var Product = require('../../models/product');
var Category = require('../../models/category');

//dhrhggfbfsgfsgfsgfsgfgfgfgdfgdf
exports.category = function(req, res){

    var page_no = 1;
    if(req.query.page) page_no = req.query.page;

    Category.findOne({slug: req.params.slug}, function(error, category){
        if(!error && category){

            var condition = {};

            Category.getChildren(category._id).then(function(children){

                var ids = [];
                ids.push(category._id);
                if(children.length > 0){
                    for(var i = 0; i < children.length; i++){
                        ids.push(children[i]._id);
                    }

                }
                console.log(ids);
                condition.status = 'published'
                condition.category = {$in: ids}

                var brand_cond = condition;

                if(req.query.price_from && req.query.price_to){
                    console.log('in price filter');
                    console.log('lower band: '+Number(req.query.price_from - 1));
                    condition["offers.price"] = {$gt: Number(req.query.price_from - 1), $lt: Number(req.query.price_to + 1)}
                }

                var filtered_brand = "";
                if(req.query.brand){
                    console.log('brand slog: '+req.query.brand);

                    var tmp_array = String(req.query.brand).split('-');
                    var len = tmp_array.length;
                    condition["manufacturer"] = tmp_array[len-1];
                    filtered_brand = tmp_array[0];
                }




                Product
                    .paginate(condition, page_no, req.app.get('total_items_per_page'), function(error, pageCount, result, itemCount){
                        if(!error){

                                var page = {
                                    meta_title: category.meta_title,
                                    meta_description: category.meta_description,
                                    name: 'Shopping',
                                    sub_name: 'Categories'
                                }

                                var og = {
                                    type: 'website',
                                    image: 'http://bestprices.ng/images/bestprices.png'
                                }

                                Product.findManufacturers({category:condition.category}).then(function(resp){
                                    console.log('brands: '+resp);
                                    return res.render(req.app.get("theme")+'/categories', {
                                        category: category,
                                        brands: resp,
                                        page: page,
                                        og:og,
                                        products: result,
                                        item_count: itemCount,
                                        page_count: pageCount,
                                        price_from: req.query.price_from || null,
                                        price_to: req.query.price_to || null,
                                        filtered_brand: filtered_brand,
                                        current_page: page_no

                                    })
                                })

                        }else{
//                       TODO error page
                            return res.render('v2/500', {});
                            console.log(error);
//                       return res.json(error);
                        }
                    }, {populate: "offers.store"})






            })



        }else{
//            TODO error page 500
            return res.render('v2/500', {});
            console.log(error);
//            return res.json(error);
        }

    })


//    return res.render('v1/index', {});


}

