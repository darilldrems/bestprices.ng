var Subscriber = require('../models/subscriber');


exports.edit = function(req, res){
    Subscriber.findOneAndUpdate({email: req.body.email}, {$set:req.body}, function(error, s){
        if(error){
            return res.json({error: error});
        }else{
            return res.json({subscriber:s});
        }
    })
}


exports.list = function(req, res){

    if(req.query.doc == "true"){
        return res.json({
            'title': 'list of email subscribers',
            'query': 'page: the page number, total: the total emails per result'
        });
    }

    console.log('in subscribers');

    var page = 1;
    var total_per_page = 10;

    if(req.query.page)
        page = req.query.page;

    if(req.query.total)
        total_per_page = req.query.total;

    Subscriber.paginate({}, page, total_per_page, function(error, pageCount, result, itemCount){
        if(error){
            console.log("in error");
            return res.json({
                error: error
            });
        }else{
            console.log("no error");
            return res.json({
                'subscribers':result,
                'page_count': pageCount,
                'item_count': itemCount
            })
        }
    });

}