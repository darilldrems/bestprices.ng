require('datejs');
var Subscriber = require('../models/subscriber');

/*
 * controller function that manages visitors email subscription
 * collects email, status to subscribed, subscribed_for
 * */
module.exports = function(req, res){
    var data = req.query;
//    console.log(req.query);
    if(req.param('doc') == "true"){
        return res.json(
            {
                'title': "route to subscribe and un subscribe email",
                'action': {
                    "subscribe": "url query to subscribe: email status(subscribe) subscribed_for(pre-launch)",
                    "un subscribe": "url query to un subscribe: email status(un-subscribe)"
                }
            }
        );
    }

//    unsubscribe's users
    if(data.status == 'un-subscribe'){
        data.un_subscribed_on = Date.today();

        Subscriber.findOne({email: data.email}, function(err, s){
            if(err){
                return res.json({
                    error: 'subscriber does not exist'
                });
            }else{
                Subscriber.findOneAndUpdate({email: data.email}, {$set:data}, function(err, s){
                    if(err){
                        return res.jsonp({
                            error: err
                        });
                    }else{
                        return res.jsonp({
                            success: 'email successfully un-subscribed'
                        });
                    }
                });
            }
        });
    }else{
        var subscriber = new Subscriber(data);
        subscriber.save(function(err, sub){
           if(err){
               return res.jsonp({
                   error: err
               });
           }else{
               return res.jsonp({
                   success: "successfully subscribed"
               })
           }
        });

    }

}