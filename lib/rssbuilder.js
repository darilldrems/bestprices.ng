var mustache = require('mustache');

var template = '<?xml version="1.0" encoding="UTF-8" ?>\
                <rss version="2.0"> \
                    <channel> \
                        <title>{{title}}</title> \
                        <link>{{url}}</link> \
                        <description>{{description}}</description>\
                        {{#items}}\
                        <item>\
                            <title>{{title}}</title>\
                            <link>{{url}}</link>\
                            <pubDate>{{pubDate}}</pubDate>\
                            <description>\
                            <![CDATA[{{content}}]]></description>\
                        </item>\
                        {{/items}}\
                        </channel>\
                        </rss>'

var feed = function(t, url, desc){
    this.obj = {
        title: t,
        url: url,
        description: desc,
        items: []
    };

    this.addNewItem = function(title, url, pubDate, content){
        this.obj.items.push({title: title, url: url, pubDate: pubDate.toDateString(), content: content})
    }

    this.getFeedXML = function(){
        return mustache.render(template, this.obj)
    }


}

module.exports = feed;