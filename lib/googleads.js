var google_ads_snippets = {
    'vertical-link-ad': '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script> \
<!-- bestprices-small-vertical-link-ad --> \
        <ins class="adsbygoogle" \
        style="display:inline-block;width:200px;height:90px" \
        data-ad-client="ca-pub-0564528186799166" \
        data-ad-slot="8486703588"></ins> \
        <script> \
        (adsbygoogle = window.adsbygoogle || []).push({}); \
</script>',
    'horizontal-link-ad': '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script> \
        <!-- bestprices-horizontal-link-ad --> \
        <ins class="adsbygoogle" \
        style="display:inline-block;width:728px;height:15px" \
        data-ad-client="ca-pub-0564528186799166" \
        data-ad-slot="7311021589"></ins> \
        <script> \
        (adsbygoogle = window.adsbygoogle || []).push({}); \
</script>',
    'box-ad': '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script> \
        <!-- bestprices-box-ad --> \
        <ins class="adsbygoogle" \
        style="display:inline-block;width:300px;height:250px" \
        data-ad-client="ca-pub-0564528186799166" \
        data-ad-slot="5694687582"></ins> \
        <script> \
        (adsbygoogle = window.adsbygoogle || []).push({}); \
</script>',
    'horizontal-banner-ad': '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script> \
        <!-- bestprices-horizontal-banner-ad --> \
        <ins class="adsbygoogle" \
        style="display:inline-block;width:728px;height:90px" \
        data-ad-client="ca-pub-0564528186799166" \
        data-ad-slot="2741221183"></ins> \
        <script> \
        (adsbygoogle = window.adsbygoogle || []).push({}); \
</script>'

}



module.exports = google_ads_snippets;