
/**
 * Module dependencies.
 */

var express = require('express');


var http = require('http');
var path = require('path');
var mongoose = require('mongoose');
var os = require('os');
var favicon = require('serve-favicon');

var hooks = require('./lib/hooks');

var subdomain = require('subdomain');
var settings = require('./settings');

//event emitter

var e = require('events')
var events = new e.EventEmitter();

//require('./lib/hooks')(events);

var app = express();

app.use(favicon('public/images/favicon.png'));



//cross origin allow
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', "http://"+"blog."+settings.host);
    res.setHeader('Access-Control-Allow-Origin', "http://"+"howto."+settings.host);
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

//console.log('host:'+os.hostname());
if(settings.state == 'production'){
//    console.log(os.hostname());
    app.set('env', 'production');
}



//all url paths to the application



// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

//app.use(express.favicon());
app.engine('html', require('ejs').renderFile);
app.engine('jade', require('jade').__express);
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.bodyParser());
app.use(express.methodOverride());

app.use(express.static(path.join(__dirname, 'public')));
app.use('/bower_components',  express.static(__dirname + '/bower_components'));
app.use(express.cookieParser('S3CRE7'));
app.use(express.cookieSession());

hooks(events);

app.set('events', events);
//app.use()

app.locals.moment = require('moment');


//initiate template tags
require('./template_tags')(app);


//app.use(app.router);



app.set('total_items_per_page', 20);
app.set('root_folder', __dirname);

//set the theme in use

app.set("theme", "v3");

if('production' == app.get('env')){
    var dbPath = "mongodb://localhost/bestprices";
    app.use(subdomain({ base : settings.host, removeWWW : true }));
    app.locals.domain = settings.domain;
}else{
    var dbPath = "mongodb://localhost/bestprices_test";
    app.use(subdomain({ base : 'localhost', removeWWW : true }));
    app.locals.domain = 'http://localhost:3000';
}

mongoose.connect(dbPath);



// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

if (app.get('env') === 'development') {
    app.locals.pretty = true;

}



require('./url')(app);

//404 error is the last route
app.use(function(req, res, next){
    res.status(404);

    return res.render('v2/404', {});
})


app.set('fb_admins', '696274040')


if(app.get('env') == 'production'){
    app.set('port', 8080);
}

http.createServer(app).listen(app.get('port'), function(){
    console.log('Express server listening on port ' + app.get('port'));
});


