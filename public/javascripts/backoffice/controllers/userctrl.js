var usersCtrl = function($scope, usersList, Api){
    $scope.users = usersList.users;

    $scope.page_count = usersList.page_count;

    $scope.current_page = 1;

    $scope.roles = [];

    $scope.action = 'delete';

    $scope.performAction = function(){

        if($scope.items_selected){
            if($scope.action == "delete"){
                $scope.$broadcast('loading', {});
                console.log($scope.items_selected);
                Api.bulkDelete('user', $scope.items_selected).then(function(res){

                    if(res.success == 0){
                        $scope.$broadcast('loading-done', {});
                        $scope.showAlert('alert-danger', 'delete failed');
                    }else{
                        Api.list('users').then(function(data){
                            $scope.$broadcast('loading-done', {});
                            $scope.users = data.users;
                        })
                    }
                })
            }
        }

    }

    $scope.paginate = function(action){
        if($scope.page_count >= $scope.current_page){
            $scope.$broadcast('loading', {});
            if(action == 'next'){
                $scope.current_page = $scope.current_page + 1;
                Api.list('users', $scope.current_page, $scope.filter).then(function(data){
                    $scope.$broadcast('loading-done', {});
                    if(data.users){
                        $scope.users = data.users;
                    }else{
                        $scope.message = 'No data available';
                    }
                })
            }else{
                if($scope.current_page > 1){
                    $scope.current_page = $scope.current_page - 1;
                    Api.list('users', $scope.current_page, $scope.filter).then(function(data){
                        $scope.$broadcast('loading-done', {})
                        if(data.users){
                            $scope.users = data.users;
                        }else{
                            $scope.message = 'No data available';
                        }
                    })
                }else{
                    $scope.$broadcast('loading-done', {})
                }
            }
        }
    }



}

usersCtrl.$dependency = ['$scope', 'usersList', 'Api'];



var userCreateCtrl = function($scope, Api, roles, $location){

    $scope.new_user = {
        email: '',
        password: '',
        first_name: '',
        last_name: '',
        role: '',
        photo: '',
        fb_url: '',
        twitter_handle: '',
        google_plus: ''
    }

    $scope.roles = roles;

    $scope.password = '';
    $scope.confirm_password = '';

    $scope.createUser = function(){
        if($scope.userForm.$valid){
            if($scope.password === $scope.confirm_password){
                $scope.$broadcast('loading', {});
                $scope.new_user.password = $scope.password;
                Api.create('user/new', $scope.new_user).then(function(res){
                    $scope.$broadcast('loading-done', {});
                    if(res.error){
                        $scope.showAlert('alert-danger', 'Error occurred. User not created.')
                    }else{
                        $location.path('users');
                    }
                })
            }else{
                $scope.showAlert('alert-warning', 'Passwords must be the same');
            }


        }

    }

}

userCreateCtrl.$dependency = ['$scope', 'Api', 'roles', '$location'];


var userEditCtrl = function($scope, user, Api, roles){
    $scope.user = user

    $scope.roles = roles;

    $scope.editUser = function(){


            $scope.$broadcast('loading', {});


            Api.edit('user', $scope.user._id, $scope.user).then(function(res){
                $scope.$broadcast('loading-done', {});
                if(res.error){
                    console.log("error: "+JSON.stringify(res.error, null, 4));
                    $scope.showAlert('alert-danger', 'Error occurred');
                }else{
                    $scope.showAlert('alert-success', 'User details successfully edited');
                }
            })




    }
}

userEditCtrl.$dependency = ['$scope', 'user', 'Api', 'roles'];