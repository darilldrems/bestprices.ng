var galleryCtrl = function($scope, fileUpload, imagesList, Api){

    $scope.images = imagesList.images;

    $scope.current_page = 1;
    $scope.page_count = imagesList.page_count;

    $scope.item_count = imagesList.item_count;


    $scope.data = {
        alt: '',
        title: ''
    }

    $scope.filter = {}


    $scope.query = '';


    $scope.action = '';

    $scope.search = function(){
        if($scope.query){
            $scope.$broadcast('loading', {});
            Api.search('image', $scope.query).then(function(res){
                $scope.$broadcast('loading-done', {});
                if(res.result){
                    $scope.images = res.result;
                }else{
                    $scope.showAlert('alert-warning', 'No result found');
                }
            })
        }
    }


    $scope.performAction = function(){
        console.log($scope.action);
        if($scope.items_selected){
            if($scope.action == "delete"){
                console.log('inside delete');
                $scope.$broadcast('loading', {});
                console.log($scope.items_selected);
                Api.bulkDelete('image', $scope.items_selected).then(function(res){

                    if(res.success == 0){
                        $scope.$broadcast('loading-done', {});
                        console.log('inside success');
                        $scope.showAlert('alert-danger', 'delete failed');
                    }else{
                        Api.list('images').then(function(data){
                            console.log("inside failure");
                            $scope.$broadcast('loading-done', {});
                            $scope.images = data.images;
                        })
                    }
                })
            }
        }

    }








    $scope.uploadFile = function(){
        if($scope.uploadForm.$valid){
            $scope.$broadcast('loading', {});
            var file = $scope.myFile;
            console.log('file is ' + JSON.stringify(file));
            var uploadUrl = "/backend/backoffice/api/upload";
            fileUpload
                .uploadFileToUrl(file, uploadUrl, $scope.data)
                .success(function(){

                    Api.list('images').then(function(res){
                        $scope.$broadcast('loading-done', {});
                        $scope.images = res.images;
                        $scope.page_count = res.page_count;

                        $scope.data.alt = '';
                        $scope.data.title = '';
                        $scope.myFile = '';
                    })

                    console.log("done successfully");
                })
                .error(function(){
                    $scope.$broadcast('loading-done', {});
                    console.log("done with error");
                });


        }
    };

    $scope.paginate = function(action){
        if($scope.page_count >= $scope.current_page){
            $scope.$broadcast('loading', {});
            if(action == 'next'){
                $scope.current_page = $scope.current_page + 1;
                Api.list('images', $scope.current_page, $scope.filter).then(function(data){
                    $scope.$broadcast('loading-done', {});
                    if(data.images){
                        $scope.images = data.images;
                    }else{
                        $scope.message = 'No data available';
                    }
                })
            }else{
                if($scope.current_page > 1){
                    $scope.current_page = $scope.current_page - 1;
                    Api.list('images', $scope.current_page, $scope.filter).then(function(data){
                        $scope.$broadcast('loading-done', {})
                        if(data.images){
                            $scope.images = data.images;
                        }else{
                            $scope.message = 'No data available';
                        }
                    })
                }else{
                    $scope.$broadcast('loading-done', {})
                }
            }
        }
    }
}

galleryCtrl.$dependency = ['$scope', 'fileUpload', 'imagesList', 'Api'];