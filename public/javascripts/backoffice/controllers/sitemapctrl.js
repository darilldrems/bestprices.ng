var sitemapCtrl = function($scope, Api, sitemapsList){
	$scope.new_sitemap = {
		url: '',
		frequency: '',
		priority: '',
		
	}

	$scope.sitemaps = sitemapsList.sitemaps;
	
	
	$scope.current_page = 1;

	$scope.filter = {};

	$scope.create = function(){
		if($scope.sitemapForm.$valid){
            console.log("cliocked button")
			$scope.$broadcast('loading', {});
			Api.create('sitemap/new', $scope.new_sitemap).then(function(res){
				if(res.error){
					$scope.showAlert('alert-danger', res.error.message);
                     $scope.$broadcast('loading-done', {});
				}else{
					Api.list('sitemaps', $scope.current_page, $scope.filter).then(function(res){
						if(!res.error)
							$scope.sitemaps = res.sitemaps;
                         $scope.$broadcast('loading-done', {});
					});		
				}
					
			});
		}
	}     

   
 
}

todoCtrl.$dependency = ['$scope', 'Api', 'sitemapsList'];

