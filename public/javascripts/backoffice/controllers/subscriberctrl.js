/*
* The file contains 3 controllers for products
* 1. productsListCtrl
* 2. productCreateCtrl
* 3. productEditCtrl
* */

/*
* controller for displaying products list and filtering
* */
var subscribersListCtrl = function($scope, subscribersList, Api){
    $scope.subscribers = subscribersList.subscribers;

    // alert(JSON.stringify(subscribersList))

    $scope.page_count = subscribersList.page_count;
    $scope.current_page = 1;

    $scope.item_count = subscribersList.item_count;

    
    // alert(JSON.stringigySettings)

    

    $scope.action = '';

    


    $scope.paginate = function(action){
        
        if($scope.page_count >= $scope.current_page){
            $scope.$broadcast('loading', {});
            if(action == 'next'){
                $scope.current_page = $scope.current_page + 1;
                Api.list('subscribers', $scope.current_page, $scope.filter).then(function(data){
                    $scope.$broadcast('loading-done', {});
                    if(!data.error){
                        $scope.subscribers = data.subscribers;
                    }else{
                        $scope.message = 'No data available';
                    }
                })
            }else{
                if($scope.current_page > 1){
                    $scope.current_page = $scope.current_page - 1;
                    Api.list('products', $scope.current_page, $scope.filter).then(function(data){
                        $scope.$broadcast('loading-done', {})
                        if(!data.error){
                            $scope.subscribers = data.subscribers;
                        }else{
                            $scope.message = 'No data available';
                        }
                    })
                }else{
                    $scope.$broadcast('loading-done', {})
                }
            }
        }
    }

}

productsListCtrl.$dependency = ['$scope', 'subscribersList', 'Api'];



