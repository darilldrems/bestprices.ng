/*
* The file contains 3 controllers for products
* 1. productsListCtrl
* 2. productCreateCtrl
* 3. productEditCtrl
* */

/*
* controller for displaying products list and filtering
* */
var productsListCtrl = function($scope, productsList, Api, categoriesFilter, usersFilter, manufacturersFilter){
    $scope.products = productsList.products;

    $scope.page_count = productsList.page_count;
    $scope.current_page = 1;

    $scope.item_count = productsList.item_count;

    $scope.usersFilter = usersFilter.users;
    $scope.categoriesFilter = categoriesFilter.categories;
    $scope.manufacturersFilter = manufacturersFilter.manufacturers;

    $scope.pfilter = {
        "category": "",
        "manufacturer": "",
        "uploaded_by": "",
        "uploaded_on": "",
        "offers_last_updated": ""
    }

    // alert(JSON.stringify($scope.usersFilter));
    // alert(JSON.stringigySettings)

    $scope.filter = {

    }

    $scope.filter_offer_size = ""

    $scope.action = '';

    $scope.search = function(){
        console.log('search button clicked');
        if($scope.query){
            console.log('inside query if');
            $scope.$broadcast('loading', {});
            Api.search('product', $scope.query).then(function(res){
                $scope.$broadcast('loading-done', {});
                if(res.result){
                    $scope.products = res.result;
                }else{
                    $scope.showAlert('alert-warning', 'No result found');
                }
            })
        }
    }

    $scope.filterList = function(){
        // alert(JSON.stringify($scope.filter))
        for(var key in $scope.pfilter){
            if($scope.pfilter[key] || $scope.filter[key]){
                $scope.filter[key] = $scope.pfilter[key];
                if(!$scope.filter[key])
                        delete $scope.filter[key];
            }
        }
        if($scope.filter_offer_size){
            // alert("in here")
            $scope.filter["offers.0"] = "{\$exists: false}";

        }

        if(!$scope.filter_offer_size && $scope.filter["offers.0"]){
            // alert("in here")
            delete $scope.filter["offers.0"]

        }

        if($scope.filter){
            // alert(JSON.stringify($scope.filter));
            $scope.$broadcast('loading', {});
            Api.list('products', $scope.current_page, $scope.filter).then(function(data){
                        $scope.$broadcast('loading-done', {})
                        if(data.products){
                            $scope.products = data.products;
                        }else{
                            $scope.message = 'No data available';
                        }
                    })
        }
    }

    $scope.performAction = function(){

        if($scope.items_selected){
            if($scope.action == "delete"){
                $scope.$broadcast('loading', {});
                console.log($scope.items_selected);
                Api.bulkDelete('product', $scope.items_selected).then(function(res){

                    if(res.success == 0){
                        $scope.$broadcast('loading-done', {});
                        $scope.showAlert('alert-danger', 'delete failed');
                    }else{
                        Api.list('products').then(function(data){
                            $scope.$broadcast('loading-done', {});
                            $scope.products = data.products;
                        })
                    }
                })
            }
        }

    }


    $scope.paginate = function(action){

        if($scope.page_count >= $scope.current_page){
            $scope.$broadcast('loading', {});
            if(action == 'next'){
                $scope.current_page = $scope.current_page + 1;
                Api.list('products', $scope.current_page, $scope.filter).then(function(data){
                    $scope.$broadcast('loading-done', {});
                    if(data.products){
                        $scope.products = data.products;
                    }else{
                        $scope.message = 'No data available';
                    }
                })
            }else{
                if($scope.current_page > 1){
                    $scope.current_page = $scope.current_page - 1;
                    Api.list('products', $scope.current_page, $scope.filter).then(function(data){
                        $scope.$broadcast('loading-done', {})
                        if(data.products){
                            $scope.products = data.products;
                        }else{
                            $scope.message = 'No data available';
                        }
                    })
                }else{
                    $scope.$broadcast('loading-done', {})
                }
            }
        }
    }

}

productsListCtrl.$dependency = ['$scope', 'productsList', 'Api', 'categoriesFilter', 'usersFilter', 'manufacturersFilter'];



/*
* controller for creating new product
* */
var productCreateCtrl = function($scope, Api, manufacturersList, categoriesList, $location, Settings){
    $scope.new_product = {
        title: '',
        slug: '',
        meta_title: '',
        videos: '',
        description: '',
        meta_description: '',
        tags:'',
        wikipedia: '',
        official_website: '',
        manufacturer: '',
        category: '',
        photos: [],
        status: '',
        features_summary: '',
        summary:'',
        is_hot: ''
    }

    $scope.settings = Settings;
    $scope.manufacturers_options = manufacturersList.manufacturers;
    $scope.categories_options = categoriesList.categories;

    $scope.image1 = '';
    $scope.image2 = '';
    $scope.image3 = '';
    $scope.image4 = '';

    $scope.addPhoto = function(){
        if($scope.image1.trim() != ''){
            $scope.new_product.photos.push($scope.image1);
        }
        if($scope.image2.trim() != '') {
            $scope.new_product.photos.push($scope.image2);
        }
        if($scope.image3.trim() != '') {
            $scope.new_product.photos.push($scope.image3);
        }
        if($scope.image4.trim() != '') {
            $scope.new_product.photos.push($scope.image4);
        }
    }

    $scope.createProduct = function(status){
        console.log($scope.new_product.category);
        console.log(status);
        if($scope.productForm.$valid){
            $scope.$broadcast('loading', {});
            $scope.addPhoto();
            $scope.new_product.status = status;

            console.log(JSON.stringify($scope.new_product))

            Api.create('product/new', $scope.new_product).then(function(data){
                $scope.$broadcast('loading-done', {})
                if(data.error){
                    console.log(JSON.stringify(data.error))
                    $scope.showAlert('alert-danger', 'Error occurred. Unable to create product');
//                    $scope.message = 'unable to create product';
                }else{
                    $location.path('/products');
                }
            })

        }
    }
}

productCreateCtrl.$dependency = ['$scope', 'Api', 'manufacturersList', 'categoriesList', '$location', 'Settings'];


/*
*controller edit product
* */
var productEditCtrl = function($scope, Api, product, categoriesList, manufacturersList, storesList){

    $scope.product = product.product;


    console.log(JSON.stringify(product.product.offers));

    $scope.offers = product.product.offers;

    $scope.stores = storesList.stores;

    $scope.categories = categoriesList.categories;

    $scope.manufacturers = manufacturersList.manufacturers;

    $scope.editProduct = function(status){

        $scope.product.status = status;

        if($scope.editForm.$valid){
            $scope.$broadcast('loading', {});
            Api.edit('product', $scope.product._id, $scope.product).then(function(res){
                $scope.$broadcast('loading-done', {});
                if(res.product){
                    $scope.showAlert('alert-success', 'Successfully edited product');
                    $scope.product = res.product;
                    $scope.offers = res.product.offers;
                }else{
                    console.log(JSON.stringify(res.error));
                    $scope.showAlert('alert-danger', 'Some error occured');
                }
            })

        }



    }

    $scope.new_offer = {
        store: '',
        link: '',
        product: $scope.product._id,
        product_state: ''
    }

    $scope.manual_offer = {
        product: $scope.product._id,
        store: '',
        link: '',
        price: '',
        product_state: ''
    }



    $scope.addOffer = function(new_offer){
        if($scope.offerForm.$valid || $scope.manualOfferForm.$valid){
            $scope.$broadcast('loading', {});
            Api.addOffer(new_offer).then(function(res){
                new_offer.store = '';
                new_offer.link = '';

                if(new_offer.price){
                    new_offer.price = '';
                }

                $scope.$broadcast('loading-done', {});
                if(res.product.offers){
                    $scope.offers = res.product.offers;
                }else{
                    $scope.showAlert('alert-danger', 'Unable to add offer. try again');
                }
            })
        }

    }

    $scope.deleteOffer = function(id){
        $scope.$broadcast('loading', {});
        Api.deleteOffer('offer', $scope.product._id, id).then(function(res){
            $scope.$broadcast('loading-done', {});
            if(res.status === "success"){
                console.log(res.status);
                console.log('in success');
                $scope.product = res.product;
                $scope.offers = res.product.offers;
            }else{

                $scope.showAlert('alert-danger', 'Unable to delete offer. try again');
            }
        })
    }

    var selected_offers = [];

    $scope.toggleSelectedOffer = function(selected_id){
        var index = selected_offers.indexOf(selected_id);
        if( index > -1){
            selected_offers.splice(index, 1);
        }else{
            selected_offers.push(selected_id);
        }
    }

    $scope.bulkDeleteOffers = function(){
        if(selected_offers.length > 0){
            $scope.$broadcast('loading', {});
            Api.bulkDeleteOffers($scope.product._id, selected_offers).then(function(res){
                $scope.$broadcast('loading-done', {});
                if(res.status === "success"){
                    console.log(res.status);
                    console.log('in success');
                    $scope.product = res.product;
                    $scope.product.category = res.product.category._id;
                    console.log("category");
                    console.log(JSON.stringify(res.product.category));
                    $scope.offers = res.product.offers;
                }else{
                    $scope.showAlert('alert-danger', 'Unable to delete offer. try again');
                }
            });
        }else{
            alert("Please select offers to bulk delete");
        }
    }

}

productEditCtrl.$dependency = ['$scope', 'Api', 'product', 'categoriesList', 'manufacturersList', 'storesList'];
