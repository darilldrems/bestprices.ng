var articlesCtrl = function($scope, articlesList, Api){
    $scope.articles = articlesList.articles;

    $scope.published_articles = articlesList.published_articles;

    $scope.item_count = articlesList.item_count;

    $scope.current_page = 1;

    $scope.page_count= articlesList.page_count;


    $scope.query = '';

    $scope.action = '';


    $scope.search = function(){
        if($scope.query){
            $scope.$broadcast('loading', {});
            Api.search('article', $scope.query).then(function(res){
                $scope.$broadcast('loading-done', {});
                if(res.result){
                    $scope.articles = res.result;
                }else{
                    $scope.showAlert('alert-warning', 'No result found');
                }
            })
        }
    }


    $scope.performAction = function(){

        if($scope.items_selected){
            if($scope.action == "delete"){
                $scope.$broadcast('loading', {});
                console.log($scope.items_selected);
                Api.bulkDelete('article', $scope.items_selected).then(function(res){

                    if(res.success == 0){
                        $scope.$broadcast('loading-done', {});
                        $scope.showAlert('alert-danger', 'delete failed');
                    }else{
                        Api.list('articles').then(function(data){
                            $scope.$broadcast('loading-done', {});
                            $scope.articles = data.articles;
                        })
                    }
                })
            }
        }

    }




    $scope.paginate = function(action){
        if($scope.page_count >= $scope.current_page){
            $scope.$broadcast('loading', {});
            if(action == 'next'){
                $scope.current_page = $scope.current_page + 1;
                Api.list('articles', $scope.current_page, $scope.filter).then(function(data){
                    $scope.$broadcast('loading-done', {});
                    if(data.articles){
                        $scope.articles = data.articles;
                    }else{
                        $scope.message = 'No data available';
                    }
                })
            }else{
                if($scope.current_page > 1){
                    $scope.current_page = $scope.current_page - 1;
                    Api.list('articles', $scope.current_page, $scope.filter).then(function(data){
                        $scope.$broadcast('loading-done', {})
                        if(data.articles){
                            $scope.articles = data.articles;
                        }else{
                            $scope.message = 'No data available';
                        }
                    })
                }else{
                    $scope.$broadcast('loading-done', {})
                }
            }
        }
    }
}

articlesCtrl.$dependency = ['$scope', 'articlesList', 'Api'];


var articleCreateCtrl = function($scope, Api, categoriesList, $location, articleTypes){

    $scope.new_article = {
        title: '',
        slug: '',
        meta_title: '',
        meta_description: '',
        content: '',
        article_type: '',
        status: '',
        tags: '',
        category:'',
        summary: '',
        thumbnail: ''
    }

    $scope.categories = categoriesList.categories;

    $scope.article_types = articleTypes;

    $scope.saveArticle = function(status){
        if($scope.articleForm.$valid){
            $scope.$broadcast('loading', {});
            console.log('valid form');
            $scope.new_article.status = status;
            Api.create('article/new', $scope.new_article).then(function(res){
                $scope.$broadcast('loading-done', {});
                console.log(res);
                if(res.error){
                    $scope.showAlert('alert-danger', 'Error occurred. Article not created');
                }else{
                    $location.path('articles');
                }
            });
        }
    }


}

articleCreateCtrl.$dependency = ['$scope', 'Api', 'categoriesList', '$location', 'articleTypes'];


var articleEditCtrl = function($scope, article, Api, articleTypes, categoriesList){

    $scope.article = article;

    $scope.article_types = articleTypes;

    $scope.categories = categoriesList.categories;

    $scope.editArticle = function(status){
        if($scope.editForm.$valid){
            $scope.$broadcast('loading', {});
            $scope.article.status = status;

            Api.edit('article', $scope.article._id, $scope.article).then(function(res){
                $scope.$broadcast('loading-done', {});
                if(res.error){
                    $scope.showAlert('alert-danger', 'Error occured');
                }else{
                    $scope.showAlert('alert-success', 'Article saved');
                }
            })

        }


    }

}

articleEditCtrl.$dependency = ['$scope', 'article', 'Api', 'articleTypes', 'categoriesList'];