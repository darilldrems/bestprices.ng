var howtosCtrl = function($scope, howtoList, Api){
    $scope.howtos = howtoList.howtos;

    $scope.published_howtos = howtoList.published_howtos;

    // alert($scope.published_howtos)

    $scope.page_count = howtoList.page_count;

    $scope.current_page = 1;

    $scope.item_count = howtoList.item_count;



    $scope.action = '';

    $scope.query = '';

    $scope.search = function(){
        if($scope.query){
            $scope.$broadcast('loading', {});
            Api.search('howto', $scope.query).then(function(res){
                $scope.$broadcast('loading-done', {});
                if(res.result){
                    $scope.howtos = res.result;
                }else{
                    $scope.showAlert('alert-warning', 'Error occurred');
                }
            })
        }
    }


    $scope.performAction = function(){

        if($scope.items_selected){
            if($scope.action == "delete"){
                $scope.$broadcast('loading', {});
                console.log($scope.items_selected);
                Api.bulkDelete('howto', $scope.items_selected).then(function(res){

                    if(res.success == 0){
                        $scope.$broadcast('loading-done', {});
                        $scope.showAlert('alert-danger', 'delete failed');
                    }else{
                        Api.list('howtos').then(function(data){
                            $scope.$broadcast('loading-done', {});
                            $scope.howtos = data.howtos;
                        })
                    }
                })
            }
        }

    }






    $scope.paginate = function(action){
        if($scope.page_count >= $scope.current_page){
            $scope.$broadcast('loading', {});
            if(action == 'next'){
                $scope.current_page = $scope.current_page + 1;
                Api.list('howtos', $scope.current_page, $scope.filter).then(function(data){
                    $scope.$broadcast('loading-done', {});
                    if(data.howtos){
                        $scope.howtos = data.howtos;
                    }else{
                        $scope.message = 'No data available';
                    }
                })
            }else{
                if($scope.current_page > 1){
                    $scope.current_page = $scope.current_page - 1;
                    Api.list('howtos', $scope.current_page, $scope.filter).then(function(data){
                        $scope.$broadcast('loading-done', {})
                        if(data.products){
                            $scope.howtos = data.howtos;
                        }else{
                            $scope.message = 'No data available';
                        }
                    })
                }else{
                    $scope.$broadcast('loading-done', {})
                }
            }
        }
    }


}

howtosCtrl.$dependency = ['$scope', 'howtoList', 'Api'];



var howtoCreateCtrl = function($scope, Api, $location, categoriesList){

    $scope.howto = {
        title: '',
        slug: '',
        meta_title: '',
        meta_description: '',
        category: '',
        tags: '',
        steps: [],
        status: '',
        summary: '',
        thumbnail: ''
    }

    $scope.item = {
        title: '',
        content: ''
    }

    $scope.categories = categoriesList.categories;

    $scope.createHowTo = function(status){
        if($scope.howtoForm.$valid){
            $scope.$broadcast('loading', {});
            $scope.howto.status = status;

            console.log('howto: '+$scope.howto);
            Api.create('howto/new', $scope.howto).then(function(res){
                $scope.$broadcast('loading-done', {});
                if(res.error){
                    console.log(res.error);
                    $scope.showAlert('alert-danger', 'Error occurred. Could not save');
                }else{
                    $location.path('/howtos');
                }
            })


        }
    }


    $scope.addItem = function(){
        console.log('in here atleast');
        if($scope.item.title && $scope.item.content){


            var safeitem = angular.copy($scope.item);
            $scope.howto.steps.push(safeitem);
            console.log('item title'+$scope.item.title);
            console.log('item content'+$scope.item.content);


            $scope.item.title = '';
            $scope.item.content = '';
        }
    }

}

howtoCreateCtrl.$dependency = ['$scope', 'Api', '$location', 'categoriesList']


var howtoEditCtrl = function($scope, Api, howto, categoriesList){
    $scope.howto = howto;

    $scope.categories = categoriesList.categories;


    $scope.editHowTo = function(status){
        if($scope.editForm.$valid){
            $scope.$broadcast('loading', {});

            $scope.howto.status = status;

            Api.edit('howto', $scope.howto._id, $scope.howto).then(function(res){
                $scope.$broadcast('loading-done', {});
                if(res.error){
                    $scope.showAlert('alert-dander', "Error occurred. How to not saved");
                }else{
                    $scope.showAlert('alert-success', "Successfully saved");
                }
            })

        }
    }

    $scope.addNewItem = function(pos){
        var new_item = {
            title: '',
            content: ''
        }

        $scope.howto.steps.splice(pos+1, 0, new_item);

    }
}

howtoEditCtrl.$dependency = ['$scope', 'Api', 'howto', 'categoriesList'];