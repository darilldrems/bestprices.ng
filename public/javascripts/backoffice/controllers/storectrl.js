var storesListCtrl = function($scope, Api, storesList){
    $scope.stores = storesList.stores;
    $scope.current_page = 1;
    $scope.page_count = storesList.page_count;

    $scope.item_count = storesList.item_count;



    $scope.paginate = function(action){
        // alert($scope.page_count);
        if($scope.page_count >= $scope.current_page){
            $scope.$broadcast('loading', {});
            if(action == 'next'){
                // alert("in next")
                $scope.current_page = $scope.current_page + 1;
                // alert($scope.current_page);
                Api.list('stores', $scope.current_page, $scope.filter).then(function(data){
                    $scope.$broadcast('loading-done', {});
                    if(data.stores){
                        $scope.stores = data.stores;
                    }else{
                        $scope.message = 'No data available';
                    }
                })
            }else{
                if($scope.current_page > 1){
                    $scope.current_page = $scope.current_page - 1;
                    Api.list('stores', $scope.current_page, $scope.filter).then(function(data){
                        $scope.$broadcast('loading-done', {})
                        if(data.stores){
                            $scope.stores = data.stores;
                        }else{
                            $scope.message = 'No data available';
                        }
                    })
                }else{
                    $scope.$broadcast('loading-done', {})
                }
            }
        }
    }
}
storesListCtrl.$dependency = ['$scope', 'Api', 'storesList']




var storeCreateCtrl = function($scope, $location, Api){

    $scope.new_store = {
        name: '',
        slug: '',
        logo: '',
        meta_title: '',
        meta_description: '',
        about: '',
        trusted_shop: false,
        price_xpath: '',
        website: '',
        email: '',
        tel: '',
        address: '',
        affiliate_query: '',
        delivery_note: '',
        is_dynamic: '',
        accepted_payments: '',
        rating: '',
	search_path: ''
    }

    $scope.createStore = function(){
        if($scope.storeForm.$valid){
            $scope.$broadcast('loading', {});
            Api.create('store/new', $scope.new_store).then(function(res){
                $scope.$broadcast('loading-done', {});
                if(res.error){
                    console.log(JSON.stringify(res.error));
                    $scope.showAlert('alert-danger', 'Error occurred. Unable to create store.');
                }else{
                    $location.path('stores');
                }
            })
        }
    }

}
storeCreateCtrl.$dependency = ['$scope', '$location', 'Api'];



var storeEditCtrl = function($scope, store, Api){

    $scope.store = store;


    $scope.editStore = function(){
        if($scope.editForm.$valid){
            $scope.$broadcast('loading', {});
            Api.edit('store', $scope.store._id, $scope.store).then(function(res){
                $scope.$broadcast('loading-done', {});
                if(res.error){
                    console.log(JSON.stringify(res.error));
                    $scope.showAlert('alert-danger', 'Error occured. store not saved');
                }else{
                    $scope.showAlert('alert-success', 'Store successfully saved');
                }
            });

        }
    }


}

storeEditCtrl.$dependency = ['$scope', '$store', 'Api'];
