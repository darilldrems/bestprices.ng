var linkCreateCtrl = function($scope, Api, $location){
    $scope.new_link = {
        title: '',
        url: '',
        external: false,
        group: '',
        meta_title: '',
        meta_description: ''
    }

    $scope.createLink = function(){
        if($scope.linkForm.$valid){

            $scope.$broadcast('loading', {});
            Api.create('link/new', $scope.new_link).then(function(result){
                $scope.$broadcast('loading-done', {});
              if(result.error){
                  $scope.showAlert('alert-danger', 'Error occurred. Link not created');
              }else{
                  $location.path('/links');
              }
            })

        }
    }
}

linkCreateCtrl.$dependency = ['$scope', 'Api', '$location'];


var linkEditCtrl = function($scope, Api, link){

    $scope.link = link;

    console.log('link title'+$scope.link.title);

    $scope.editLink = function(){
        if($scope.editForm){
            $scope.$broadcast('loading', {});
            Api.edit('link', $scope.link._id, $scope.link).then(function(res){
                $scope.$broadcast('loading-done', {});
                if(res.error){
                    $scope.showAlert('alert-danger', 'Error occured. Link not saved');
                }else{
                    $scope.showAlert('alert-success', 'Link successfully saved');
                }
            });
        }
    }


}

linkEditCtrl.$dependency = ['$scope', 'Api', 'link'];


var linksCtrl = function($scope, linksList, Api){

    $scope.links = linksList.links;

    $scope.page_count = linksList.page_count;

    $scope.current_page = 1;


    $scope.paginate = function(action){
        if($scope.page_count >= $scope.current_page){
            $scope.$broadcast('loading', {});
            if(action == 'next'){
                $scope.current_page = $scope.current_page + 1;
                Api.list('links', $scope.current_page, $scope.filter).then(function(data){
                    $scope.$broadcast('loading-done', {});
                    if(data.links){
                        $scope.links = data.links;
                    }else{
                        $scope.message = 'No data available';
                    }
                })
            }else{
                if($scope.current_page > 1){
                    $scope.current_page = $scope.current_page - 1;
                    Api.list('links', $scope.current_page, $scope.filter).then(function(data){
                        $scope.$broadcast('loading-done', {})
                        if(data.links){
                            $scope.links = data.links;
                        }else{
                            $scope.message = 'No data available';
                        }
                    })
                }else{
                    $scope.$broadcast('loading-done', {})
                }
            }
        }
    }




}

linksCtrl.$dependency = ['$scope', 'linksList', 'Api'];