var todoCtrl = function($scope, Api, manufacturersList, categoriesList, todosList){
	$scope.new_todo = {
		title: '',
		category: '',
		manufacturer: '',
		features: '',
		wikipedia: '',
		official_url: '',
		ranking: ''
	}

	$scope.todos = todosList.todos;
	$scope.manufacturers = manufacturersList.manufacturers;
	$scope.categories = categoriesList.categories;
	
	$scope.current_page = 1;

	$scope.filter = {};

	$scope.create = function(){
		if($scope.todoForm.$valid){
            console.log("cliocked button")
			$scope.$broadcast('loading', {});
			Api.create('todo/new', $scope.new_todo).then(function(res){
				if(res.error){
					$scope.showAlert('alert-danger', res.error.message);
                     $scope.$broadcast('loading-done', {});
				}else{
					Api.list('todos', $scope.current_page, $scope.filter).then(function(res){
						if(!res.error)
							$scope.todos = res.todos;
                         $scope.$broadcast('loading-done', {});
					});		
				}
					
			});
		}
	}     

   
 
}

todoCtrl.$dependency = ['$scope', 'Api', 'manufacturersList', 'categoriesList', 'todosList'];

