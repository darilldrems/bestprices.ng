var categoryCreateCtrl = function($scope, $location, Api, categoriesList){

    $scope.new_category = {
        name: '',
        slug: '',
        meta_title: '',
        meta_description: '',
        description: '',
        parent: '',
	       bag_of_words: "",
         position: 100
    }

    $scope.parents = categoriesList.categories;

    $scope.createCategory = function(){
        if($scope.categoryForm.$valid){
            $scope.$broadcast('loading', {})
            console.log('inside form valid');
            Api.create('category/new', $scope.new_category).then(function(data){
                $scope.$broadcast('loading-done', {})
                console.log(data);
                if(data.error){
                    $scope.showAlert('alert-danger', 'Error occured. Unable to create category');
                }else{
                    console.log(data);
                    $location.path('/categories');
                }
            })

        }
    }

}

categoryCreateCtrl.$dependency = ['$scope', '$location', 'Api', 'categoriesList'];



var categoriesCtrl = function($scope, Api, categoriesList){
    $scope.categories = categoriesList.categories;

    $scope.current_page = 1;

    $scope.page_count = categoriesList.page_count;

    $scope.item_count = categoriesList.item_count;


    $scope.filter = {};

    $scope.paginate = function(action){
        if($scope.page_count >= $scope.current_page){
            $scope.$broadcast('loading', {});
            if(action == 'next'){
                $scope.current_page = $scope.current_page + 1;
                Api.list('categories', $scope.current_page, $scope.filter).then(function(data){
                    $scope.$broadcast('loading-done', {});
                    if(data.categories){
                        $scope.categories = data.categories;
                    }else{
                        $scope.message = 'No data available';
                    }
                })
            }else{
                if($scope.current_page > 1){
                    $scope.current_page = $scope.current_page - 1;
                    Api.list('categories', $scope.current_page, $scope.filter).then(function(data){
                        $scope.$broadcast('loading-done', {})
                        if(data.categories){
                            $scope.categories = data.categories;
                        }else{
                            $scope.message = 'No data available';
                        }
                    })
                }else{
                    $scope.$broadcast('loading-done', {})
                }
            }
        }
    }




}

categoriesCtrl.$dependency = ['$scope', 'Api', 'categoriesList'];


var categoryEditCtrl = function($scope, Api, category, categoriesList){

    $scope.category = category;

    $scope.categories = categoriesList.categories;

    $scope.editCategory = function(){
        if($scope.editForm.$valid){
            $scope.$broadcast('loading', {});

            Api.edit('category', $scope.category._id, $scope.category).then(function(res){
                $scope.$broadcast('loading-done', {});
                if(res.error){
                    $scope.showAlert('alert-danger', 'Error occured. edit failed');
                }else{
                    $scope.showAlert('alert-success', 'Successfully edited category');
                }
            })

        }
    }


}

categoryEditCtrl.$dependency = ['$scope', 'Api', 'category', 'categoriesList'];
