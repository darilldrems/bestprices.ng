var baseController = function($scope, $location, $http, $cookies){
    $scope.currentUser = null;

    $scope.setCurrentUser = function(val){
        $scope.currentUser = {
            role: $cookies['role'],
            email: $cookies['email'],
            id: $cookies['id'],
            first_name: $cookies['first_name'],
            last_name: $cookies['last_name'],
            profile: $cookies['profile']
        };

        console.log($scope.currentUser);
        console.log("go rhough base");

    }

    $scope.setCurrentUser();


    $scope.hasPermission = function(permitted, role){
        console.log('sent role id:'+role);
        if(role){
            if(permitted.indexOf(role) > -1){
                return true;
            }
        }

        return false;

    }



    $scope.isPublished = function(article){
        if(article.status === 'published'){
            return true;
        }

        return false;
    }

    $scope.previous = 'previous';
    $scope.next = 'next';

    $scope.draft = 'draft';
    $scope.published = 'published';

    $scope.isActive = function(model){
        if($location.path().indexOf(model) > -1){
            return true;
        }

        return false;
    }

    $scope.logout = function(){
        $http.get('/backend/backoffice/api/logout').then(function(res){
            if(res.data.success){
                $location.path('/login');
            }
        })
    }


    $scope.items_selected = [];
//    $scope.selected_model = null;
    $scope.toggleSelected = function(val){
        var location = $scope.items_selected.indexOf(val);
        if( location > -1){
//            remove it
            $scope.items_selected.splice(location, 1);
        }else{
//            add it to item_selected
            $scope.items_selected.push(val);
        }
        console.log($scope.items_selected);
    }



    $scope.tinymceOptions = {
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        toolbar2: "print preview media | forecolor backcolor emoticons",
        image_advtab: true,
        height: "600px",
        relative_urls: false,
        convert_urls: false,
        remove_script_host: false

    }




    $scope.mainAlert = {
        isShown: false
    };

    $scope.showAlert = function(alertType, message) {
        $scope.mainAlert.message = message;
        $scope.mainAlert.isShown = true;
        $scope.mainAlert.alertType = alertType;
    }

    $scope.closeAlert = function() {
        $scope.mainAlert.message = '';
        $scope.mainAlert.isShown = false;
    };


}

baseController.$dependency = ['$scope', '$location', '$http', '$cookies'];