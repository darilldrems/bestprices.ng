/**
*@author Ridwan Olalere
*@description controller for displaying and editing landing pages
*/

var landingPagesCtrl = function($scope, landingPagesList, Api){
	$scope.landing_pages = landingPagesList.landing_pages;
	$scope.page_count = landingPagesList.page_count;

	$scope.current_page = 1;

	$scope.item_count = landingPagesList.item_count;

	 $scope.paginate = function(action){

        if($scope.page_count >= $scope.current_page){
            $scope.$broadcast('loading', {});
            if(action == 'next'){
                $scope.current_page = $scope.current_page + 1;
                Api.list('landing_pages', $scope.current_page, $scope.filter).then(function(data){
                    $scope.$broadcast('loading-done', {});
                    if(data.landing_pages){
                        $scope.landing_pages = data.landing_pages;
                    }else{
                        $scope.message = 'No data available';
                    }
                })
            }else{
                if($scope.current_page > 1){
                    $scope.current_page = $scope.current_page - 1;
                    Api.list('landing_pages', $scope.current_page, $scope.filter).then(function(data){
                        $scope.$broadcast('loading-done', {})
                        if(data.landing_pages){
                            $scope.landing_pages = data.landing_pages;
                        }else{
                            $scope.message = 'No data available';
                        }
                    })
                }else{
                    $scope.$broadcast('loading-done', {})
                }
            }
        }
    }


}

landingPagesCtrl.$dependency = ['$scope', 'landingPagesList', 'Api'];


var landingPageEditCtrl = function($scope, landingPage, Api){
	$scope.landing_page = landingPage.landing_page;

	$scope.query = "";

    $scope.search_results = [];


    $scope.save = function(status){
        $scope.landing_page.status = status;
        $scope.$broadcast('loading', {});
        Api.edit('landing_page', $scope.landing_page._id, $scope.landing_page).then(function(res){
            $scope.$broadcast('loading-done', {});
            if(res.error){
                alert("An Error occured");
            }else{
                $scope.showAlert('alert-success', 'Successfully edited product');
            }
        });
    }

    //allows to search for product to add to landing page
    $scope.search = function(){
        if($scope.query){
            $scope.$broadcast('loading', {});
            Api.search('product', $scope.query).then(function(res){
                $scope.$broadcast('loading-done', {});
                if(res.result){
                    $scope.search_results = res.result;
                }else{
                    $scope.showAlert('alert-warning', 'No result found');
                }
            })
        }
    }

    $scope.returnLowestPrice = function(offers){
        var lowestPrice = offers[0].price;

        for(var i in offers){
            var off = offers[i];

            if(off.price < lowestPrice)
                lowestPrice = off.price;

        }

        return lowestPrice;
    }

    //adds product to the landing page products array
    $scope.addProduct = function(index){
        if($scope.search_results.length > 0){
            var selected = $scope.search_results[index];

            var already_selected = false;

            for(var i in $scope.landing_page.products){
                var item = $scope.landing_page.products[i];
                if(item._id == selected._id){
                    already_selected = true;
                }
            }

            if(already_selected){
                alert("Product already added");
                return ;
            }

            $scope.landing_page.products.push(selected);
            $scope.search_results.splice(index, 1);
        }
    }

    //allows removal of product from landing page
    $scope.remove = function(index){
        $scope.landing_page.products.splice(index, 1);
    }


}

landingPageEditCtrl.$dependency = ['$scope', 'landingPage', 'Api'];


var landingPageNewCtrl = function($scope, Api){
    $scope.landing_page = {
        meta_title: '',
        meta_description: '',
        title: '',
        description: '',
        banner: '',
        mobile_banner: '',
        slug: '',
        status: 'draft'
    }

    $scope.create = function(){
        if($scope.editForm.$valid){
            $scope.$broadcast('loading', {});
            Api.create('landing_page/new', $scope.landing_page).then(function(res){
                $scope.$broadcast('loading-done', {});
                if(res.error){
                    $scope.showAlert('alert-danger', 'Landing page not created.');
                }else{
                    $scope.showAlert('alert-success', 'Landing page Successfully created.');
                }
            })
        }
    }
}

landingPageNewCtrl.$dependency = ['$scope', 'Api'];