var manufacturerCreateCtrl = function($scope, $location, Api, Settings){
    $scope.new_manufacturer = {
        logo: '',
        slug: '',
        meta_title: '',
        meta_description: '',
        wikipedia: '',
        website: '',
        founded: '',
        about: ''
    }

    $scope.settings = Settings;
    $scope.createManufacturer = function(){
//        console.log($scope.new_manufacturer.description);
        if($scope.newManForm.$valid){

            $scope.$broadcast('loading', {});
            Api.create('manufacturer/new', $scope.new_manufacturer).then(function(res){
                $scope.$broadcast('loading-done', {});
                if(res.error){
                    $scope.showAlert('alert-danger', 'Error occurred. Unable to create manufacturer');
                }else{
                    $location.path('/manufacturers');
                }
            })


        }
    }


}

manufacturerCreateCtrl.$dependency = ['$scope', '$location', 'Api', 'Settings']


var manufacturersListCtrl = function($scope, Api, manufacturersList){
    $scope.manufacturers = manufacturersList.manufacturers;

    $scope.current_page = 1;

    $scope.page_count = manufacturersList.page_count;

    $scope.paginate = function(action){
        if($scope.page_count >= $scope.current_page){
            $scope.$broadcast('loading', {});
            if(action == 'next'){
                $scope.current_page = $scope.current_page + 1;
                Api.list('manufacturers', $scope.current_page, $scope.filter).then(function(data){
                    $scope.$broadcast('loading-done', {});
                    if(data.manufacturers){
                        $scope.manufacturers = data.manufacturers;
                    }else{
                        $scope.message = 'No data available';
                    }
                })
            }else{
                if($scope.current_page > 1){
                    $scope.current_page = $scope.current_page - 1;
                    Api.list('manufacturers', $scope.current_page, $scope.filter).then(function(data){
                        $scope.$broadcast('loading-done', {})
                        if(data.manufacturers){
                            $scope.manufacturers = data.manufacturers;
                        }else{
                            $scope.message = 'No data available';
                        }
                    })
                }else{
                    $scope.$broadcast('loading-done', {})
                }
            }
        }
    }





}

manufacturersListCtrl.$dependency = ['$scope', 'Api', 'manufacturersList'];


var manufacturerEditCtrl = function($scope, Api, manufacturer){

    $scope.manufacturer = manufacturer;

    $scope.editManufacturer = function(){
        console.log('editform clicked');
        if($scope.editForm.$valid){
            $scope.$broadcast('loading', {});
            Api.edit('manufacturer', $scope.manufacturer._id, $scope.manufacturer).then(function(res){
                $scope.$broadcast('loading-done', {});
                if(res.error){
                    $scope.showAlert('alert-danger', 'Error occured. manufacturer not saved');
                }else{
                    $scope.showAlert('alert-success', 'Successfully saved');
                }
            })
        }

    }

}

manufacturerEditCtrl.$dependency = ['$scope', 'Api', 'manufacturer'];