
    var loginController = function($scope, Api, $location, Session){

        $scope.credentials = {
            email: '',
            password: ''
        }

        $scope.login = function(){
            if($scope.loginForm.$valid){
                $scope.$broadcast('loading', {});
               Api.login($scope.credentials).then(function(isLoggedin){
                   $scope.$broadcast('loading-done', {});
                   if(isLoggedin){
                       console.log(isLoggedin);
                       $scope.setCurrentUser(Session);
                       $location.path('/dashboard');
                   }else{
                       console.log(isLoggedin);
                       $scope.showAlert('alert-danger', 'Incorrect username or password');
                   }
               })



            }
        }




    }

    loginController.$dependency = ['$scope', 'Api', '$location', 'Session'];

