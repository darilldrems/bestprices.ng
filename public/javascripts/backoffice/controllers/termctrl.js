var termsListCtrl = function($scope, Api, termsList){
	$scope.terms = termsList.terms;


	$scope.item_count = termsList.item_count;

    $scope.current_page = 1;

    $scope.page_count= termsList.page_count;

    $scope.filter = {};

	$scope.paginate = function(action){
        if($scope.page_count >= $scope.current_page){
            $scope.$broadcast('loading', {});
            if(action == 'next'){
                $scope.current_page = $scope.current_page + 1;
                Api.list('terms', $scope.current_page, $scope.filter).then(function(data){
                    $scope.$broadcast('loading-done', {});
                    if(data.terms){
                        $scope.terms = data.terms;
                    }else{
                        $scope.message = 'No data available';
                    }
                })
            }else{
                if($scope.current_page > 1){
                    $scope.current_page = $scope.current_page - 1;
                    Api.list('terms', $scope.current_page, $scope.filter).then(function(data){
                        $scope.$broadcast('loading-done', {})
                        if(data.terms){
                            $scope.terms = data.terms;
                        }else{
                            $scope.message = 'No data available';
                        }
                    })
                }else{
                    $scope.$broadcast('loading-done', {})
                }
            }
        }
    }
}

termsListCtrl.$dependency = ['$scope', 'Api', 'termsList'];

var termsNewCtrl = function($scope, Api){
	$scope.term = {
		title: '',
		slug: '',
		status: ''
	}

	$scope.options = ["draft", "published"];

	$scope.create = function(){
		Api.create('term/new', $scope.term).then(function(res){
                $scope.$broadcast('loading-done', {});
                if(res.error){
                    $scope.showAlert('alert-danger', 'term page not created.');
                }else{
                    $scope.showAlert('alert-success', 'Term Successfully created.');
                }
            })
	}
}

termsNewCtrl.$dependency = ['$scope', 'Api'];

var termsEditCtrl = function($scope, Api, term){
	$scope.term = term.term;

	$scope.options = ["draft", "published"];

	$scope.edit = function(){
		Api.edit('term', $scope.term._id, $scope.term).then(function(res){
            $scope.$broadcast('loading-done', {});
            if(res.error){
                console.log(JSON.stringify(res.error));
                alert("An Error occured");
            }else{
                $scope.showAlert('alert-success', 'Successfully edited product');
            }
        });
	}
}

termsEditCtrl.$dependency = ['$scope', 'Api', 'term'];