angular
    .module('CustomDirectives', [])
    .directive('Loading', function(){
        return {
            restrict: 'E',
            template: '<p id="page-loading" class="text-danger text-center">Loading...</p>',
            link: function(){

            }
        }
    })
    .directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
    }])
    .directive('blockUi', function(){

        var link = function($scope, elm, attr){

            var loadingFunction = function(data){
                console.log('event loadding broadcasted');
                angular.element.blockUI({ css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                } });

//                    setTimeout($.unblockUI, 2000);

            }



            $scope.$on('$routeChangeStart', loadingFunction);
            $scope.$on('loading', loadingFunction);

            $scope.$on('$routeChangeSuccess', function(data){
                console.log('event loading-done broadcasted');
                angular.element.unblockUI();
            })

            $scope.$on('loading-done', function(data){
                console.log('event loading-done broadcasted');
                angular.element.unblockUI();
            })
        }



        return {
            restrict: 'E',
            link: link
        }
    })
    .directive('slug', function(){

        var charmap = {
            ' ': " ",
            '¡': "!",
            '¢': "c",
            '£': "lb",
            '¥': "yen",
            '¦': "|",
            '§': "SS",
            '¨': "\"",
            '©': "(c)",
            'ª': "a",
            '«': "<<",
            '¬': "not",
            '­': "-",
            '®': "(R)",
            '°': "^0",
            '±': "+/-",
            '²': "^2",
            '³': "^3",
            '´': "'",
            'µ': "u",
            '¶': "P",
            '·': ".",
            '¸': ",",
            '¹': "^1",
            'º': "o",
            '»': ">>",
            '¼': " 1/4 ",
            '½': " 1/2 ",
            '¾': " 3/4 ",
            '¿': "?",
            'À': "`A",
            'Á': "'A",
            'Â': "^A",
            'Ã': "~A",
            'Ä': '"A',
            'Å': "A",
            'Æ': "AE",
            'Ç': "C",
            'È': "`E",
            'É': "'E",
            'Ê': "^E",
            'Ë': '"E',
            'Ì': "`I",
            'Í': "'I",
            'Î': "^I",
            'Ï': '"I',
            'Ð': "D",
            'Ñ': "~N",
            'Ò': "`O",
            'Ó': "'O",
            'Ô': "^O",
            'Õ': "~O",
            'Ö': '"O',
            '×': "x",
            'Ø': "O",
            'Ù': "`U",
            'Ú': "'U",
            'Û': "^U",
            'Ü': '"U',
            'Ý': "'Y",
            'Þ': "Th",
            'ß': "ss",
            'à': "`a",
            'á': "'a",
            'â': "^a",
            'ã': "~a",
            'ä': '"a',
            'å': "a",
            'æ': "ae",
            'ç': "c",
            'è': "`e",
            'é': "'e",
            'ê': "^e",
            'ë': '"e',
            'ì': "`i",
            'í': "'i",
            'î': "^i",
            'ï': '"i',
            'ð': "d",
            'ñ': "~n",
            'ò': "`o",
            'ó': "'o",
            'ô': "^o",
            'õ': "~o",
            'ö': '"o',
            '÷': ":",
            'ø': "o",
            'ù': "`u",
            'ú': "'u",
            'û': "^u",
            'ü': '"u',
            'ý': "'y",
            'þ': "th",
            'ÿ': '"y',
            'Ā': "A",
            'ā': "a",
            'Ă': "A",
            'ă': "a",
            'Ą': "A",
            'ą': "a",
            'Ć': "'C",
            'ć': "'c",
            'Ĉ': "^C",
            'ĉ': "^c",
            'Ċ': "C",
            'ċ': "c",
            'Č': "C",
            'č': "c",
            'Ď': "D",
            'ď': "d",
            'Đ': "D",
            'đ': "d",
            'Ē': "E",
            'ē': "e",
            'Ĕ': "E",
            'ĕ': "e",
            'Ė': "E",
            'ė': "e",
            'Ę': "E",
            'ę': "e",
            'Ě': "E",
            'ě': "e",
            'Ĝ': "^G",
            'ĝ': "^g",
            'Ğ': "G",
            'ğ': "g",
            'Ġ': "G",
            'ġ': "g",
            'Ģ': "G",
            'ģ': "g",
            'Ĥ': "^H",
            'ĥ': "^h",
            'Ħ': "H",
            'ħ': "h",
            'Ĩ': "~I",
            'ĩ': "~i",
            'Ī': "I",
            'ī': "i",
            'Ĭ': "I",
            'ĭ': "i",
            'Į': "I",
            'į': "i",
            'İ': "I",
            'ı': "i",
            'Ĳ': "IJ",
            'ĳ': "ij",
            'Ĵ': "^J",
            'ĵ': "^j",
            'Ķ': "K",
            'ķ': "k",
            'Ĺ': "L",
            'ĺ': "l",
            'Ļ': "L",
            'ļ': "l",
            'Ľ': "L",
            'ľ': "l",
            'Ŀ': "L",
            'ŀ': "l",
            'Ł': "L",
            'ł': "l",
            'Ń': "'N",
            'ń': "'n",
            'Ņ': "N",
            'ņ': "n",
            'Ň': "N",
            'ň': "n",
            'ŉ': "'n",
            'Ō': "O",
            'ō': "o",
            'Ŏ': "O",
            'ŏ': "o",
            'Ő': '"O',
            'ő': '"o',
            'Œ': "OE",
            'œ': "oe",
            'Ŕ': "'R",
            'ŕ': "'r",
            'Ŗ': "R",
            'ŗ': "r",
            'Ř': "R",
            'ř': "r",
            'Ś': "'S",
            'ś': "'s",
            'Ŝ': "^S",
            'ŝ': "^s",
            'Ş': "S",
            'ş': "s",
            'Š': "S",
            'š': "s",
            'Ţ': "T",
            'ţ': "t",
            'Ť': "T",
            'ť': "t",
            'Ŧ': "T",
            'ŧ': "t",
            'Ũ': "~U",
            'ũ': "~u",
            'Ū': "U",
            'ū': "u",
            'Ŭ': "U",
            'ŭ': "u",
            'Ů': "U",
            'ů': "u",
            'Ű': '"U',
            'ű': '"u',
            'Ų': "U",
            'ų': "u",
            'Ŵ': "^W",
            'ŵ': "^w",
            'Ŷ': "^Y",
            'ŷ': "^y",
            'Ÿ': '"Y',
            'Ź': "'Z",
            'ź': "'z",
            'Ż': "Z",
            'ż': "z",
            'Ž': "Z",
            'ž': "z",
            'ſ': "s"
        };



        function _slugify(s) {
            if (!s) return "";
            var ascii = [];
            var ch, cp;
            for (var i = 0; i < s.length; i++) {
                if ((cp = s.charCodeAt(i)) < 0x180) {
                    ch = String.fromCharCode(cp);
                    ascii.push(charmap[ch] || ch);
                }
            }
            s = ascii.join("");
            s = s.replace(/[^\w\s-]/g, "").trim().toLowerCase();
            return s.replace(/[-\s]+/g, "-");
        }


        return {
            restrict: 'EA',
            scope:{
                ngModel: '=',
                from: '='
            },
            link: function(scope, elm, attr){
                scope.$watch('from', function(newVal){
                    scope.ngModel = _slugify(newVal);
                    scope.$apply();
                    console.log('new value: '+newVal);
                })
            }

        }
    })
    .directive('summary', function(){})
    .directive('customTextarea', function(){
        return {
            restrict:'E',
            require: '^ngModel',
            scope: {
                displayPlaceholder: '@',
                name: '@',
                minCharacters: '@',
                maxCharacters: '@',
                rows: '@',
                ngRequired: '@',
                placeholder: '@',
                ngModel: '=',
                from: '='


            },
            replace: true,
            controller: ['$scope', function($scope){
                $scope.charactersLeft = $scope.maxCharacters;
//                $scope.ngModel = $scope.ngModel;
                console.log('initial ng model data: '+$scope.ngModel);

            }],
            template: "<span>{{maxCharacters}} characters allowed. <span class=\"text-warning\">({{charactersLeft}} left)<span></span></span><textarea placeholder='{{placeholder}}' name=\"{{name}}\" ng-model=\"ngModel\" ng-required=\"ngRequired\" class=\"form-control\" rows=\"{{rows}}\" maxlength='{{maxCharacters}}'>{{ngModel}}</textarea>",

            link: function($scope, elm, attr){

                   if($scope.displayPlaceholder === "true"){
                       $scope.ngModel = $scope.placeholder;
                   }

                $scope.$watch('ngModel', function(newVal, oldVal){
//                        check minimum characters

                       var count = newVal.length;

                       $scope.charactersLeft = $scope.maxCharacters - count;

                       $scope.$apply();

                   })



                    $scope.$watch('from', function(newVal, oldVal){
                        var string_val = newVal.replace(/(<([^>]+)>)/ig, '');
                        string_val = string_val.replace(/\n/ig, '');
                        string_val = string_val.replace(/&#([^\s]*);/g, '');
                        string_val = string_val.replace(/&nbsp;/, '');

                        string_val = string_val.substring(0, $scope.maxCharacters);

                        $scope.ngModel = string_val;

                        $scope.$apply();
                    })

            }

        }
    })