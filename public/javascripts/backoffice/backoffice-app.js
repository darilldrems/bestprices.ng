(function(){
    var backoffice = angular.module('BackOffice', ['ngRoute', 'ui.tinymce', 'BackEndServices', 'CustomDirectives', 'ngSanitize', 'ngCookies']);

    backoffice.run(function($rootScope) {
        $rootScope.accessors = {
            getId: function(row) {
                return row._id
            }
        }
    });


    backoffice.run(function($window, $rootScope){
        $window.addEventListener('offline', function(){
            $rootScope.$emit('loading-done', {});
            alert('Internet connection is out. Please try again later when internet is back.');
        }, false);

        $window.addEventListener('online', function(){
//            $rootScope.$emit('online', {});
            alert('Internet connection is back.')
        }, false);
    })



    backoffice.config(['$routeProvider', '$httpProvider', function($routeProvider, $httpProvider){

        var route = $routeProvider.when('/login', {
            templateUrl: '/partials/login.html',
            controller: 'LoginCtrl'
        })
            .otherwise({
                redirectTo: '/login'
            });


//        dashboard route
        route.when('/dashboard', {
            templateUrl: '/partials/dashboard.html',
            controller: 'DashboardCtrl',
            resolve:{
                dashboardData: function($http){
                    return $http.get('/backend/backoffice/api/dashboard').then(function(res){
                        return res.data;
                    })
                }
            }
//
        });

	route.when('/todos', {
		templateUrl: '/partials/todo.html',
		controller: 'todoCtrl',
        resolve: {
            manufacturersList: function($http){
                return $http.get('/backend/backoffice/api/manufacturers?total=total').then(function(res){
                    return res.data;
                });
            },
            
            categoriesList: function($http){
                return $http.get('/backend/backoffice/api/categories?total=total').then(function(res){
                    return res.data;
                });
            },
            todosList: function($http){
                return $http.get('/backend/backoffice/api/todos').then(function(res){
                    return res.data;
                })
            }
        }
	})

    route.when('/sitemaps', {
        templateUrl: '/partials/sitemap.html',
        controller: 'sitemapCtrl',
        resolve: {
           
            sitemapsList: function($http){
                return $http.get('/backend/backoffice/api/sitemaps').then(function(res){
                    return res.data;
                })
            }
        }
    })

    route.when('/subscribers', {
        templateUrl: '/partials/subscribers.html',
        controller: 'subscribersListCtrl',
        resolve: {
            subscribersList: function($http){
                return $http.get('/backend/backoffice/api/subscribers').then(function(res){
                    return res.data;
                });
            }
        }
    })

    route
        .when('/terms', {
            templateUrl: '/partials/terms.html',
            controller: 'TermsListCtrl',
            resolve: {
                termsList: function($http){
                    return $http.get('/backend/backoffice/api/terms').then(function(res){
                        return res.data;
                    })
                }
            }
        })
        .when('/terms/new', {
            templateUrl: '/partials/terms-new.html',
            controller: 'TermsNewCtrl'
        })
        .when('/terms/edit/:id', {
            templateUrl: '/partials/terms-edit.html',
            controller: 'TermsEditCtrl',
            resolve: {
                term: function($http, $route){
                    return $http.get('/backend/backoffice/api/term/'+$route.current.params.id).then(function(res){
                        return res.data;
                    })
                }
            }
        })

    route
        .when('/landing_pages', {
            templateUrl: '/partials/landingpages.html',
            controller: 'LandingPagesCtrl',
            resolve: {
                landingPagesList: function($http){
                    return $http.get('/backend/backoffice/api/landing_pages').then(function(res){
                        return res.data;
                    });
                }
            }
        })
        .when('/landing_page/edit/:id', {
            templateUrl: '/partials/landingpage-edit.html',
            controller: 'LandingPageEditCtrl',
            resolve: {
                landingPage: function($route, $http){
                    return $http.get('/backend/backoffice/api/landing_page/'+$route.current.params.id).then(function(res){
                        return res.data;
                    });
                }
            }
        })
        .when('/landing_page/new', {
            templateUrl: '/partials/landingpage-new.html',
            controller: 'LandingPageNewCtrl'
        })

//        products route
        route
            .when('/products', {
                templateUrl: '/partials/products.html',
                controller: 'ProductsCtrl',
                resolve: {
                    productsList: function($http){
                        return $http.get('/backend/backoffice/api/products').then(function(res){
                            return res.data;
                        })
                    },
                    categoriesFilter: function($http){
                        return $http.get('/backend/backoffice/api/categories?total=all').then(function(res){
                            return res.data;
                        })
                    },
                    usersFilter: function($http){
                        return $http.get('/backend/backoffice/api/users?total=all').then(function(res){
                            return res.data;
                        })
                    },
                    manufacturersFilter: function($http){
                        return $http.get('/backend/backoffice/api/manufacturers?total=all').then(function(res){
                            return res.data;
                        })
                    }
                }
            })
            .when('/product/new', {
                templateUrl: '/partials/product-new.html',
                controller: 'ProductCreateCtrl',
                resolve:{
                    manufacturersList: function($http){
                        return $http.get('/backend/backoffice/api/manufacturers?total=total').then(function(res){
                            return res.data;
                        })
                    },
                    categoriesList: function($http){
                        return $http.get('/backend/backoffice/api/categories?total=total').then(function(res){
                            return res.data;
                        })
                    },
                    Settings: function($http){
                        return $http.get('/backend/backoffice/api/settings').then(function(res){
                            return res.data;
                        });
                    }
                }
            })
            .when('/product/edit/:id', {
                templateUrl: '/partials/product-edit.html',
                controller: 'ProductEditCtrl',
                resolve: {
                    product: function($route, $http){
                        return $http.get('/backend/backoffice/api/product/'+$route.current.params.id).then(function(res){
                            return res.data;
                        })
                    },
                    categoriesList: function($http){
                        return $http.get('/backend/backoffice/api/categories?total=total').then(function(res){
                            return res.data;
                        })
                    },
                    manufacturersList: function($http){
                        return $http.get('/backend/backoffice/api/manufacturers?total=total').then(function(res){
                            return res.data;
                        })
                    },
                    storesList: function($http){
                        return $http.get('/backend/backoffice/api/stores?total=total').then(function(res){
                            return res.data;
                        });
                    }
                }
            });

//          categories route
        route
            .when('/categories', {
                templateUrl: '/partials/categories.html',
                controller: 'CategoriesCtrl',
                resolve:{
                    categoriesList: function($http){
                        return $http.get('/backend/backoffice/api/categories').then(function(res){
                            return res.data;
                        })
                    }
                }
            })
            .when('/category/new', {
                templateUrl: '/partials/category-new.html',
                controller: 'CategoryCreateCtrl',
                resolve:{
                    categoriesList: function($http){
                        return $http.get('/backend/backoffice/api/categories?total=total').then(function(res){
                            return res.data;
                        })
                    }
                }
            })
            .when('/category/edit/:id', {
                templateUrl: '/partials/category-edit.html',
                controller: 'CategoryEditCtrl',
                resolve: {
                    category: function($http, $route){
                        return $http.get('/backend/backoffice/api/category/'+$route.current.params.id).then(function(res){
                            return res.data.category;
                        })
                    },
                    categoriesList: function($http){
                        return $http.get('/backend/backoffice/api/categories?total=total').then(function(res){
                            return res.data;
                        })
                    }
                }
            })

//            gallery routes
        route
            .when('/gallery', {
                templateUrl: '/partials/gallery.html',
                controller: 'GalleryCtrl',
                resolve:{
                    imagesList: function($http){
                        return $http.get('/backend/backoffice/api/images').then(function(res){
                            return res.data;
                        })
                    }
                }
            })


//          manufacturers route
        route
            .when('/manufacturers', {
                templateUrl: '/partials/brands.html',
                controller: 'ManufacturersListCtrl',
                resolve:{
                    manufacturersList: function($http){
                        return $http.get('/backend/backoffice/api/manufacturers').then(function(res){
                            return res.data;
                        })
                    }
                }
            })
            .when('/manufacturer/new', {
                templateUrl: '/partials/manufacturer-new.html',
                controller: 'ManufacturerCreateCtrl',
                resolve:{
                    Settings: function($http){
                        return $http.get('/backend/backoffice/api/settings').then(function(res){
                            return res.data;
                        });
                    }
                }
            })
            .when('/manufacturer/edit/:id', {
                templateUrl: '/partials/manufacturer-edit.html',
                controller: "ManufacturerEditCtrl",
                resolve:{
                    manufacturer: function($http, $route){
                        return $http.get('/backend/backoffice/api/manufacturer/'+$route.current.params.id).then(function(res){
                            return res.data.manufacturer;
                        })
                    }
                }
            })

//            stores route
        route
            .when('/stores', {
                templateUrl: '/partials/stores.html',
                controller: 'StoresListCtrl',
                resolve: {
                    storesList: function($http){
                        return $http.get('/backend/backoffice/api/stores').then(function(res){
                            return res.data;
                        });
                    }
                }
            })
            .when('/store/new', {
                templateUrl: '/partials/store-new.html',
                controller: 'StoreCreateCtrl'
            })
            .when('/store/edit/:id', {
                templateUrl: '/partials/store-edit.html',
                controller: 'StoreEditCtrl',
                resolve: {
                    store: function($http, $route){
                        return $http.get('/backend/backoffice/api/store/'+$route.current.params.id)
                            .then(function(res){
                                return res.data.store;
                            })
                    }
                }
            })

//        articles route
        route
            .when('/articles', {
                templateUrl: '/partials/articles.html',
                controller: 'ArticlesCtrl',
                resolve: {
                    articlesList: function($http){
                        return $http.get('/backend/backoffice/api/articles').then(function(res){
                            return res.data;
                        })
                    }
                }
            })
            .when('/article/new', {
                templateUrl: '/partials/article-new.html',
                controller: 'ArticleCreateCtrl',
                resolve: {
                    categoriesList: function($http){
                        return $http.get('/backend/backoffice/api/categories?total=total').then(function(res){
                            return res.data;
                        })
                    },
                    articleTypes: function($http){
                        return $http.get('/backend/backoffice/api/article/option/types').then(function(res){
                            return res.data;
                        })
                    }
                }
            })
            .when('/article/edit/:id', {
                templateUrl: '/partials/article-edit.html',
                controller: "ArticleEditCtrl",
                resolve: {
                    article: function($http, $route){
                        return $http.get('/backend/backoffice/api/article/'+$route.current.params.id).then(function(res){
                            return res.data.article;
                        })
                    },
                    articleTypes: function($http){
                        return $http.get('/backend/backoffice/api/article/option/types').then(function(res){
                            return res.data;
                        })
                    },
                    categoriesList: function($http){
                        return $http.get('/backend/backoffice/api/categories?total=total').then(function(res){
                            return res.data;
                        })
                    }
                }
            })

//        how to route
        route
            .when('/howtos', {
                templateUrl: '/partials/howto.html',
                controller: 'HowTosCtrl',
                resolve:{
                    howtoList: function($http){
                        return $http.get('/backend/backoffice/api/howtos').then(function(res){
                            return res.data;
                        })
                    }
                }
            })
            .when('/howto/new', {
                templateUrl: '/partials/howto-new.html',
                controller: 'HowToCreateCtrl',
                resolve: {
                    categoriesList: function($http){
                        return $http.get('/backend/backoffice/api/categories?total=total').then(function(res){
                            return res.data;
                        })
                    }
                }
            })
            .when('/howto/edit/:id', {
                templateUrl: '/partials/howto-edit.html',
                controller: 'HowToEditCtrl',
                resolve: {
                    howto: function($http, $route){
                        return $http.get('/backend/backoffice/api/howto/'+$route.current.params.id).then(function(res){
                            return res.data.howto;
                        })
                    },
                    categoriesList: function($http){
                        return $http.get('/backend/backoffice/api/categories?total=total').then(function(res){
                            return res.data;
                        })
                    }
                }
            })


//            users route
        route
            .when('/users', {
                templateUrl: '/partials/users.html',
                controller: 'UsersCtrl',
                resolve: {
                    usersList: function($http){
                        return $http.get('/backend/backoffice/api/users').then(function(res){
                            return res.data;
                        })
                    }
                }
            })
            .when('/user/new', {
                templateUrl: '/partials/user-new.html',
                controller: 'UserCreateCtrl',
                resolve:{
                    roles: function($http){
                        return $http.get('/backend/backoffice/api/users/roles').then(function(res){
                            return res.data;
                        })
                    }
                }
            })
            .when('/user/edit/:id', {
                templateUrl: '/partials/user-edit.html',
                controller: 'UserEditCtrl',
                resolve: {
                    user: function($http, $route){
                        return $http.get('/backend/backoffice/api/user/'+$route.current.params.id).then(function(res){
                            return res.data.user;
                        })
                    },
                    roles: function($http){
                        return $http.get('/backend/backoffice/api/users/roles').then(function(res){
                            return res.data;
                        })
                    }

                }
            })


        route
            .when('/links', {
                templateUrl: '/partials/links.html',
                controller: 'LinksCtrl',
                resolve:{
                    linksList: function($http){
                        return $http.get('/backend/backoffice/api/links').then(function(res){
                            return res.data;
                        })
                    }
                }
            })
            .when('/link/new', {
                templateUrl: '/partials/link-new.html',
                controller: 'LinkCreateCtrl'
            })
            .when('/link/edit/:id', {
                templateUrl: '/partials/link-edit.html',
                controller: 'LinkEditCtrl',
                resolve:{
                    link: function($http, $route){
                        return $http.get('/backend/backoffice/api/link/'+$route.current.params.id).then(function(res){
                            return res.data.link;
                        });
                    }
                }
            })

        $httpProvider.interceptors.push(['$q', '$timeout', '$location', function($q, $timeout, $location){
            return {
                responseError: function(rejection){
                    console.log('in responseError');
                    if(rejection.status == 401){
                        console.log('discovered 401');
                        $timeout(function(){
                            $location.path('/login');
                        }, 2000)
                        return $q.reject(rejection);
                    }
                }
            }
        }]);


    }]);


    backoffice.service('fileUpload', ['$http', function ($http) {
        this.uploadFileToUrl = function(file, uploadUrl, data){
            var fd = new FormData();
            fd.append('file', file);
//            fd.append('data', data);
//            console.log(fd);
            return $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined},
                params: data
            })

        }





    }]);




    backoffice.controller('LoginCtrl', loginController);
    backoffice.controller('baseCtrl', baseController);
    backoffice.controller('DashboardCtrl', dashboardCtrl);
    backoffice.controller('ProductsCtrl', productsListCtrl);
    backoffice.controller('ProductCreateCtrl', productCreateCtrl);
    backoffice.controller('CategoryCreateCtrl', categoryCreateCtrl);
    backoffice.controller('CategoriesCtrl', categoriesCtrl);
    backoffice.controller('CategoryEditCtrl', categoryEditCtrl);
    backoffice.controller('ManufacturerCreateCtrl', manufacturerCreateCtrl);
    backoffice.controller('ManufacturersListCtrl', manufacturersListCtrl);
    backoffice.controller('ManufacturerEditCtrl', manufacturerEditCtrl);
    backoffice.controller('GalleryCtrl', galleryCtrl);
    backoffice.controller('StoresListCtrl', storesListCtrl);
    backoffice.controller('StoreCreateCtrl', storeCreateCtrl);
    backoffice.controller('StoreEditCtrl', storeEditCtrl);
    backoffice.controller('ArticlesCtrl', articlesCtrl);
    backoffice.controller('ArticleCreateCtrl', articleCreateCtrl);
    backoffice.controller('ArticleEditCtrl', articleEditCtrl);
    backoffice.controller('UsersCtrl', usersCtrl);
    backoffice.controller('UserCreateCtrl', userCreateCtrl);
    backoffice.controller('HowTosCtrl', howtosCtrl);
    backoffice.controller('HowToCreateCtrl', howtoCreateCtrl);
    backoffice.controller('HowToEditCtrl', howtoEditCtrl);
    backoffice.controller('UserEditCtrl', userEditCtrl);
    backoffice.controller('LinkEditCtrl', linkEditCtrl);
    backoffice.controller('LinkCreateCtrl', linkCreateCtrl);
    backoffice.controller('LinksCtrl', linksCtrl);
    backoffice.controller('todoCtrl', todoCtrl);
    backoffice.controller('sitemapCtrl', sitemapCtrl);
    backoffice.controller('LandingPagesCtrl', landingPagesCtrl);
    backoffice.controller('LandingPageEditCtrl', landingPageEditCtrl);
    backoffice.controller('LandingPageNewCtrl', landingPageNewCtrl);
    backoffice.controller('TermsListCtrl', termsListCtrl);
    backoffice.controller('TermsEditCtrl', termsEditCtrl);
    backoffice.controller('TermsNewCtrl', termsNewCtrl);

    backoffice.controller('ProductEditCtrl', productEditCtrl);

})();
