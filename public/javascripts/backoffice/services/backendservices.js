angular
    .module('BackEndServices', ['MyServices'])
    .factory('Api', ['$http', 'Session', function($http, Session){

        var base_url = '/backend/backoffice/api/';
        var api = {};

        api.login = function(details){
            return $http.post(base_url+'login', details).then(function(res){
                if(res.data.user){
//                    set session here
                    console.log(res.data.user.email);
                    var user_copy = angular.copy(res.data.user);
                    Session.create(user_copy);
                    return true;
                }else{
                    return false;
                }
            })
        }

        api.search = function(path, query){
            return $http.get(base_url+path+'/search/'+query).then(function(res){
                return res.data;
            })
        }

        api.list = function(path, page, filter){
            var page_number = page || 1;
            var condition = filter || {state: 'active'};

            return $http.get(base_url+path+'?page='+page_number, {params:{condition: condition}}).then(function(res){
                return res.data;
            })

        }


        api.edit = function(path, id,  obj){
            return $http.post(base_url+path+'/edit/'+id, obj).then(function(res){
                return res.data;
            })
        }


        api.addOffer = function(param){
            return $http.get(base_url+'offer/new', {params: param}).then(function(res){
                return res.data;
            })
        }

        api.getOffers = function(product_id){
            return $http.get(base_url+'product/'+product_id).then(function(res){
                return res.data.offers;
            })
        }

        api.create = function(path, body){
            return $http.post(base_url+path, body).then(function(res){
                return res.data;
            })
        }

        api.logout = function(){
            $http.get(base_url+'logout').then(function(res){
                Session.destroy()

            });
        }


        api.bulkDelete = function(path, id_array){
            return $http.post(base_url+path+'/bulk_delete', {ids: id_array}).then(function(res){
                return res.data;
            })
        }

        api.delete = function(path, id){
            return $http.get(base_url+path+'/delete/'+id).then(function(res){
                console.log(res.data);
                return res.data;
            })
        }

        api.deleteOffer = function(path, product_id, offer_id){
            return $http.get(base_url+path+'/delete/'+product_id+'/'+offer_id).then(function(res){
                return res.data;
            })
        }

        api.bulkDeleteOffers = function(product_id, offers){
            // alert(JSON.stringify(offers));
            return $http.get(base_url+"offer"+"/bulk_delete/"+product_id, {params: {offers_ids: offers}}).then(function(res){
                return res.data;
            })
        }


        return api;
    }])