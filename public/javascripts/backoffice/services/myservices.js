angular
    .module('MyServices', ['ngCookies'])
    .factory('Session', ['$cookies', function($cookies){
        var session = {
            role: $cookies['role'],
            email: $cookies['email'],
            id: $cookies['id'],
            first_name: $cookies['first_name'],
            last_name: $cookies['last_name'],
            profile: $cookies['profile']
        }


        session.role = $cookies['role'];
        session.email = $cookies['email'];
        session.id = $cookies['id'];
        session.first_name = $cookies['first_name'];
        session.last_name = $cookies['last_name'];
        session.profile = $cookies['profile'];

//        if(localStorageService.isSupported){
//            console.log('Yes it is a supported feature');
//        }else{
//            console.log('its not supported o');
//        }

        session.create = function(obj){


//            console.log('What is localStorage data role'+$window.localStorage['role'])

            $cookies['role'] = angular.copy(obj.role);
            $cookies['email'] = angular.copy(obj.email);
            $cookies['id'] = angular.copy(obj.id);
            $cookies['first_name'] = angular.copy(obj.first_name);
            $cookies['last_name'] = angular.copy(obj.last_name);
            $cookies['profile'] = angular.copy(obj.photo);
            // session.role = angular.copy(obj.role);
            // session.email = angular.copy(obj.email);
            // session.id = angular.copy(obj.id);
            // session.first_name = angular.copy(obj.first_name);
            // session.last_name = angular.copy(obj.last_name);
            // session.profile = angular.copy(obj.photo);


        }

        session.destroy = function(){
            delete session.role;
            delete session.email;
            delete session.id;
            delete session.first_name;
            delete session.last_name;
            delete session.profile;
        }

        return session;
    }])
