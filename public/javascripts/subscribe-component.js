var query = {
    status: 'subscribe',
    subscribe_for: 'pre-launch'
};

var req_path = 'backend/api/email/subscription';

(function(q, path){
    $('#subscribeForm').submit(function(event){
        event.preventDefault();
        $('#warning').addClass('hide');

        var email = $("input[type='email']").val();

        if(validateEmail(email) && email){
//
            mixpanelTrackEventEmailSubscribed('yes', email);
            q.email = email;
            $.ajax({
                url: path,
                type: 'GET',
                data: q
            }).done(function(result){
                    if(!result.error){
                        $('#subscribeForm').addClass('hide');
                        $('#success-message').addClass('show');
                        $('#social-share').addClass('show');
                    }
                })
        }else{
            mixpanelTrackEventEmailSubscribed('no', email);
            $('#warning').addClass('show');
        }

        return false;


    });
})(query, req_path);

function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,6})?$/;
    if( !emailReg.test( $email ) ) {
        return false;
    } else {
        return true;
    }
}

/*
* Using mix panel to track email subscribed event
* */

function mixpanelTrackEventEmailSubscribed(success, email){

    var device_type = "desktop";
    if($.browser.mobile){
        device_type = 'mobile'
    }

    mixpanel.track("Email subscribed", {
        referrer: document.referrer,
        device: device_type,
        browser: $.browser.version,
        successful: success,
        email: email
    })
}