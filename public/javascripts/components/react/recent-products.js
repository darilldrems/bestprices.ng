var RecentProducts = React.createClass({
	getInitialState: function(){
		return {
			products: []
		}
	},
	componentDidMount: function(){
		$.ajax({
			url: this.props.domain+"/search?search="+this.props.query+"&resp_type=json",
			cache: false,
			dataType: "json",
			success: function(products){
				this.setState({products: products});
			}.bind(this)
		});
	},
	render: function(){
		var products = this.state.products.map(function(p){
			return (
				<Product domain={this.props.domain} currency={this.props.currency} product={p} />

				)
		}.bind(this))
		return (
				<div>
					<h4>Popular Products</h4>
					{products}
				</div>

			)
	}

})

var Product = React.createClass({
	render: function(){
		var url =this.props.domain+"/"+this.props.product.slug;
		var image = this.props.domain + this.props.product.image_src
		var imgStyle = {
			maxHeight:250
		}
		var thumbnailStyle = {
			paddingBottom: '12px'
		}

		var bottonStyle = {
			display: 'inline',
			marginTop: '-30px'
		}
		return (

			<div className="thumbnail" style={thumbnailStyle}>
                <img src={image} style={imgStyle}  />
                <div className="caption">
                    <h5>{this.props.product.title[0]}</h5>
                    <div className="about">
                        <div className="price">
                            <span>In {this.props.product.number_of_offers} stores</span>
                            <p>{this.props.currency}{this.props.product.lowest_price}</p>
                        </div>
                        <div className="compare-button pull-right" style={bottonStyle}>
                            <a href={url} className="btn btn-compare">Compare</a>
                        </div>
                    </div>
                </div>
            </div>


			)
	}
})