var SimilarProducts = React.createClass({
	getInitialState: function(){
		return {products: []}
	},
	componentDidMount: function(){
		$.ajax({
			url: "/search?search="+this.props.name+"&resp_type=json&limit=4",
			dataType: "json",
			cache: false,
			success: function(products){
				this.setState({products: products})
			}.bind(this)
		})
	},
	render: function(){
		var products = this.state.products.map(function(product){
			return (
				<Product product={product} currency={this.props.currency}/>
				)
		}.bind(this))
		return(

			
                <div class="popular-products">
                    <h4>People also viewed</h4>
                    {products}
                </div>
			)
	}

})

var Product = React.createClass({
	render:function(){
		var url = "/"+this.props.product.slug;
		return (
			<div className="thumbnail">
                <img src={this.props.product.image_src} width="20%"  />
                <div className="caption">
                    <h5>{this.props.product.title[0]}</h5>
                    <div className="about">
                        <div className="price">
                            <span>In {this.props.product.number_of_offers} stores</span>
                            <p>{this.props.currency}{this.props.product.lowest_price}</p>
                            <a href={url} className="btn btn-compare btn-sm">View Deal</a>
                        </div>
                    </div>
                </div>
            </div>

			)
	}
})