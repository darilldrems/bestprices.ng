var SubscribeBox = React.createClass({
	getInitialState: function(){
		return{
			title: "",
			buttonTitle: "",
			placeholder: "",
			successful: false,
			successMessage: ""

		}
	},
	validateEmail: function(email){
		var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    	return re.test(email);
	},
	subscribe: function(){
		var email = React.findDOMNode(this.refs.email).value.trim();
		var is_valid = this.validateEmail(email);
		if(is_valid){
			this.submitEmail(email);
			this.setState({successful: true});
		}else{
			React.findDOMNode(this.refs.email).style.borderColor = "red";
		}
	},
	submitEmail: function(email){
		$.ajax({
			url: "/backend/api/email/subscription?status=subscribe&subscribe_for=updates&email="+email,
			dataType: "json",
			cache: false,
			success: function(data){
				// alert(data)
			}
		})
	},
	componentDidMount: function(){
		// alert("inhere")
		this.setState({title: this.props.title});
		this.setState({buttonTitle: this.props.button});
		this.setState({placeholder: this.props.placeholder});
		this.setState({successMessage: this.props.successMessage});
	},
	render: function(){
		
		

		if(this.state.successful){
			return (
				<h4>{this.state.successMessage}</h4>
				)
		}
			

		return(
				<div>
					<h4>{this.state.title}</h4>
                    <input className="form-control" ref="email" type="email" placeholder={this.state.placeholder} />
                    <br/>
                    <button onClick={this.subscribe} className="btn btn-black">{this.state.buttonTitle}</button>
                </div>
			)
	}
})