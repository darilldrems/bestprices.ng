var Offers = React.createClass({
	getInitialState: function(){
		return {
			product: {}
		}
	},
	componentDidMount: function(){
		console.log("in here")
		var selector = ".embed-component";
		var id = $(selector).attr("id");
		console.log("id is: "+id);
		$.ajax({
			url: this.props.domain+"/ajax/product/"+id,
			dataType: "jsonp",
			cache: false,
			success: function(product){
				console.log("in here");
				console.log(JSON.stringify(product))
				this.setState({product: product})
			}.bind(this)
		})


	},
	render: function(){
		var title = ""
		var offers = null
		var mobileOffers = null
		if(this.state.product){
			title = this.state.product.title;
		}

		if(this.state.product.offers){
			offers = this.state.product.offers.map(function(offer){
				return (
						<OfferDesktop offer={offer} domain={this.props.domain} currency={this.props.currency} />
						)
			}.bind(this))

			mobileOffers = this.state.product.offers.map(function(offer){
				return (
						<OfferMobile offer={offer} domain={this.props.domain} currency={this.props.currency} />
						)
			}.bind(this))
		}
		return (
			<div>
				<h4 className="">Offers for {title}</h4>
				<table className="hidden-xs table table-striped">
					<thead className="hidden-xs">
					  <tr>
					    <th>Seller</th>
					    <th>Seller Rating</th>
					    <th>Est. Delivery</th>
					    <th>Price</th>
					    <th></th>
					  </tr>
					</thead>
					<tbody>
					  {offers}
					</tbody>
				</table>
				{mobileOffers}
			</div>
  

		)
	}
});

var OfferDesktop = React.createClass({
	componentDidMount: function(){
		$(function(){
          $('div.store-star-rating').raty({
              path:"https://cdnjs.cloudflare.com/ajax/libs/raty/2.7.0/images",
            readOnly: true,
            score: function() {
              return $(this).attr('data-rating');
            }
          });
      });
	},
	render: function(){
		var aff_link = this.props.offer.store.affiliate_query;
		var link = "#"
		if(aff_link){
			link = aff_link.replace("[link]", this.props.offer.link)
		}
		var imgStyle = {
			maxHeight: '25px'
		}

		var pStyle = {
			color: 'red'
		}
		return(
			<tr>
		        <td><img src={this.props.offer.store.logo} style={imgStyle}/></td>
		        <td>
		         <div className="store-star-rating" data-rating={this.props.offer.store.rating}>
		         </div>
		        </td>
		        <td></td>
		        <td>
		          <p style={pStyle}>{this.props.currency}{this.props.offer.price}</p>
		          <p></p>
		        </td>
		        <td>
		          <a href={link} target="_blank" className="btn btn-compare btn-sm">go to Store</a>
		        </td>
		      </tr>



			)
	}
})

var OfferMobile = React.createClass({
	componentDidMount: function(){
		$(function(){
          $('div.store-star-rating').raty({
              path:"https://cdnjs.cloudflare.com/ajax/libs/raty/2.7.0/images",
            readOnly: true,
            score: function() {
              return $(this).attr('data-rating');
            }
          });
      });
	},
	render: function(){
		var aff_link = this.props.offer.store.affiliate_query;
		var link = "#"
		if(aff_link){
			link = aff_link.replace("[link]", this.props.offer.link)
		}
		var imgStyle = {
			maxHeight: '25px'
		}

		var pStyle = {
			color: 'red'
		}

		return (
			<div className="visible-xs price-range">
			    <div className="first-row"><img src={this.props.offer.store.logo} style={imgStyle}/>
			      <div className="item-price">
			        <h5>Store Price</h5>
			        <p style={pStyle}>{this.props.currency}{this.props.offer.price}</p>
			      </div>
			    </div>
			    <div className="second-row">
			      <div className="store-star-rating" data-rating={this.props.offer.store.rating}>
		         </div>
			      <div className="delivery">
			        
			      </div>
			    </div>
			    <div className="third-row">
			      <a href={link} target="_blank" className="btn btn-compare btn-sm">go to Store</a>
			    </div>
			  </div>
		  )
	}
})