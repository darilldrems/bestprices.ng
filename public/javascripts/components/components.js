//method expects object types(article or howto or product) with object id to increase page view count on the server

function countPageViews(objectType, objectId){
    var data = {
        object: objectType || null,
        id: objectId || null
    };

    if(data.object != null && data.id != null){
        $.ajax('/ajax/pageviews', {
            data: data,
            type: 'GET',
            success: function(){

            },
            error: function(){

            }
        });
    }

    return false;

}







//offers component insert prices of specied products into the article content
function offersComponent(){
    var componentSelector = ".embed-component";
    var components = $(componentSelector);
    console.log("components selected"+components);

//    alert('in here');
    $.each(components, function(i, val){

        var comp = val

        var product_id = $(comp).attr('id');

        var url = "http://bestprices.ng/ajax/product/"+product_id;

        tempReplace(url, "#compare-prices-template", comp, "GET", "jsonp");

    })


}

function hotProducts(){
    var component = $("#hot-products-container");
    tempReplace("http://bestprices.ng/ajax/products/hot", "#hot-products-template", component, "GET", "jsonp")
}


function popularPosts(){
    var component = $("#popular-posts-container");
    tempReplace("http://bestprices.ng/ajax/posts/popular", "#popular-posts-template", component, "GET", "jsonp")
}

function latestNews(){
    var component = $("#news-template-container");
    console.log("container:"+component.attr('id'));
    tempReplace("http://bestprices.ng/ajax/news/latest", "#recent-news-template", component, "GET", "jsonp")


}

function recommendedProducts(term){
    var component = $('#product-display-container');

    tempReplace("http://bestprices.ng/ajax/products/similar/"+term, "#recommended-products-template", component, "GET", "jsonp")
}



function tempReplace(url, template_id, component, req, dataType){
    Handlebars.registerHelper('afflink', function(offer){
        var aff_query = offer.store.affiliate_query || offer.link
        var aff_link = aff_query.replace("[link]", offer.link)

        return aff_link;
    })

    $.ajax({
        url: url,
        async: false,
        type: req,
        dataType: dataType,
        success: function(data){
            console.log('response data'+JSON.stringify(data));
            console.log("template_id"+template_id);
            var source = $(template_id).html()
            var template = Handlebars.compile(source);
            var _html = template({data:data});
            $(component).replaceWith(_html);
//            console.log("outerhtml:"+component.outerHTML);
        }

    })
}


function subscribeEmail(){
//    jsonp endpoint. data = status n email
    var path =  'http://bestprices.ng/backend/api/email/subscription';

    $("#email-subscribe-form").submit(function(event){
        event.preventDefault();

//        alert("in here");
        var inputField = $("#subscribe-email-textfield");


        var email = inputField.val();
//        console.log("email:"+email);
        if(isEmail(email)){
//            alert("in true");
            inputField.prop("disabled", true);
            var req_data = {
                status: "subscribe",
                email: email,
                subscribe_for: "pre-launch"
            }


            $.ajax({
                url: path,
                dataType: "jsonp",
                type: 'GET',
                data: req_data
            }).done(function(result){
                    console.log(JSON.stringify(result));
                    if(!result.error){
                        inputField.prop("disabled", false);
                        $("#email-subscribe-form").toggle();
                        $("#email-subscribe-message").toggle();
                        mixpanelTrackEventEmailSubscribed('subscribed', email);
                    }else{
                        mixpanelTrackEventEmailSubscribed('error', email);
                    }
                })

        }else{
            inputField.css({"border": "1px solid red"});
            mixpanelTrackEventEmailSubscribed('not valid email', email);
        }

        return false;


    })


}

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function mixpanelTrackEventEmailSubscribed(success, email){


    mixpanel.track("Email subscribed", {
        referrer: document.referrer,
        successful: success,
        email: email
    })
}

